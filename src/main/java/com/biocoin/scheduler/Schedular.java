package com.biocoin.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class Schedular {

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws Exception {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	@Scheduled(cron = "0 * * * * ?")
	public void cron() throws Exception {
		System.out.println("Entered into scheduler");

		JSONObject json = readJsonFromUrl("http://localhost:8070/biocoin/api/biocoinuser/check/slabs/schedule");
		String json2 = (String) json.get("message");
		String json3 = (String) json.get("status");

		System.out.println(json2 + " :::::::::: " + json3);
	}
}
