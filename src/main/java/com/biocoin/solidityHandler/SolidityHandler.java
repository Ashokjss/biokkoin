package com.biocoin.solidityHandler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

import com.biocoin.dto.PaypalDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.model.BurnedToken;
import com.biocoin.model.Config;
import com.biocoin.model.ICOPeriodInfo;
import com.biocoin.model.MintedToken;
import com.biocoin.model.PaymentsInfo;
import com.biocoin.model.PurchaseTokenInfo;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.Token_info;
import com.biocoin.model.TransactionHistory;
import com.biocoin.repo.BurnedInfoRepo;
import com.biocoin.repo.ConfigInfoRepository;
import com.biocoin.repo.ICOPeriodInfoRepo;
import com.biocoin.repo.MintTokenInfoRepo;
import com.biocoin.repo.PaymentInfoRepo;
import com.biocoin.repo.PurchaseTokenInfoRepository;
import com.biocoin.repo.TokensInfoRepo;
import com.biocoin.repo.TransactionInfoRepository;
import com.biocoin.repo.UserRegisterRepository;
import com.biocoin.service.TokenService;
import com.biocoin.service.TransactionHistoryService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.solidityToJava.Biokkoin;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.CurrentValueUtils;
import com.biocoin.utils.EncryptDecrypt;
import com.biocoin.utils.SessionCollector;
import com.biocoin.utils.BitCoinUtils;

@Service
public class SolidityHandler {
	private static final Logger LOG = LoggerFactory.getLogger(SolidityHandler.class);

	private Web3j web3j = Web3j.build(new HttpService("https://rinkeby.infura.io/"));

	public static TransactionReceipt transactionReceipt;

	@Autowired
	private Environment env;
	@Autowired
	private ConfigInfoRepository configrepo;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private BurnedInfoRepo burnedInfoRepo;
	@Autowired
	private MintTokenInfoRepo mintTokenInfoRepo;
	@Autowired
	private BioCoinUtils bioCoinUtils;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private UserRegisterRepository userRegisterRepository;
	@Autowired
	private TransactionHistoryService transactionHistoryService;
	@Autowired
	private TransactionInfoRepository transactionInfoRepository;
	@Autowired
	private TokensInfoRepo tokensInfoRepo;
	@Autowired
	private CurrentValueUtils currentValueUtils;
	@Autowired
	private PurchaseTokenInfoRepository purchaseTokenInfoRepository;
	@Autowired
	private ICOPeriodInfoRepo icoPeriodInfoRepo;
	@Autowired
	private PaymentInfoRepo paymentInfoRepo;
	@Autowired
	private BitCoinUtils bitCoinUtils;

	BigInteger GAS_LIMIT = Contract.GAS_LIMIT;
	BigInteger GAS_PRICE = Contract.GAS_PRICE;
	BigInteger initialWeiValue = BigInteger.valueOf(0);

	public boolean TransferCoinfromAdminWalletTouserWallet(String getAddress, BigInteger requestTokens) {
		// TODO Auto-generated method stub
		try {

			Credentials credentials = WalletUtils.loadCredentials(this.env.getProperty("credentials.password"),
					this.env.getProperty("credentials.address"));
			Biokkoin assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
					Contract.GAS_PRICE, Contract.GAS_LIMIT);
			TransactionReceipt transactionReceipt = assetToken.transfer(getAddress, requestTokens).send();
			LOG.info(" transactionReceipt  " + transactionReceipt.toString());
			if (transactionReceipt.toString() != null) {
				LOG.info("Transfer Coin Successfully");
				return true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			LOG.info("Exception is " + e);
			return false;
		}

		LOG.info("Transfer Coin Failed");
		return false;
	}

	public String burntoken(TokenDTO tokendto) throws IOException, CipherException, Exception {

		HttpSession session = SessionCollector.find(tokendto.getSessionId());
		// Input sessionid
		if (tokendto.getSessionId() != null) {
			String mail = (String) session.getAttribute("emailId");
			RegisterInfo reginfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
			String etherwalletaddress = reginfo.getEthWalletAddress();
			String etherwalletpassword = reginfo.getEthWalletPassword();
			Config config = configrepo.findConfigByConfigKey("walletFile");

			Credentials credentials = WalletUtils.loadCredentials(EncryptDecrypt.decrypt(etherwalletpassword),
					config.getConfigValue() + EncryptDecrypt.decrypt(etherwalletaddress));

			Biokkoin assetToken = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials, GAS_PRICE,
					GAS_LIMIT);

			System.out.println("Number Of Token : " + tokendto.getBurnToken());

			// CompletableFuture.supplyAsync(() -> {
			TransactionReceipt transactionreceipt;
			// try {
			Double amt = tokendto.getBurnToken() * 100000000;
			BigDecimal amount = BigDecimal.valueOf(amt);
			BigInteger tokenAmount = amount.toBigInteger();
			transactionreceipt = assetToken.burn(tokenAmount).send();
			System.out.println("TransactionReceipt : " + transactionreceipt.getStatus());
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// return "Callback blockchain";
			//
			// }).thenAccept(product -> {

			if (transactionreceipt.getStatus().equalsIgnoreCase("0x1")) {
				BurnedToken burnedToken = new BurnedToken();
				burnedToken.setBurnedtokens(tokendto.getBurnToken().doubleValue());
				burnedToken.setRegisterInfo(reginfo);
				burnedToken.setDate(new Date());
				burnedToken.setEmailId(mail);
				burnedInfoRepo.save(burnedToken);

				reginfo.setBurnedTokens(reginfo.getBurnedTokens() + tokendto.getBurnToken().doubleValue());
				userRegisterRepository.save(reginfo);

				Token_info token_info = tokenService.findById(1);

				token_info.setBurnedTokens(token_info.getBurnedTokens() + tokendto.getBurnToken());
				token_info.setTotalAvailableTokens(token_info.getTotalAvailableTokens() - tokendto.getBurnToken());
				token_info.setManualAvailableTokens(token_info.getManualAvailableTokens() - tokendto.getBurnToken());
				tokensInfoRepo.save(token_info);

			} else if (transactionreceipt.getStatus().equalsIgnoreCase("0x0")) {
				return env.getProperty("token.burn.failed");
			}
			// });
			return env.getProperty("token.burn.success");
		}
		return env.getProperty("token.burn.failed");
	}

	public String mintToken(TokenDTO tokendto) throws IOException, CipherException, Exception {
		HttpSession session = SessionCollector.find(tokendto.getSessionId());

		String mail = (String) session.getAttribute("emailId");
		RegisterInfo reginfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		if (reginfo != null) {
			String etherwalletaddress = reginfo.getEthWalletAddress();
			String etherwalletpassword = reginfo.getEthWalletPassword();

			Config configinfo = configrepo.findConfigByConfigKey("walletFile");
			Credentials credentials = WalletUtils.loadCredentials(EncryptDecrypt.decrypt(etherwalletpassword),
					configinfo.getConfigValue() + EncryptDecrypt.decrypt(etherwalletaddress));
			Biokkoin biokkoin = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials, GAS_PRICE,
					GAS_LIMIT);

			// CompletableFuture.supplyAsync(() -> {

			BigInteger mintedAmount = new BigDecimal(tokendto.getMintToken() * 100000000).toBigInteger();
			TransactionReceipt transactionreceipt;
			// try {
			transactionreceipt = biokkoin.mint(tokendto.getToAddress(), mintedAmount).send();

			if (transactionreceipt.getStatus().equalsIgnoreCase("0x1")) {
				System.out.println("Token minting success : " + transactionreceipt.getStatus());
				MintedToken mintedToken = new MintedToken();
				mintedToken.setEmailId(mail);
				/* Minted Address */
				mintedToken.setMintedETHwalletaddress(tokendto.getToAddress());
				mintedToken.setMintedtokens(mintedAmount.doubleValue());
				mintedToken.setRegisterInfo(reginfo);
				mintedToken.setDate(new Date());
				mintTokenInfoRepo.save(mintedToken);

				Token_info token_info = tokenService.findById(1);

				token_info.setMintTokens(token_info.getMintTokens() + tokendto.getMintToken());
				token_info.setTotalAvailableTokens(token_info.getTotalAvailableTokens() + tokendto.getMintToken());
				token_info.setManualAvailableTokens(token_info.getManualAvailableTokens() + tokendto.getMintToken());
				token_info.setTotalToken(token_info.getTotalToken() + tokendto.getMintToken());
				tokensInfoRepo.save(token_info);
				return env.getProperty("token.mint.success");
			} else if (transactionreceipt.getStatus().equalsIgnoreCase("0x0")) {

				System.out.println("Token minting failed : " + transactionreceipt.getStatus());
				return env.getProperty("token.mint.failed");
			}

			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// return true;
			// }).thenAccept(product -> {
			// });
		}
		return env.getProperty("token.mint.failed");

	}

	public boolean manualtransferCoinsBetweenUsers(TokenDTO tokenDTO) {
		try {
			TransactionReceipt transactionReceipt;
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			String mail = (String) session.getAttribute("emailId");
			RegisterInfo regInfo = userRegisterService.isAccountExistCheckByEmailId(mail);

			String getAddress = EncryptDecrypt.decrypt(regInfo.getEthWalletAddress());
			String getpsw = EncryptDecrypt.decrypt(regInfo.getEthWalletPassword());
			Config config = configrepo.findConfigByConfigKey("walletfile");

			Credentials credentials = WalletUtils.loadCredentials(getpsw, config.getConfigValue() + getAddress);

			Biokkoin assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
					Contract.GAS_PRICE, Contract.GAS_LIMIT);

			String ethwalletaddress = bioCoinUtils.getWalletAddress(config.getConfigValue(), getAddress);

			Integer balanceOfTokens = assetToken.balanceOf(ethwalletaddress).send().intValue();

			BigInteger transferToken = BigInteger.valueOf(tokenDTO.getTransferToken());
			if (balanceOfTokens >= tokenDTO.getTransferToken()) {
				// Double i=tokenDTO.getTransferToken();
				transactionReceipt = assetToken.transfer(tokenDTO.getToAddress(), transferToken).send();
			} else {
				return false;
			}

			LOG.info(" transactionReceipt  " + transactionReceipt.toString());
			if (transactionReceipt.toString() != null) {
				LOG.info("Transfer Coin Successfully");
				return true;
			}
		} catch (Exception e) {
			LOG.info("Exception is " + e);
			return false;
		}
		LOG.info("Transfer Coin Failed");
		return false;
	}

	public Double balanceTokens(TokenDTO tokenDTO) throws Exception {

		Config configInfo = configrepo.findConfigByConfigKey("walletFile");
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());

		String mail = (String) session.getAttribute("emailId");

		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);

		String walletPassword = registerInfo.getEthWalletPassword();

		String dwalletPassword = EncryptDecrypt.decrypt(walletPassword);

		String fromAddress = bioCoinUtils.getEtherWalletAddress(configInfo.getConfigValue(),
				EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress()));

		LOG.info("configInfo.getConfigValue() + EncryptDecrypt.decrypt(registerInfo.getEtherWalletAddress())");

		Credentials credentials = WalletUtils.loadCredentials(dwalletPassword,
				configInfo.getConfigValue() + EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress()));
		Biokkoin assetToken = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials,
				Contract.GAS_PRICE, Contract.GAS_LIMIT);

		BigInteger amount = assetToken.balanceOf(fromAddress).send();
		Double value = amount.doubleValue() / 100000000;
		tokenDTO.setTokenBalance(value);
		return value;
	}

	public String manualTokenTransferToUsers(TokenDTO tokenDTO) throws Exception {
		transactionReceipt = new TransactionReceipt();
		Token_info token_info = tokensInfoRepo.findOne(1);
		Config configInfo = configrepo.findConfigByConfigKey("walletFile");
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		String fromAddress = bioCoinUtils.getEtherWalletAddress(configInfo.getConfigValue(),
				EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress()));
		LOG.info("configInfo.getConfigValue() + EncryptDecrypt.decrypt(registerInfo.getEtherWalletAddress())");

		System.out.println("Before the transactionHistory");
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setFromAddress(fromAddress);
		transactionHistory.setToAddress(tokenDTO.getToAddress());
		transactionHistory.setCryptoAmount(new BigDecimal(0.0));
		transactionHistory.setTransferToken(tokenDTO.getSendToken());
		transactionHistory.setTxDate(new Date());
		transactionHistory.setPaymentMode("Manual");
		transactionHistory.setTxstatus("Pending");
		transactionHistory.setTypeOfVerifications(registerInfo.getTypeOfVerifications());
		transactionHistory.setRegisterInfo(registerInfo);
		transactionHistory.setSenderId(registerInfo.getId());
		transactionHistory.setReceiverId(0);
		transactionHistory.setEmailId(mail);
		transactionInfoRepository.save(transactionHistory);

		Credentials credentials;
		try {
			if (transactionHistory != null) {
				credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
						configInfo.getConfigValue() + EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress()));
				Biokkoin assetToken = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials,
						Contract.GAS_PRICE, Contract.GAS_LIMIT);
				if (assetToken != null) {
					LOG.info("Token Getting Amount" + tokenDTO.getSendToken());
					BigDecimal amount = BigDecimal.valueOf(tokenDTO.getSendToken());
					BigDecimal tamount = amount.multiply(new BigDecimal("100000000"));
					BigInteger tokenAmount = tamount.toBigInteger();
					transactionReceipt = assetToken.transfer(tokenDTO.getToAddress().trim(), tokenAmount).send();

					LOG.info("transactionReceipt : " + transactionReceipt.getTransactionHash()
							+ transactionReceipt.getGasUsed() + " : " + transactionReceipt.getStatus() + " : "
							+ transactionReceipt.getCumulativeGasUsed());
					tokenDTO.setId(transactionHistory.getId());
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (transactionReceipt.getStatus().equalsIgnoreCase("0x1")) {
			// if (transactionHistory.getTypeOfVerifications()
			// .equalsIgnoreCase(env.getProperty("transfer.type.mode.others"))
			// || transactionHistory.getTypeOfVerifications()
			// .equalsIgnoreCase(env.getProperty("transfer.type.mode.ico.users")))
			// {
			//
			// System.out.println("Transfering to other account : " +
			// transactionHistory.getTypeOfVerifications());
			//
			// } else if
			// (!tokenDTO.getTypeOfVerifications().equalsIgnoreCase(env.getProperty("transfer.type.mode.others"))
			// || !transactionHistory.getTypeOfVerifications()
			// .equalsIgnoreCase(env.getProperty("transfer.type.mode.ico.users")))
			// {

			// Date date = new Date();
			// System.out.println("Current Date : " + date);
			// Date vestingEndDate = token_info.getLockedTokensEndDate();
			// System.out.println("Vesting period End Date : " +
			// vestingEndDate);

			// if (date.before(vestingEndDate)) {
			// System.out.println("Inside Vesting period");
			//
			// boolean isValid;
			// try {
			// isValid = tokenService.validatingAndUpdating(tokenDTO);
			// if (!isValid) {
			// System.out.println("Not properly saved");
			// }

			transactionHistory.setReceiverId(0);
			transactionInfoRepository.save(transactionHistory);

			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// } else {
			// System.out.println("Vesting period Ends");
			// }
			// }

			transactionHistory.setTxstatus("Success");
			transactionInfoRepository.save(transactionHistory);

			if (env.getProperty("main.address").equals(tokenDTO.getToAddress())) {

				token_info.setTotalAvailableTokens(token_info.getTotalAvailableTokens() + tokenDTO.getSendToken());
				token_info.setManualAvailableTokens(token_info.getManualAvailableTokens() + tokenDTO.getSendToken());
				token_info.setTotalSoldTokens(token_info.getTotalSoldTokens() - tokenDTO.getSendToken());
				tokensInfoRepo.save(token_info);
			}
		} else {
			transactionHistory.setTxstatus("Failed");
			transactionInfoRepository.save(transactionHistory);
		}
		return transactionReceipt.toString();
	}

	@SuppressWarnings("unused")
	public String purchaseTokens(TokenDTO tokenDTO)
			throws FileNotFoundException, IOException, ParseException, Exception {
		Config configInfo = configrepo.findConfigByConfigKey("walletFile");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		if (registerInfo != null) {
			String eth = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());
			String ethPassword = EncryptDecrypt.decrypt(registerInfo.getEthWalletPassword());
			tokenDTO.setEthWalletAddress(eth);
			tokenDTO.setEthWalletPassword(ethPassword);
			String ethAddress = bioCoinUtils.getEtherWalletAddress(configInfo.getConfigValue(),
					tokenDTO.getEthWalletAddress());
			/*************** Using ETH for Purchase ****************/
			String purchase = "";
			if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("eth.payment"))) {
				System.out.println("Type of Purchase : " + tokenDTO.getTypeOfPurchase());
				purchase = ethPurchase(tokenDTO);
				// System.out.println("purchase : " + purchase);
				if (purchase != null) {
					System.out.println("purchase : " + purchase);
				}

			}
			/*************** Using BTC for Purchase ****************/
			else if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("btc.payment"))) {
				purchase = btcPurchase(tokenDTO);
				if (purchase != null) {
					System.out.println("purchase : " + purchase);
				}
			}
			/*************** Transferring the Purchase Tokens ****************/
			transactionReceipt = new TransactionReceipt();
			TransactionHistory transactionHistory = new TransactionHistory();
			if (purchase != null) {

				transactionHistory.setFromAddress(env.getProperty("main.address"));
				transactionHistory.setToAddress(ethAddress);
				transactionHistory.setCryptoAmount(tokenDTO.getAmount());
				transactionHistory.setTransferToken(tokenDTO.getTokenAmount());
				transactionHistory.setTxDate(new Date());
				transactionHistory.setPaymentMode("Purchase");
				transactionHistory.setEmailId(registerInfo.getEmailId());
				transactionHistory.setSenderId(1);
				transactionHistory.setReceiverId(registerInfo.getId());
				transactionHistory.setTxstatus("Pending");
				transactionHistory.setRegisterInfo(registerInfo);
				transactionHistory.setTypeOfVerifications(registerInfo.getTypeOfVerifications());
				transactionInfoRepository.save(transactionHistory);
				if (transactionHistory != null) {
					Credentials credentials = WalletUtils.loadCredentials(env.getProperty("credentials.password"),
							env.getProperty("credentials.address"));
					Biokkoin assetToken = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials,
							Contract.GAS_PRICE, Contract.GAS_LIMIT);
					if (assetToken != null) {
						LOG.info("Token Getting Amount" + tokenDTO.getTokenAmount());
						BigDecimal amount = BigDecimal.valueOf(tokenDTO.getTokenAmount())
								.multiply(new BigDecimal("100000000"));
						System.out.println("amount==>>" + tokenDTO.getTokenAmount());
						System.out.println("amount : " + BigDecimal.valueOf(tokenDTO.getTokenAmount()));
						transactionReceipt = assetToken.transfer(ethAddress, BigDecimal
								.valueOf(tokenDTO.getTokenAmount() * Double.valueOf("100000000")).toBigInteger())
								.send();

						LOG.info("transactionReceipt : " + transactionReceipt.getTransactionHash() + " : "
								+ transactionReceipt.getGasUsed() + " : " + transactionReceipt.getStatus() + " : "
								+ transactionReceipt.getCumulativeGasUsed());
					}
				}

				if (transactionReceipt.getStatus().equalsIgnoreCase("0x1")) {
					System.out.println("Inside Validations for Purchase Tokens after success");
					transactionHistory.setTxstatus("Success");
					transactionHistoryService.save(transactionHistory);

					Token_info token_info = tokensInfoRepo.findOne(1);
					token_info.setIcoSoldTokens(token_info.getIcoSoldTokens() + tokenDTO.getTokenAmount());
					token_info.setTotalSoldTokens(token_info.getTotalSoldTokens() + tokenDTO.getTokenAmount());

					token_info.setIcoAvailableTokens(token_info.getIcoAvailableTokens() - tokenDTO.getTokenAmount());
					token_info
							.setTotalAvailableTokens(token_info.getTotalAvailableTokens() - tokenDTO.getTokenAmount());
					tokensInfoRepo.save(token_info);

					ICOPeriodInfo icoPeriodInfo = icoPeriodInfoRepo.findICOPeriodInfoByicoStatus(1);

					icoPeriodInfo
							.setIcoAvailableTokens(icoPeriodInfo.getIcoAvailableTokens() - tokenDTO.getTokenAmount());
					icoPeriodInfo.setIcoSoldTokens(icoPeriodInfo.getIcoSoldTokens() + tokenDTO.getTokenAmount());
					icoPeriodInfoRepo.save(icoPeriodInfo);

					PurchaseTokenInfo purchaseTokenInfo = purchaseTokenInfoRepository.findOne(tokenDTO.getId());
					purchaseTokenInfo.setAsynchStatus("Success");
					purchaseTokenInfoRepository.save(purchaseTokenInfo);
				} else {
					transactionHistory.setTxstatus("Failed");
					transactionHistoryService.save(transactionHistory);

					PurchaseTokenInfo purchaseTokenInfo = purchaseTokenInfoRepository.findOne(tokenDTO.getId());
					purchaseTokenInfo.setAsynchStatus("Failed");
					purchaseTokenInfoRepository.save(purchaseTokenInfo);
				}

				return transactionReceipt.toString();
			}

		}
		return null;
	}

	private String btcPurchase(TokenDTO tokenDTO) throws NumberFormatException, Exception {
		/* Getting Value of the 1 USD rate of Bitcoin */
		Token_info token_info = tokensInfoRepo.findOne(1);

		RegisterInfo userModelInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());

		Double currentTokenRateInUSD = Double.parseDouble(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
		System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

		Double btcAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
		System.out.println("Current Rate of 1 USD to bitcoin : " + btcAmount);

		Double btcValueForOneToken = btcAmount * currentTokenRateInUSD;
		System.out.println("Rate of 1 Tokens in btc : " + btcValueForOneToken);

		Double bitcoinValue = tokenDTO.getTokenAmount() * btcValueForOneToken;

		System.out.println("current bitcoinValue Value : " + bitcoinValue);
		BigDecimal btc = new BigDecimal(bitcoinValue);

		System.out.println(btc);
		DecimalFormat df = new DecimalFormat("#.########");
		BigDecimal bitcoin = new BigDecimal(df.format(btc));
		tokenDTO.setAmount(bitcoin);
		PurchaseTokenInfo purchaseTokenInfo = new PurchaseTokenInfo();
		purchaseTokenInfo.setEmailId(tokenDTO.getEmailId());
		purchaseTokenInfo.setEtherWalletAddress("");
		purchaseTokenInfo.setCryptoAmount(bitcoin.doubleValue());
		purchaseTokenInfo.setPurchaseTokens(tokenDTO.getTokenAmount());
		purchaseTokenInfo.setTypeOfPurchase(env.getProperty("btc.payment"));
		purchaseTokenInfo.setPurchasedDate(new Date());
		purchaseTokenInfo.setTransferType(2);
		purchaseTokenInfo.setBtcWalletAddress(EncryptDecrypt.decrypt(userModelInfo.getBtcWalletAddress()));
		purchaseTokenInfo.setAsynchStatus("Pending");
		purchaseTokenInfoRepository.save(purchaseTokenInfo);

		if (purchaseTokenInfo.getId() != null) {
			String isTransfer = bitCoinUtils.btcSendCoins(tokenDTO);
			System.out.println("Status : " + isTransfer);
			LOG.info("Status BTC : " + isTransfer);
			if (isTransfer != null) {
				purchaseTokenInfo.setAsynchStatus("Pending");
				purchaseTokenInfoRepository.save(purchaseTokenInfo);
				tokenDTO.setId(purchaseTokenInfo.getId());
				return "Success";
			} else {
				purchaseTokenInfo.setAsynchStatus("Failed");
				purchaseTokenInfoRepository.save(purchaseTokenInfo);
			}

		}

		return null;
	}

	public String ethPurchase(TokenDTO tokenDTO) throws FileNotFoundException, IOException, ParseException, Exception {
		transactionReceipt = new TransactionReceipt();
		Config configInfo = configrepo.findConfigByConfigKey("walletFile");
		Credentials credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
				configInfo.getConfigValue() + tokenDTO.getEthWalletAddress());
		String ethAddress = bioCoinUtils.getEtherWalletAddress(configInfo.getConfigValue(),
				tokenDTO.getEthWalletAddress());
		/* Getting Value of the 1 USD rate of Ether */
		Token_info token_info = tokensInfoRepo.findOne(1);

		Double currentTokenRateInUSD = Double.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
		System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

		Double etherAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
		System.out.println("Current Rate of 1 USD to Ether : " + etherAmount);

		Double etherValueForOneToken = etherAmount * currentTokenRateInUSD;
		System.out.println("Rate of 1 Tokens in Ether : " + etherValueForOneToken);

		Double etherValue = tokenDTO.getTokenAmount() * etherValueForOneToken;

		System.out.println("current Ether Value : " + etherValue);
		BigDecimal eth = new BigDecimal(etherValue);

		System.out.println(eth);
		DecimalFormat df = new DecimalFormat("#.########");
		BigDecimal ether = new BigDecimal(df.format(eth));
		tokenDTO.setAmount(ether);
		PurchaseTokenInfo purchaseTokenInfo = new PurchaseTokenInfo();

		purchaseTokenInfo.setEmailId(tokenDTO.getEmailId());
		purchaseTokenInfo.setEtherWalletAddress(ethAddress);
		purchaseTokenInfo.setCryptoAmount(ether.doubleValue());
		purchaseTokenInfo.setPurchaseTokens(tokenDTO.getTokenAmount());
		purchaseTokenInfo.setTypeOfPurchase(env.getProperty("eth.payment"));
		purchaseTokenInfo.setPurchasedDate(new Date());
		purchaseTokenInfo.setTransferType(1);
		purchaseTokenInfo.setBtcWalletAddress("");
		purchaseTokenInfo.setPaymentId("");
		purchaseTokenInfo.setAsynchStatus("Pending");
		purchaseTokenInfoRepository.save(purchaseTokenInfo);

		if (purchaseTokenInfo.getId() != null) {
			transactionReceipt = Transfer
					.sendFunds(web3j, credentials, env.getProperty("main.address"), ether, Convert.Unit.ETHER).send();
			LOG.info("TransactionReceipt : " + transactionReceipt.getStatus());
		}

		if (transactionReceipt.getStatus().equals("0x1")) {
			purchaseTokenInfo.setAsynchStatus("Pending");
			purchaseTokenInfoRepository.save(purchaseTokenInfo);
			tokenDTO.setId(purchaseTokenInfo.getId());
			System.out.println("purchaseTokenInfo id : " + purchaseTokenInfo.getId());
		} else if (transactionReceipt.getStatus().equals("0x0")) {
			purchaseTokenInfo.setAsynchStatus("Failed");
			purchaseTokenInfoRepository.save(purchaseTokenInfo);
		}
		return transactionReceipt.getStatus();
	}

	public JSONObject purchaseTokensViaPaymentGateway(PaypalDTO paypalDTO) throws Exception {

		Config configInfo = configrepo.findConfigByConfigKey("walletFile");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(paypalDTO.getEmailId());
		if (registerInfo != null) {
			String eth = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());
			// paypalDTO.setEthWalletAddress(eth);
			String ethAddress = bioCoinUtils.getEtherWalletAddress(configInfo.getConfigValue(), eth);
			System.out.println(ethAddress);
			if (paypalDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("paypal.payment"))) {

				JSONObject data = null;
				String paymentId = null;
				if (paypalDTO.getCurrency().equalsIgnoreCase(env.getProperty("paypal.payment.usd"))) {

					/* Getting Value of the 1 USD rate of Ether */
					Token_info token_info = tokensInfoRepo.findOne(1);

					Double currentTokenRateInUSD = Double
							.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
					System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

					Double rateOfTokensInUSD = currentTokenRateInUSD * paypalDTO.getTokenAmount();

					// DecimalFormat df = new DecimalFormat("#.###");

					Integer value = rateOfTokensInUSD.intValue();
					System.out.println("value : " + value);

					paypalDTO.setTotal(value.toString());

					data = bioCoinUtils.createPayments(paypalDTO);

					System.out.println("Data : " + data);
					paymentId = (String) data.get("id");
					System.out.println("Payment ID : " + paymentId);

				} else if (paypalDTO.getCurrency().equalsIgnoreCase(env.getProperty("paypal.payment.inr"))) {

					/* Getting Value of the 1 USD rate of Ether */
					Token_info token_info = tokensInfoRepo.findOne(1);

					Double currentTokenRateInUSD = Double
							.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
					System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

					// INR conversion

					Double rateOfOneUSDInINR = currentValueUtils.getCurrencyRateInINR();
					System.out.println("Rate Of One USD In INR : " + rateOfOneUSDInINR);

					Double multiplyingWithUSDOfTokenRate = rateOfOneUSDInINR * currentTokenRateInUSD;

					Double rateOfTokensInINR = multiplyingWithUSDOfTokenRate * paypalDTO.getTokenAmount();

					// DecimalFormat df = new DecimalFormat("#.###");

					Integer value = rateOfTokensInINR.intValue();

					paypalDTO.setTotal(value.toString());

					data = bioCoinUtils.createPayments(paypalDTO);
					// JSONObject json = new JSONObject();
					paymentId = (String) data.get("id");
					System.out.println("Payment ID : " + paymentId);
				}

				PurchaseTokenInfo purchaseTokenInfo = new PurchaseTokenInfo();
				purchaseTokenInfo.setEmailId(paypalDTO.getEmailId());
				purchaseTokenInfo.setEtherWalletAddress(EncryptDecrypt.decrypt(registerInfo.getDummywalletaddress()));
				purchaseTokenInfo.setCryptoAmount(Double.valueOf(paypalDTO.getTotal()));
				purchaseTokenInfo.setPurchaseTokens(paypalDTO.getTokenAmount());
				purchaseTokenInfo.setTypeOfPurchase(paypalDTO.getCurrency());
				purchaseTokenInfo.setPurchasedDate(new Date());
				purchaseTokenInfo.setTransferType(3);
				purchaseTokenInfo.setPaymentId(paymentId);
				purchaseTokenInfo.setBtcWalletAddress("");
				purchaseTokenInfo.setAsynchStatus("Pending");
				purchaseTokenInfoRepository.save(purchaseTokenInfo);

				PaymentsInfo paymentsInfo = new PaymentsInfo();
				paymentsInfo.setCreate_time(new Date());
				paymentsInfo.setPaymentID(paymentId);
				paymentsInfo.setCurrency(paypalDTO.getCurrency());
				paymentsInfo.setTokenAmount(paypalDTO.getTokenAmount());
				paymentsInfo.setEmailId(paypalDTO.getEmailId());
				paymentsInfo.setAmount(paypalDTO.getTotal());
				paymentsInfo.setStatus("payment created");
				paymentsInfo.setEthAddress(EncryptDecrypt.encrypt(ethAddress));
				paymentInfoRepo.save(paymentsInfo);

				return data;
			}
		}
		return null;
	}

	public List<TokenDTO> transactionHistory(TokenDTO tokenDTO, String etherWalletAddress) throws Exception {

		System.out.println("Inside Tx History ");
		System.out.println("Inside Tx History Type : " + tokenDTO.getTransactionType());

		List<TokenDTO> transactionList = new ArrayList<TokenDTO>();
		if (tokenDTO.getTransactionType() == 4) {
			
			List<TransactionHistory> transactionHistory = transactionInfoRepository
					.findByFromAddressOrToAddressOrderByTxDateDesc(etherWalletAddress, etherWalletAddress);
			
			for (TransactionHistory transactionInfo : transactionHistory) {
				TokenDTO transactions = new TokenDTO();
				transactions.setFromAddress(transactionInfo.getFromAddress());
				transactions.setToAddress(transactionInfo.getToAddress());
				// transactions.setTransferAmount(transactionInfo.getCryptoAmount());
				transactions.setTxDate(transactionInfo.getTxDate());
				transactions.setPaymentMode(transactionInfo.getPaymentMode());
				transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
				transactions.setTxStatus(transactionInfo.getTxstatus());
				transactions.setTokens(transactionInfo.getTransferToken());

				transactionList.add(transactions);
			}
			
		} else if (tokenDTO.getTransactionType() == 3) {

			List<TransactionHistory> transactionHistory = transactionInfoRepository
					.findTop3ByFromAddressOrToAddressOrderByTxDateDesc(etherWalletAddress, etherWalletAddress);
			for (TransactionHistory transactionInfo : transactionHistory) {
				TokenDTO transactions = new TokenDTO();
				transactions.setFromAddress(transactionInfo.getFromAddress());
				transactions.setToAddress(transactionInfo.getToAddress());
				// transactions.setTransferAmount(transactionInfo.getCryptoAmount());
				transactions.setTxDate(transactionInfo.getTxDate());
				transactions.setPaymentMode(transactionInfo.getPaymentMode());
				transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
				transactions.setTxStatus(transactionInfo.getTxstatus());
				transactions.setTokens(transactionInfo.getTransferToken());

				transactionList.add(transactions);
			}

		} else {
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "txDate"));
			Pageable pageable = new PageRequest(tokenDTO.getPageNum(), tokenDTO.getPageSize(), sort);

			if (tokenDTO.getTransactionType() == 0) {
				System.out.println("Inside All history");
				Page<TransactionHistory> transactionHistory = transactionInfoRepository
						.findByFromAddressOrToAddressOrderByTxDateDesc(etherWalletAddress, etherWalletAddress,
								pageable);
				LOG.info("Transaction History:::::::::::" + transactionHistory.toString());
				tokenDTO.setTotalPages(transactionHistory.getTotalPages());
				System.out.println("transactionHistory.getTotalPages() : " + transactionHistory.getTotalPages()
						+ "TotalElements : " + transactionHistory.getTotalElements());
				tokenDTO.setTotalElements(Math.toIntExact(transactionHistory.getTotalElements()));

				for (TransactionHistory transactionInfo : transactionHistory) {
					System.out.println("Inside All history");
					TokenDTO transactions = new TokenDTO();
					transactions.setFromAddress(transactionInfo.getFromAddress());
					transactions.setToAddress(transactionInfo.getToAddress());
					// transactions.setTransferAmount(transactionInfo.getCryptoAmount());
					transactions.setTxDate(transactionInfo.getTxDate());
					transactions.setPaymentMode(transactionInfo.getPaymentMode());
					transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
					transactions.setTxStatus(transactionInfo.getTxstatus());
					transactions.setTokens(transactionInfo.getTransferToken());
					transactionList.add(transactions);
				}

			} else if (tokenDTO.getTransactionType() == 1) {
				Page<TransactionHistory> transactionHistory = transactionInfoRepository
						.findByFromAddressOrderByTxDateDesc(etherWalletAddress, pageable);
				LOG.info("Transaction History:::::::::::" + transactionHistory.toString());
				tokenDTO.setTotalPages(transactionHistory.getTotalPages());
				System.out.println("transactionHistory.getTotalPages() : " + transactionHistory.getTotalPages()
						+ "TotalElements : " + transactionHistory.getTotalElements());
				tokenDTO.setTotalElements(Math.toIntExact(transactionHistory.getTotalElements()));

				for (TransactionHistory transactionInfo : transactionHistory) {

					if (tokenDTO.getEthWalletAddress().equalsIgnoreCase(transactionInfo.getFromAddress())) {

						TokenDTO transactions = new TokenDTO();
						transactions.setFromAddress(transactionInfo.getFromAddress());
						transactions.setToAddress(transactionInfo.getToAddress());
						// transactions.setTransferAmount(transactionInfo.getCryptoAmount());
						transactions.setTxDate(transactionInfo.getTxDate());
						transactions.setPaymentMode(transactionInfo.getPaymentMode());
						transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
						transactions.setTxStatus(transactionInfo.getTxstatus());
						transactions.setTokens(transactionInfo.getTransferToken());
						transactionList.add(transactions);
					}
				}

			} else if (tokenDTO.getTransactionType() == 2) {
				Page<TransactionHistory> transactionHistory = transactionInfoRepository
						.findByToAddressOrderByTxDateDesc(etherWalletAddress, pageable);
				LOG.info("Transaction History:::::::::::" + transactionHistory.toString());
				tokenDTO.setTotalPages(transactionHistory.getTotalPages());
				System.out.println("transactionHistory.getTotalPages() : " + transactionHistory.getTotalPages()
						+ "TotalElements : " + transactionHistory.getTotalElements());
				tokenDTO.setTotalElements(Math.toIntExact(transactionHistory.getTotalElements()));

				for (TransactionHistory transactionInfo : transactionHistory) {

					if (tokenDTO.getEthWalletAddress().equalsIgnoreCase(transactionInfo.getToAddress())) {

						TokenDTO transactions = new TokenDTO();
						transactions.setFromAddress(transactionInfo.getFromAddress());
						transactions.setToAddress(transactionInfo.getToAddress());
						// transactions.setTransferAmount(transactionInfo.getCryptoAmount());
						transactions.setTxDate(transactionInfo.getTxDate());
						transactions.setPaymentMode(transactionInfo.getPaymentMode());
						transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
						transactions.setTxStatus(transactionInfo.getTxstatus());
						transactions.setTokens(transactionInfo.getTransferToken());
						transactionList.add(transactions);
					}
				}
			}
		}
		return transactionList;
	}
}
