package com.biocoin.service;

import java.io.Serializable;
import java.util.List;

public interface HibernateServiceDao<T, ID extends Serializable> {
	
	public List<T> findAll();
	
	public List<T> findLimitedRecords(int index);

	public List<T> findAll(String flag);
	
	public List<T> findLimitedRecordsByAscendingOrder(String field,int index);

	public T findById(ID id);

	public void add(T entity);
	
//	public Long SeralizeId(Object entity);

	public void update(T entity);

	public void delete(T entity);

	public void deleteById(ID id);
	
	public void saveOrUpdate(T entity);
	
	public boolean save(Object entity);
	
	public List<T> findAllByAscendingOrder(String field,String flag);
	
	public List<T> findAllByAscendingOrder(String field);
		
	public double getWorkingDays(String fromDate, String toDate);
	
	public double getfinYrDays(String fromDate);
	
	

	public List<Object[]> getPotentialMonths();

	/*public T find(Class<T> type, Serializable id);

	public T[] find(Class<T> type, Serializable... ids);

	public T getReference(Class<T> type, Serializable id);

	public T[] getReferences(Class<T> type, Serializable... ids);

	public boolean save(Object entity);

	public boolean[] save(Object... entities);

	public boolean remove(Object entity);

	public void remove(Object... entities);

	public boolean removeById(Class<?> type, Serializable id);

	public void removeByIds(Class<?> type, Serializable... ids);

	public List<T> findAll(Class<T> type);

	public List search(ISearch search);

	public Object searchUnique(ISearch search);

	public int count(ISearch search);

	public SearchResult searchAndCount(ISearch search);

	public boolean isAttached(Object entity);

	public void refresh(Object... entities);

	public void flush();

	public Filter getFilterFromExample(Object example);

	public Filter getFilterFromExample(Object example, ExampleOptions options);*/
}
