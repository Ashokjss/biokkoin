package com.biocoin.service;

import java.io.Serializable;

import org.springframework.stereotype.Service;

import com.biocoin.model.SecurityInfo;

@Service
public interface SecurityInfoService extends HibernateServiceDao<SecurityInfo, Serializable>{

	SecurityInfo getSecurityInfoByEmailId(String emailId);

}
