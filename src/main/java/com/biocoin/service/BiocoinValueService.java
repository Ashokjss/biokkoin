package com.biocoin.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.biocoin.dto.PaypalDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.BiocoinValue;


@Service
public interface BiocoinValueService extends HibernateServiceDao<BiocoinValue, Serializable>{

	public List<UserRegisterDTO> listAllocatedTokens(UserRegisterDTO userRegisterDTO);
	
	public List<UserRegisterDTO> listTokensRate(UserRegisterDTO userRegisterDTO) throws Exception;
	
	public boolean updateTokensRate(UserRegisterDTO userRegisterDTO) throws Exception;
	
	public List<UserRegisterDTO> listICOPeriod(UserRegisterDTO userRegisterDTO);
	
	public String updateICOPeriods(UserRegisterDTO userRegisterDTO);
	
	public boolean deleteICOPeriods(UserRegisterDTO userRegisterDTO);
	
	public boolean deleteAllocateTokens(UserRegisterDTO userRegisterDTO);
	
	public boolean UpdateTheICOPeriodSimultanously();
	
	public String tokenAmountValidationForPurchase(TokenDTO tokenDTO);
	
	public boolean userPurchaseTokens(TokenDTO tokenDTO) throws FileNotFoundException, IOException, ParseException, Exception;
	
	public String saveAllocateTokens(UserRegisterDTO userRegisterDTO);
	
	public List<TokenDTO> purchaseList(TokenDTO tokenDTO);
	
	public List<TokenDTO> purchaseListFilter(TokenDTO tokenDTO);
	
	public JSONObject userPurchaseTokensPaymentGateway(PaypalDTO paypalDTO) throws Exception;
	
	public String oauthToken(PaypalDTO paypalDTO) throws Exception;

	String tokenAmountValidationForPurchaseForPayPal(PaypalDTO paypalDTO);
	
}
