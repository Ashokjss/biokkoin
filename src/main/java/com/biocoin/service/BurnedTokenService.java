package com.biocoin.service;

import java.io.Serializable;
import org.springframework.stereotype.Service;
import com.biocoin.model.BurnedToken;

@Service
public interface BurnedTokenService extends HibernateServiceDao<BurnedToken, Serializable> {

}
