package com.biocoin.service;

import java.io.Serializable;

import org.springframework.stereotype.Service;

import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.ExpirationDataInfo;

@Service
public interface ExpirationDataService extends HibernateServiceDao<ExpirationDataInfo, Serializable>{

	boolean isSendEmail(UserRegisterDTO userregisterdto);

	ExpirationDataInfo getexpDataInfoByUseerEmailId(String emailId);
   
}
