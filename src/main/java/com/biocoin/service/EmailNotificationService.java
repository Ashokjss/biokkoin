package com.biocoin.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServlet;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.biocoin.dto.StatusResponseDTO;


@SuppressWarnings("serial")
@Service
public class EmailNotificationService extends HttpServlet {
	private static final Logger LOG = LoggerFactory.getLogger(EmailNotificationService.class);

	@SuppressWarnings("unused")
	@Autowired
	private JavaMailSender javaMailService;
	@Autowired
	private Environment envi;
	@Value("${email.host}")
	private String host;

	@Value("${email.port}")
	private Integer port;

	@Value("${email.username}")
	private String username;

	@Value("${email.password}")
	private String password;

	@Value("${spring.mail.transport.protocol}")
	private String transportProtocol;

	@Value("${env}")
	private String env;
	@Value("${email.path}")
	private String emailPath;
	@Value("${email.tokenpath}")
	private String tokenstatusemailPath;
	@Value("${email.tokenfailpath}")
	private String tokenstatusfailemailPath;
	// Supply your SMTP credentials below. Note that your SMTP credentials are
	// different from your AWS credentials.
	@Value("${smtp.username}")
	private String SMTP_USERNAME;

	@Value("${smtp.password}")
	private String SMTP_PASSWORD;

	public boolean sendEmail(String toEamilId, String subject, String content, String content1, String userName, String password, Integer id) {

		LOG.info("Before sending email  ENV : " + env);
		LOG.info("host  : " + host);
		LOG.info("port  : " + port);
		LOG.info("username  : " + username);
		LOG.info("transportProtocol  : " + transportProtocol);
		// DEV Env
		if (env.equalsIgnoreCase("dev")) {
			return sendEmailDEV(toEamilId, subject, content, content1, userName,password,id);
		}  else {
			return sendEmailDEV(toEamilId, subject, content, content1, userName, password,id);
		}

	}
	
	public boolean sendEmail(String toEamilId, String subject, String content) {

		LOG.info("Before sending email  ENV : " + env);
		LOG.info("host  : " + host);
		LOG.info("port  : " + port);
//		LOG.info("username  : " + username);
		LOG.info("transportProtocol  : " + transportProtocol);
		// DEV Env
		if (env.equalsIgnoreCase("dev")) {
			return sendEmailPROD(toEamilId, subject, content);
		}  else {
			return sendEmailPROD(toEamilId, subject, content);
		}

	}

	private boolean sendEmailDEV(String toEamilId, String subject, String content, String content1, String userName,
			String userPassword,Integer id) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		     statusResponseDTO.setMessage(envi.getProperty("verify.mailId"));
	
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEamilId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);
			// Session session = Session.getDefaultInstance(props);
			MimeMessage msg = new MimeMessage(session);

			// creates a new e-mail message
			msg.setFrom(new InternetAddress(username));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEamilId));
			msg.setSubject(subject);
			MimeMultipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();

			Map<String, String> input = new HashMap<String, String>();
			input.put("tushar.goyal@mansudsales.com", toEamilId);
			input.put("newuser", userName);
//			input.put("123456", userPassword);
			input.put("5KW2X105F1NE1BA1Y5X24HS4L3C27DEC", content);
			input.put("bitcoin", content1);
			input.put("activateLink", statusResponseDTO.getMessage()+id);
			
			System.out.println();
			
			
			input.put("actMessage", ("Click here to activate your account"));
			
			String htmlText = readEmailFromHtml(emailPath, input);
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();

			
			System.out.println("Mail sent successfully...");

			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}
	}

	private boolean sendEmailPROD(String toEamilId, String subject, String content) {
		Transport transport = null;
		try {
			LOG.info("In  sendEmail PROD  START: " + env);
			
			// Create a Properties object to contain connection configuration
			// information.
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", transportProtocol);
			props.put("mail.smtp.port", port);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");

			// Create a Session object to represent a mail session with the
			// specified properties.
			Session session = Session.getDefaultInstance(props);
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			// Use to send an single 'TO' Recipient
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEamilId)); 
			msg.setSubject(subject);
			msg.setContent(content, "text/html");

			/*
			 * // Add multiple 'TO' Recipient in the Email msg =
			 * addMultipleMailId(msg, cc_SendAddress, Message.RecipientType.TO);
			 * 
			 * // Add multiple 'CC' Recipient in the Email msg =
			 * addMultipleMailId(msg, cc_SendAddress, Message.RecipientType.CC);
			 */

			// Create a transport
			transport = session.getTransport();
			LOG.info("Attempting to send an email through the Amazon SES SMTP interface...");
			transport.connect(host, SMTP_USERNAME, SMTP_PASSWORD);
			transport.sendMessage(msg, msg.getAllRecipients());
			LOG.info("Email sent successfully !");
			transport.close();

			LOG.info("In  sendEmail PROD  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmail PROD  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmail PROD  END: " + env);
			e.printStackTrace();
			return false;
		}

	}
	// Method to replace the values for keys
		protected String readEmailFromHtml(String filePath, Map<String, String> input) {
			String msg = readContentFromFile(filePath);
			try {
				Set<Entry<String, String>> entries = input.entrySet();
				for (Map.Entry<String, String> entry : entries) {
					msg = msg.replace(entry.getKey().trim(), entry.getValue().trim());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return msg;
		}

	private String readContentFromFile(String fileName) {
		StringBuffer contents = new StringBuffer();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			try {
				String line = null;
				while ((line = reader.readLine()) != null) {
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
					// System.out.println("hello");
				}
			} finally {
				reader.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return contents.toString();
	}
	public boolean sendEmailforgot(String toEamilId, String subject, String content) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEamilId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			InternetAddress[] toAddresses = { new InternetAddress(toEamilId) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			
			String message = "<b>Hi,</b><br><br>";
	        message += "<b>Please find your login credentials for Biokkoin.</b><br>";
	        message += "<font color=red> Your  password: "+content +"</font><br><br>";
			message += "<b>Thank you,</b><br>";
			message += "<b>Biokkoin team.</b>";
			
			helper.setText(message);
			// set plain text message
			msg.setContent(message, "text/html");
			// sends the e-mail
			
			
			
			
			LOG.info("Attempting to send an email");
			
			Transport.send(msg);
			
			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean sendEmailScocial(String toEmailId, String subject, String content) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			System.out.println("toEmailId"+toEmailId);
			System.out.println("subject "+subject);
			System.out.println("content "+content);
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEmailId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			InternetAddress[] toAddresses = { new InternetAddress(toEmailId) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setText(content);
			// set plain text message
			msg.setContent(content, "text/html");
			// sends the e-mail
			LOG.info("Attempting to send an email");
			
			Transport.send(msg);
			
			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}
	}
	public boolean sendEmailTokenTransfer(String toEmailId, String subject, String content) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			System.out.println("toEmailId"+toEmailId);
			System.out.println("subject "+subject);
			System.out.println("content "+content);
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEmailId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			InternetAddress[] toAddresses = { new InternetAddress(toEmailId) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setText(content);
			// set plain text message
			msg.setContent(content, "text/html");
			// sends the e-mail
			LOG.info("Attempting to send an email");
			
			Transport.send(msg);
			
			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}
	}

	public boolean sendTransferCoinEmail(String emailId, String alertmsg, String fromadd, String toadd,
			BigInteger reqToken, BigDecimal amt, Date date, String selectTransactionType,String status) {
		// TODO Auto-generated method stub
		LOG.info("Before sending email  ENV : " + env);
		LOG.info("host  : " + host);
		LOG.info("port  : " + port);
		LOG.info("username  : " + username);
		LOG.info("transportProtocol  : " + transportProtocol);
		// DEV Env
		if (env.equalsIgnoreCase("dev")) {
			
			if(status.equalsIgnoreCase("S")) {
				return sendSuccessTransferEmail(emailId, alertmsg, fromadd, toadd,reqToken,amt,date,selectTransactionType,status);
			}
			else {
				return sendFailTransferEmail(emailId, alertmsg, fromadd, toadd,reqToken,amt,date,selectTransactionType,status);
			}
			
		}
		return false;  
	}

	private boolean sendFailTransferEmail(String emailId, String alertmsg, String fromadd, String toadd,
			BigInteger reqToken, BigDecimal amt, Date date, String selectTransactionType, String status) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(emailId);
			mail.setFrom(username);
			mail.setSubject(alertmsg);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);
			// Session session = Session.getDefaultInstance(props);
			MimeMessage msg = new MimeMessage(session);

			// creates a new e-mail message
			msg.setFrom(new InternetAddress(username));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailId));
			msg.setSubject(alertmsg);
			MimeMultipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();

			Map<String, String> input = new HashMap<String, String>();
			input.put("usermailid", emailId);
			input.put("fromadd", fromadd);
			input.put("toadd", toadd);
			input.put("reqToken", reqToken.toString());
			input.put("amt", amt.toString());
			input.put("selectTransactionType",selectTransactionType);
			
			System.out.println();
			
			
			input.put("actMessage", ("Click here to activate your account"));
			
			String htmlText = readEmailFromHtml(tokenstatusemailPath, input);
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();

			
			System.out.println("Mail sent successfully...");

			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}
	}

	private boolean sendSuccessTransferEmail(String emailId, String alertmsg, String fromadd, String toadd,
			BigInteger reqToken, BigDecimal amt, Date date, String selectTransactionType, String status) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(emailId);
			mail.setFrom(username);
			mail.setSubject(alertmsg);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);
			// Session session = Session.getDefaultInstance(props);
			MimeMessage msg = new MimeMessage(session);

			// creates a new e-mail message
			msg.setFrom(new InternetAddress(username));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailId));
			msg.setSubject(alertmsg);
			MimeMultipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();

			Map<String, String> input = new HashMap<String, String>();
			input.put("usermailid", emailId);
			input.put("fromadd", fromadd);
			input.put("toadd", toadd);
			input.put("reqToken", reqToken.toString());
			input.put("amt", amt.toString());
			input.put("selectTransactionType",selectTransactionType);
			
			System.out.println();
			
			
			input.put("actMessage", ("Click here to activate your account"));
			
			String htmlText = readEmailFromHtml(tokenstatusemailPath, input);
			messageBodyPart.setContent(htmlText, "text/html");
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);
			Transport transport = session.getTransport("smtp");
			transport.connect(host, username, password);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();

			
			System.out.println("Mail sent successfully...");

			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}

	}
	
	public boolean sendEmailforOTP(String toEamilId, String subject, int pin) {
		try {
			LOG.info("In  sendEmailDEV  START: " + env);
			
			SimpleMailMessage mail = new SimpleMailMessage();
			mail.setTo(toEamilId);
			mail.setFrom(username);
			mail.setSubject(subject);
			Properties properties = new Properties();
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", port);
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.ssl.trust", host);

			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};

			Session session = Session.getInstance(properties, auth);

			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(username));
			InternetAddress[] toAddresses = { new InternetAddress(toEamilId) };
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			
			String message = "<b>Hi,</b><br><br>";
	        message += "<b>One Time Password for Biokkoin Login is " + pin + ". Please use this PIN to complete the Login. Please do not share this with anyone.</b><br>";
	        message += "<br><br>";
			message += "<b>Thank you,</b><br>";
			message += "<b>Biokkoin team.</b>";
			
			helper.setText(message);
			// set plain text message
			msg.setContent(message, "text/html");
			// sends the e-mail
			
			
			
			
			LOG.info("Attempting to send an email");
			
			Transport.send(msg);
			
			LOG.info("Email sent successfully !");
			LOG.info("In  sendEmailDEV  END: " + env);
			return true;
		} catch (MailException e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			LOG.info("Problem in sending mail sendEmailDEV  END: " + env);
			e.printStackTrace();
			return false;
		}
	}
}
