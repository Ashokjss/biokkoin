package com.biocoin.service;

import java.io.Serializable;
import org.springframework.stereotype.Service;
import com.biocoin.model.MintedToken;

@Service
public interface MintedTokenService  extends HibernateServiceDao<MintedToken, Serializable>{

}
