package com.biocoin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.biocoin.dto.TokenDTO;
import com.biocoin.model.TransactionHistory;

@Service
public interface TransactionHistoryService extends HibernateServiceDao<TransactionHistory, Integer>{

	public List<TokenDTO> transactionHistory(TokenDTO tokenDTO, String etherWalletAddress) throws Exception;
	
	public List<TokenDTO> transactionHistoryFilter(TokenDTO tokenDTO);

//	List<TokenDTO> txHistory(TokenDTO tokenDTO) throws Exception;
//	List<TransactionHistory> getTransactionHistory(Integer userId) ;
//	
//	List<TransactionHistory> getAllTransactionHistory(String etherwalletAddress);
//	List<TransactionHistory> getSendTransactionHistory(String etherwalletAddress);
//	List<TransactionHistory> getReceiveTransactionHistory(String etherwalletAddress);

}
