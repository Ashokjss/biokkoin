package com.biocoin.service;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.web3j.crypto.CipherException;
import com.biocoin.dto.TokenDTO;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.Token_info;


@Service
public interface TokenService extends HibernateServiceDao<Token_info, Serializable>{
	
	public boolean validAmount(TokenDTO tokenDTO) throws Exception;
	
	public String burnToken(TokenDTO tokenDTO) throws IOException, CipherException, Exception;
	
	public BigInteger tokenBalance(TokenDTO tokenDTO) throws Exception;
	
	public String mintToken(TokenDTO tokenDTO)throws IOException, CipherException, Exception;
	
	public String isValidTokenBalForTokenTransfer(TokenDTO tokenDTO);
	
	public String manualTokenTransferForAdmin(TokenDTO tokenDTO) throws Exception;
	
	public boolean validatingAndUpdating(TokenDTO tokenDTO) throws Exception;
	
	public String ValidateInputParamsOfApproval(TokenDTO tokenDTO) throws Exception;
	
	public Token_info getCountCurrentDateBetweenIcoDate(Date startDate, Date endDate);
	
	void requestcoinPushNotification(TokenDTO tokenDTO) throws Exception;
	
	void sendETHCoinPushNotificationFrom(RegisterInfo registerInfo, TokenDTO tokenDTO);
	
	void sendETHCoinPushNotificationTo(RegisterInfo registerInfo, TokenDTO tokenDTO);
	
	public boolean checkMainbalance(TokenDTO tokenDTO);
	
	public String manualTokenTransferToUsers(TokenDTO tokenDTO) throws Exception;
	
	public String validateTypeOfPersons(TokenDTO tokenDTO) throws Exception;
	
} 
