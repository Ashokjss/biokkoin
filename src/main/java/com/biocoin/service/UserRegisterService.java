package com.biocoin.service;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import com.biocoin.dto.KycDTO;
import com.biocoin.dto.LoginDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.RegisterInfo;

@Service
public interface UserRegisterService extends HibernateServiceDao<RegisterInfo, Serializable> {
	
	public KycDTO getKycDetails(TokenDTO tokenDTO);

	public boolean updateKYC(KycDTO kycDTO);
	
	public List<KycDTO> kycList();
	
	RegisterInfo isAccountExistCheckByEmailId(String emailId);

	boolean saveAccountInfo(UserRegisterDTO userRegisterDTO) throws Exception;
	
	public KycDTO saveKycInfo(KycDTO kycDTO, String kycDoc1FilePath, String kycDoc2FilePath, String kycDoc3FilePath, String kycDoc4FilePath, MultipartFile kycDoc1, MultipartFile kycDoc2, MultipartFile kycDoc3, MultipartFile kycDoc4);
	

	public String userDocumentUpload(MultipartFile uploadedFileRef, String emailId,String docType) throws Exception;
	String isEmailVerified(UserRegisterDTO userRegisterDTO);

	LoginDTO isEmailIdAndPasswordExist(UserRegisterDTO userRegisterDTO, HttpServletRequest request,
			String encryptPassword) throws Exception;

	RegisterInfo getRegisterInfoByEmailIdAndPassword(String emailId, String Password);

	public boolean isOldPassword(UserRegisterDTO userRegisterDTO) throws Exception;

	public boolean isChangePassword(UserRegisterDTO userRegisterDTO) throws Exception;

	long getTotalUserCount();

	String isAccountHolderActiveOrNot(UserRegisterDTO userRegisterDTO) throws Exception;

	List<UserRegisterDTO> userList(UserRegisterDTO userRegisterDTO) throws Exception;

//	List<RegisterInfo> registerUserList();

	public boolean getUserInfo(UserRegisterDTO userRegisterDTO) throws Exception;

	public List<KycDTO> filterKyCStatus(KycDTO kycDTO);

	public List<KycDTO> getFeedItemsForKycUsers(Pageable pageRequest);

	public List<KycDTO> getFeedItemsForKycUsers(Integer kycStatus, Pageable pageRequest);

	public List<KycDTO> getFeedItemsForKycUsers(String fullName, Pageable pageRequest);

	public List<KycDTO> getFeedItemsForKycUsersByEmail(String emailId, Pageable pageRequest);

	public TokenDTO getKycCount(TokenDTO tokenDTO);

	public KycDTO getKycCountByStatus(KycDTO kycDTO);

	public KycDTO getKycCountByUsername(KycDTO kycDTO);

	public KycDTO getKycCountByEmailid(KycDTO kycDTO);

	public List<KycDTO> getFeedItemsForKycUsersByUsers(String filterKyc, Pageable pageRequest);

	public BigDecimal getBtcBalance(TokenDTO tokenDTO) throws IOException, ParseException, Exception;

	

}
