package com.biocoin.service.impl;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.ExpirationDataInfo;
import com.biocoin.model.RegisterInfo;
import com.biocoin.service.ExpirationDataService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.utils.EncryptDecrypt;

@Service
public class ExpirationDataServiceImpl extends HibernateServiceDaoImpl<ExpirationDataInfo, Serializable>
		implements ExpirationDataService {

	@Autowired
	Environment env;
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	EmailNotificationServiceImpl emailnotificationserviceimpl;
	@Autowired
	ExpirationDataService expirationDataService;
	@Autowired
	UserRegisterService userRegisterService;
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean isSendEmail(UserRegisterDTO userregisterdto) {

		// UserModelInfo artcoinUserModelInfo
		// =registrationrepo.findUserModelInfoByEmail(artcoinUserDTO.getEmail());

		RegisterInfo registrationObj = userRegisterService.isAccountExistCheckByEmailId(userregisterdto.getEmailId());

		if (registrationObj != null) {
			try {
				ExpirationDataInfo expirationDataInfo = expirationDataService
						.getexpDataInfoByUseerEmailId(userregisterdto.getEmailId());
				String token = UUID.randomUUID().toString();

				if (expirationDataInfo != null) {

					// ExpirationDataInfo expirationDataInfo = new
					// ExpirationDataInfo();

					token = UUID.randomUUID().toString();

					expirationDataInfo.setToken(token);
					Calendar cal = Calendar.getInstance();// This will produce
															// the current time
															// (cal.get
															// time)------ Tue
															// May 01 19:05:34
															// EEST 2012
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
					cal.add(Calendar.HOUR_OF_DAY, 1);
					Date expiredDate = new Date(dateFormat.format(cal.getTime()));

					expirationDataInfo.setExpiredDate(expiredDate);
					expirationDataInfo.setTokenStatus(false);

					expirationDataService.update(expirationDataInfo);
				} else {

					ExpirationDataInfo expirationDataInfo1 = new ExpirationDataInfo();

					expirationDataInfo1.setToken(token);
					Calendar cal = Calendar.getInstance();// This will produce
															// the current time
															// (cal.get
															// time)------ Tue
															// May 01 19:05:34
															// EEST 2012
					DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
					cal.add(Calendar.HOUR_OF_DAY, 1);
					Date expiredDate = new Date(dateFormat.format(cal.getTime()));

					expirationDataInfo1.setExpiredDate(expiredDate);
					expirationDataInfo1.setTokenStatus(false);
					expirationDataInfo1.setEmailId(userregisterdto.getEmailId());

					expirationDataService.save(expirationDataInfo1);
				}
				String encryptedEmail = EncryptDecrypt.encrypt(userregisterdto.getEmailId());
				String encryptedToken = EncryptDecrypt.encrypt(token);

				String verificationLink = "Hi " + registrationObj.getFirstName() + "," + "<br><br>" + env.getProperty("email.forgot.password") + "<br><br>"
						+ "<a href='" + env.getProperty("reset.url") + encryptedEmail + env.getProperty("reset.url2") + encryptedToken +"'>"
						+ env.getProperty("reset.user.portal.url") + "</a>" + "<br><br>" + "- "
						+ env.getProperty("biokkoin.team");

				boolean isEmailSent = emailnotificationserviceimpl.sendEmail(userregisterdto.getEmailId(),
						"Resetting your Biokkoin password", verificationLink);
				if (isEmailSent) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return false;
	}

	@Transactional
	public ExpirationDataInfo getexpDataInfoByUseerEmailId(String emailId) {
		// TODO Auto-generated method stub

		org.hibernate.Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from ExpirationDataInfo where emailId=:emailId").setParameter("emailId",
				emailId);

		return (ExpirationDataInfo) query.uniqueResult();

	}

}
