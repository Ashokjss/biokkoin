package com.biocoin.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.biocoin.dto.TokenDTO;
import com.biocoin.model.TransactionHistory;
import com.biocoin.repo.TransactionInfoRepository;
import com.biocoin.service.TransactionHistoryService;
import com.biocoin.solidityHandler.SolidityHandler;

@Service
public class TransactionHistoryServiceImpl implements TransactionHistoryService {

	@Autowired
	private SolidityHandler solidityHandler;
	@Autowired
	private TransactionInfoRepository transactionInfoRepository;

	@Override
	public List<TokenDTO> transactionHistory(TokenDTO tokenDTO, String etherWalletAddress) throws Exception {
		List<TokenDTO> transactionLists = solidityHandler.transactionHistory(tokenDTO, etherWalletAddress);
		
		return transactionLists;
	}
	
	@Override
	public List<TokenDTO> transactionHistoryFilter(TokenDTO tokenDTO) {
		
		List<TokenDTO> filter = new ArrayList<TokenDTO>();
		String etherWalletAddress = tokenDTO.getEthWalletAddress();
		if(tokenDTO.getSearchEtherWalletAddress() != null) {
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "txDate"));
			Pageable pageable = new PageRequest(tokenDTO.getPageNum(), tokenDTO.getPageSize(), sort);
			Page<TransactionHistory> transactionHistory = transactionInfoRepository.findByWalletAddress(etherWalletAddress, etherWalletAddress, tokenDTO.getSearchEtherWalletAddress(), tokenDTO.getSearchEtherWalletAddress(), pageable);
			tokenDTO.setTotalPages(transactionHistory.getTotalPages());
			System.out.println("transactionHistory.getTotalPages() : " + transactionHistory.getTotalPages() + "TotalElements : " + transactionHistory.getTotalElements());
			tokenDTO.setTotalElements(Math.toIntExact(transactionHistory.getTotalElements()));
			
			for(TransactionHistory transactionInfo : transactionHistory) {
				TokenDTO transactions = new TokenDTO();
				transactions.setFromAddress(transactionInfo.getFromAddress());
				transactions.setToAddress(transactionInfo.getToAddress());
//				transactions.setTransferAmount(transactionInfo.getCryptoAmount());
				transactions.setTxDate(transactionInfo.getTxDate());
				transactions.setPaymentMode(transactionInfo.getPaymentMode());
				transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
				transactions.setTxStatus(transactionInfo.getTxstatus());
				transactions.setTokens(transactionInfo.getTransferToken());
				filter.add(transactions);
			}
		} else if(tokenDTO.getSearchStatus() != null) {
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "txDate"));
			Pageable pageable = new PageRequest(tokenDTO.getPageNum(), tokenDTO.getPageSize(), sort);
			Page<TransactionHistory> transactionHistory = transactionInfoRepository.findByTxstatusLike(etherWalletAddress, etherWalletAddress, tokenDTO.getSearchStatus(), pageable);
			tokenDTO.setTotalPages(transactionHistory.getTotalPages());
			System.out.println("transactionHistory.getTotalPages() : " + transactionHistory.getTotalPages() + "TotalElements : " + transactionHistory.getTotalElements());
			tokenDTO.setTotalElements(Math.toIntExact(transactionHistory.getTotalElements()));
			
			for(TransactionHistory transactionInfo : transactionHistory) {				
				TokenDTO transactions = new TokenDTO();
				transactions.setFromAddress(transactionInfo.getFromAddress());
				transactions.setToAddress(transactionInfo.getToAddress());
//				transactions.setTransferAmount(transactionInfo.getCryptoAmount());
				transactions.setTxDate(transactionInfo.getTxDate());
				transactions.setPaymentMode(transactionInfo.getPaymentMode());
				transactions.setTypeOfVerifications(transactionInfo.getTypeOfVerifications());
				transactions.setTxStatus(transactionInfo.getTxstatus());
				transactions.setTokens(transactionInfo.getTransferToken());
				filter.add(transactions);
			}
		}		
		return filter;
	}
	
	
//	@SuppressWarnings("unchecked")
//	@Transactional
//	public List<TransactionHistory> getTransactionHistory(Integer userId) {
//		// TODO Auto-generated method stub
//		org.hibernate.Session session = sessionFactory.getCurrentSession();
//		Query query = session.createQuery("from TransactionHistory where registerInfo.id=:userId");
//		query.setParameter("userId", userId);
//
//		return query.list();
//	}
//
//	@Override
//	public List<TokenDTO> txHistory(TokenDTO tokenDTO) throws Exception {
//		// TODO Auto-generated method stub
//		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
//		String mail = (String) session.getAttribute("emailId");
//		RegisterInfo regInfo = userRegisterService.isAccountExistCheckByEmailId(mail);
//
//		String getAddress = EncryptDecrypt.decrypt(regInfo.getEthWalletAddress());
//
//		String[] fetchAddress = getAddress.split("--");
//		String etherwalletAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];
//
//		List<TransactionHistory> txList;
//
//		List<TokenDTO> transactionList = new ArrayList<TokenDTO>();
//
//		if (tokenDTO.getTxTpe().equalsIgnoreCase("A")) {
//			txList = transactionHistoryServicel
//					.getAllTransactionHistory(EncryptDecrypt.encrypt("0x" + etherwalletAddress));
//		} else if (tokenDTO.getTxTpe().equalsIgnoreCase("S")) {
//			txList = transactionHistoryServicel
//					.getSendTransactionHistory(EncryptDecrypt.encrypt("0x" + etherwalletAddress));
//		} else {
//			txList = transactionHistoryServicel
//					.getReceiveTransactionHistory(EncryptDecrypt.encrypt("0x" + etherwalletAddress));
//		}
//
//		for (TransactionHistory transactionHistoryInfo : txList) {
//			TokenDTO transaction = bioCoinUtils.listTransactions(transactionHistoryInfo);
//			transactionList.add(transaction);
//		}
//
//		return transactionList;
//	}
//
//	@Transactional
//	public List<TransactionHistory> getAllTransactionHistory(String etherwalletAddress) {
//		// TODO Auto-generated method stub
//		// org.hibernate.Session session = sessionFactory.getCurrentSession();
//		// Query query =session.createQuery("from TransactionHistory where
//		// (fromAddress=:etherwalletAddress) or
//		// (toAddress=:etherwalletAddress)");
//		// query.setParameter("etherwalletAddress",etherwalletAddress);
//		List<TransactionHistory> transactionInfos = (List<TransactionHistory>) transactionInfoRepository
//				.findByFromAddressOrToAddressOrderByTxDateDesc(etherwalletAddress, etherwalletAddress);
//		return transactionInfos;
//	}
//
//	@Transactional
//	public List<TransactionHistory> getSendTransactionHistory(String etherwalletAddress) {
//		// TODO Auto-generated method stub
//		// org.hibernate.Session session = sessionFactory.getCurrentSession();
//		// Query query =session.createQuery("from TransactionHistory where
//		// (fromAddress=:etherwalletAddress) or
//		// (toAddress=:etherwalletAddress)");
//		// query.setParameter("etherwalletAddress",etherwalletAddress);
//		// return query.list();
//		List<TransactionHistory> transactionInfos = (List<TransactionHistory>) transactionInfoRepository
//				.findByFromAddressOrderByTxDateDesc(etherwalletAddress);
//		return transactionInfos;
//	}
//
//	@Transactional
//	public List<TransactionHistory> getReceiveTransactionHistory(String etherwalletAddress) {
//		// TODO Auto-generated method stub
//		// org.hibernate.Session session = sessionFactory.getCurrentSession();
//		// System.out.println("etherwalletAddress=="+etherwalletAddress);
//		// Query query =session.createQuery("from TransactionHistory where
//		// (fromAddress=:etherwalletAddress) or
//		// (toAddress=:etherwalletAddress)");
//		// query.setParameter("etherwalletAddress",etherwalletAddress);
//		// return query.list();
//		List<TransactionHistory> transactionInfos = (List<TransactionHistory>) transactionInfoRepository
//				.findByToAddressOrderByTxDateDesc(etherwalletAddress);
//		return transactionInfos;
//	}
//
	@Override
	public List<TransactionHistory> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionHistory> findLimitedRecords(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionHistory> findAll(String flag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionHistory> findLimitedRecordsByAscendingOrder(String field, int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TransactionHistory findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(TransactionHistory entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TransactionHistory entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TransactionHistory entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveOrUpdate(TransactionHistory entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean save(Object entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<TransactionHistory> findAllByAscendingOrder(String field, String flag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TransactionHistory> findAllByAscendingOrder(String field) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getWorkingDays(String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getfinYrDays(String fromDate) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Object[]> getPotentialMonths() {
		// TODO Auto-generated method stub
		return null;
	}

}
