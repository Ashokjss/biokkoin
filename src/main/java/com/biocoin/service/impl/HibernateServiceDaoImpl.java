package com.biocoin.service.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.biocoin.service.HibernateServiceDao;






@SuppressWarnings(value = "unchecked")
public class HibernateServiceDaoImpl<T, ID extends Serializable> implements HibernateServiceDao<T, ID> {
	
	protected Class<T> persistentClass;
	public HibernateServiceDaoImpl() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public List<T> findAll() {
		return getSessionFactory().getCurrentSession().createCriteria(persistentClass).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	
	@Transactional
	public List<T> findLimitedRecords(int index) {
		return getSessionFactory().getCurrentSession().createCriteria(persistentClass).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).setFirstResult(index).setMaxResults(10).list();
	}
	
	@Transactional()
	public List<T> findAll(String flag) {
		return getSessionFactory().getCurrentSession().createCriteria(persistentClass).add(Restrictions.eq("status", flag)).list();
	}
	
	
	@Transactional()
	public List<T> findLimitedRecordsByAscendingOrder(String field,int index) {
		return getSessionFactory().getCurrentSession().createCriteria(persistentClass).addOrder(Order.asc(field)).setFirstResult(index).setMaxResults(10).list();
	}
	


	@Transactional
	public List<T> findAllByAscendingOrder(String field, String flag) {
		return getSessionFactory().getCurrentSession().createCriteria(persistentClass).add(Restrictions.eq("status", flag)).addOrder(Order.asc(field)).list();
	}
	
	@Transactional
	public List<T> findAllByAscendingOrder(String field) {
		return getSessionFactory().getCurrentSession().createCriteria(persistentClass).addOrder(Order.asc(field)).list();
	}
	
	@Transactional
	public T findById(ID id) {
		return (T) getSessionFactory().getCurrentSession().get(persistentClass, id);
	}

	@Transactional
	public void add(T entity) {
		getSessionFactory().getCurrentSession().save(entity);
	}
	
	@Transactional
	public boolean save(Object entity) {
		// TODO Auto-generated method stub
		getSessionFactory().getCurrentSession().save(entity);
		return false;
	}

	@Transactional
	public void update(T entity) {
		getSessionFactory().getCurrentSession().update(entity);
	}

	@Transactional
	public void delete(T entity) {
		getSessionFactory().getCurrentSession().delete(entity);
	}

	@Transactional
	public void deleteById(ID id) {
		delete(findById(id));
	}

	@Transactional
	public void saveOrUpdate(T entity) {
		getSessionFactory().getCurrentSession().saveOrUpdate(entity);
		
	}

	@Transactional
	public List<Object[]> getPotentialMonths(){
		return getSessionFactory().getCurrentSession().createSQLQuery("select to_char(trunc(SYSDATE), 'Mon'),"
																	+ "to_char(add_months(trunc(SYSDATE), -1), 'Mon'),"
																	+ "to_char(add_months(trunc(SYSDATE), -2), 'Mon'),"
																	+ "to_char(add_months(trunc(SYSDATE), -3), 'Mon')from dual").list();
	}

	@Transactional
	public double getWorkingDays(String fromDate, String toDate){
		String workquery= "select decode(COUNT(allobj),0,1,COUNT(allobj)) from (select to_date('"+fromDate+"','dd/mm/rrrr')+rownum-1 allobj from all_objects "
						+ "where rownum<= (select to_date('"+toDate+"','dd/mm/rrrr')+1-to_date('"+fromDate+"','dd/mm/rrrr') from dual)) where trim(to_char(allobj,'DAY')) <>'SUNDAY'";
		Query qry = getSessionFactory().getCurrentSession().createSQLQuery(workquery);
		return ((BigDecimal)qry.uniqueResult()).doubleValue();
	}
	
	@Transactional
	public double getfinYrDays(String fromDate){
		String finYrQuery="select COUNT(allobj) from (select to_date(microsharp.given_date_Fin_start_date(to_date('"+fromDate+"','dd/mm/rrrr')),'dd/mm/rrrr')+rownum allobj "
						+ "from all_objects where	rownum<= (select to_date(microsharp.given_date_Fin_end_date(to_date('"+fromDate+"','dd/mm/rrrr')),'dd/mm/rrrr') "
						+ "- to_date(microsharp.given_date_Fin_start_date(to_date('"+fromDate+"','dd/mm/rrrr')),'dd/mm/rrrr') from dual)) where trim(to_char(allobj,'DAY')) <>'SUNDAY'";
		Query qry = getSessionFactory().getCurrentSession().createSQLQuery(finYrQuery);
		return ((BigDecimal)qry.uniqueResult()).doubleValue();
	}

	
	
	/*@Override
	public T find(Class<T> type, Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T[] find(Class<T> type, Serializable... ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T getReference(Class<T> type, Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T[] getReferences(Class<T> type, Serializable... ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(Object entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean[] save(Object... entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void remove(Object... entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean removeById(Class<?> type, Serializable id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeByIds(Class<?> type, Serializable... ids) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<T> findAll(Class<T> type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAttached(Object entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void refresh(Object... entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
	}*/

}
