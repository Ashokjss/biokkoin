package com.biocoin.service.impl;

import static com.biocoin.utils.BioCoinConstants.ACTIVITY_FEED_PAGE_SIZE;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.web3j.crypto.WalletUtils;

import com.biocoin.controller.UserRegisterController;
import com.biocoin.dto.LoginDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.Config;
import com.biocoin.model.QRCode;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.SecurityInfo;
import com.biocoin.repo.ConfigInfoRepository;
import com.biocoin.repo.QRCodeRepository;
import com.biocoin.repo.UserRegisterRepository;
import com.biocoin.service.EmailNotificationService;
import com.biocoin.service.ExpirationDataService;
import com.biocoin.service.SecurityInfoService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.BitCoinUtils;
import com.biocoin.utils.EncryptDecrypt;
import com.biocoin.utils.SessionCollector;
import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;
import com.biocoin.dto.KycDTO;
import com.biocoin.model.KycInfo;
import com.biocoin.repo.KycInfoRepository;
import com.biocoin.utils.UserUtils;
import com.google.common.io.Files;
import java.math.BigDecimal;

@Service
public class UserRegisterServiceImpl extends HibernateServiceDaoImpl<RegisterInfo, Serializable>
		implements UserRegisterService {

	@Autowired
	private BitCoinUtils bitCoinUtils;

	@Autowired
	private UserUtils userUtils;

	@Autowired
	private KycInfoRepository kycInfoRepo;

	@Autowired
	private UserRegisterRepository userRegisterRepo;

	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	ConfigInfoRepository configInfoRepository;
	@Autowired
	Environment env;
	@Autowired
	UserRegisterService userRegisterService;
	@Autowired
	ExpirationDataService expirationDataService;
	@Autowired
	BioCoinUtils bioCoinUtils;
	@Autowired
	EmailNotificationService emailNotificationService;
	@Autowired
	SecurityInfoService securityInfoService;
	@Autowired
	private SessionCollector sessionCollector;
	@Autowired
	private UserRegisterRepository userRegisterRepository;
	@Autowired
	private QRCodeRepository qrCodeRepository;
	@Autowired
	private QRCodeGenerateServiceimpl qrCodeGenerateService;

	private static final Logger LOG = LoggerFactory.getLogger(UserRegisterController.class);

	@Transactional
	public RegisterInfo isAccountExistCheckByEmailId(String emailId) {
		// TODO Auto-generated method stub

		org.hibernate.Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from RegisterInfo where emailId=:emailId").setParameter("emailId", emailId);

		return (RegisterInfo) query.uniqueResult();
	}


@SuppressWarnings("unused")
	public boolean saveAccountInfo(UserRegisterDTO userRegisterDTO) throws Exception {
		// TODO Auto-generated method stub
		RegisterInfo reginfoObj = new RegisterInfo();

		if (userRegisterDTO.getAppId() != null && userRegisterDTO.getDeviceType() != null) {
			reginfoObj.setAppId(userRegisterDTO.getAppId());
			reginfoObj.setDeviceType(userRegisterDTO.getDeviceType());
		} else {
			reginfoObj.setAppId("");
			reginfoObj.setDeviceType("");
		}

		reginfoObj.setFirstName(userRegisterDTO.getFirstName());
		reginfoObj.setLastName(userRegisterDTO.getLastName());
		reginfoObj.setPassword(EncryptDecrypt.encrypt(userRegisterDTO.getPassword()));
		reginfoObj.setMobileno(userRegisterDTO.getMobileno());
		reginfoObj.setEmailId(userRegisterDTO.getEmailId());
		reginfoObj.setUserType("U");
		reginfoObj.setTokenBalance(0.0);
		reginfoObj.setTypeOfVerifications("user");
		reginfoObj.setLockedTokenBalance(0.0);
		reginfoObj.setGmailstatus(0);
		reginfoObj.setSignup_date(new Date());
		reginfoObj.setBurnedTokens(0.0);
		reginfoObj.setKycStatus(0);
		userRegisterService.save(reginfoObj);
		if (reginfoObj != null) {

			boolean status = isWalletCreate(userRegisterDTO, reginfoObj.getId());

			if (status == true) {

				String encrypteathWalletAddress = EncryptDecrypt.encrypt(userRegisterDTO.getEathwalletAddress());
				String encrypteathWalletPassword = EncryptDecrypt.encrypt(userRegisterDTO.getEathwalletPassword());

				reginfoObj.setEthWalletAddress(encrypteathWalletAddress);
				reginfoObj.setEthWalletPassword(encrypteathWalletPassword);

				Config config = configInfoRepository.findConfigByConfigKey("walletfile");

				String walletAddress = bioCoinUtils.getWalletAddress(config.getConfigValue(),
						userRegisterDTO.getEathwalletAddress());
				String encryptDummyWalletAddress = EncryptDecrypt.encrypt(walletAddress);
				reginfoObj.setDummywalletaddress(encryptDummyWalletAddress);
				qrCodeGenerater(reginfoObj, walletAddress);

				boolean btcStatus = bitCoinUtils.createBitcoinWallet(userRegisterDTO);

				if (btcStatus == true) {
					reginfoObj.setBtcWalletAddress(userRegisterDTO.getBtcwalletAddress());
					reginfoObj.setBtcWalletPassword(EncryptDecrypt.encrypt(userRegisterDTO.getEathwalletPassword()));
					reginfoObj.setBtcWalletGuid(userRegisterDTO.getBtcWalletGuid());
				}

				userRegisterService.update(reginfoObj);
				/*
				 * String verificationLink =
				 * env.getProperty("accholderotpverification.url") +
				 * reginfoObj.getUsername() + registerInfo.getUserId() +
				 * registerInfo.getPassword();
				 */
				// Send verification link to user email Id - With subject &
				// content
				boolean isEmailSent = emailNotificationService.sendEmail(userRegisterDTO.getEmailId(),
						"Welcome to Biokkoin", walletAddress, EncryptDecrypt.decrypt(userRegisterDTO.getBtcwalletAddress()), reginfoObj.getFirstName() + " " + reginfoObj.getLastName(),
						reginfoObj.getPassword(), reginfoObj.getId());
				if (!isEmailSent) {
					return false;
				}
				return true;
			}
			userRegisterService.delete(reginfoObj);
			return false;

		} else
			return false;
	}

	public boolean isWalletCreate(UserRegisterDTO userRegisterDTO, Integer Id) {
		try {
			Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");
			if (configInfo != null) {
				String walletFileLocation = configInfo.getConfigValue();

				walletFileLocation = walletFileLocation.replace("/", "\\");
				File createfolder = new File(walletFileLocation);
				if (!createfolder.exists()) {
					createfolder.mkdirs();
				}

				String fileName = null;

				fileName = WalletUtils.generateNewWalletFile(userRegisterDTO.getEathwalletPassword(),
						new File(walletFileLocation), false);

				userRegisterDTO.setEathwalletAddress(fileName);

				return true;
			}
		} catch (Exception e) {

			return false;
		}
		return false;
	}

	@Override
	public String isEmailVerified(UserRegisterDTO userRegisterDTO) {

		RegisterInfo registerInfo = userRegisterService.findById(userRegisterDTO.getId());
		if (registerInfo != null) {
			if (registerInfo.getGmailstatus() == 1) {
				return "Account already verified successfully";
			} else {
				registerInfo.setGmailstatus(1);
				userRegisterService.update(registerInfo);
				return "Account verified successfully";
			}

		}
		return "falied";
	}

	@SuppressWarnings("unused")
	@Override
	public LoginDTO isEmailIdAndPasswordExist(UserRegisterDTO userRegisterDTO, HttpServletRequest request,
			String password) throws Exception {
		LoginDTO responseDTO = new LoginDTO();
		TokenDTO tokenDTO = new TokenDTO();
		LOG.info("Object DTO : " + userRegisterDTO.getEmailId() + userRegisterDTO.getPassword());
		Config config = configInfoRepository.findConfigByConfigKey("walletfile");

		RegisterInfo registerInfo = userRegisterService
				.getRegisterInfoByEmailIdAndPassword(userRegisterDTO.getEmailId().trim(), password.trim());
		// LOGIN Success
		if (registerInfo != null && config != null) {

			try {
				if (userRegisterDTO.getAppId() != null && userRegisterDTO.getDeviceType() != null) {
					System.out.println("User info is not null");
					registerInfo.setAppId(userRegisterDTO.getAppId());
					registerInfo.setDeviceType(userRegisterDTO.getDeviceType());
					userRegisterRepository.save(registerInfo);
				}
				bioCoinUtils.generateSecureKey(userRegisterDTO);
				if (userRegisterDTO.getSecuredKey() == null) {
					responseDTO.setStatus("failed");
					return responseDTO;
				}
				System.out.println("Secured Key : " + userRegisterDTO.getSecuredKey());

			} catch (Exception e) {

				System.out.println("Exception is : " + e.toString());
				return null;

			}

			SecurityInfo securityInfo = securityInfoService.getSecurityInfoByEmailId(userRegisterDTO.getEmailId());

			if (securityInfo != null) {
				securityInfo.setEmailId(userRegisterDTO.getEmailId());
				securityInfo.setCreatedTime(new Date());
				securityInfo.setSecuredKey(userRegisterDTO.getSecuredKey());
				securityInfoService.update(securityInfo);
			} else {
				SecurityInfo securityInf = new SecurityInfo();

				securityInf.setEmailId(userRegisterDTO.getEmailId());
				securityInf.setCreatedTime(new Date());
				securityInf.setSecuredKey(userRegisterDTO.getSecuredKey());
				securityInfoService.save(securityInf);

			}

			// SecurityInfo
			HttpSession session = request.getSession();
			session.setAttribute("emailId", registerInfo.getEmailId());
			HttpSessionEvent event = new HttpSessionEvent(session);
			sessionCollector.sessionCreated(event);
			responseDTO.setUserName(registerInfo.getFirstName());
			responseDTO.setEmailId(registerInfo.getEmailId());
			String decryptWalletAddress = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());
			String walletAddress = bioCoinUtils.getWalletAddress(config.getConfigValue(), decryptWalletAddress);
			if (walletAddress != null) {
				LOG.info("walletAddress " + walletAddress);
				userRegisterDTO.setEathwalletAddress(walletAddress);
			}
			responseDTO.setId(session.getId());
			responseDTO.setSecuredKey(userRegisterDTO.getSecuredKey().toString());
			responseDTO.setUserType(registerInfo.getUserType());
			responseDTO.setStatus("success");
			return responseDTO;

		}
		responseDTO.setStatus("failed");
		return responseDTO;
	}

	@Transactional
	public RegisterInfo getRegisterInfoByEmailIdAndPassword(String emailId, String Password) {
		// TODO Auto-generated method stub
		org.hibernate.Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from RegisterInfo where emailId=:emailId and password=:Password");
		query.setParameter("emailId", emailId);
		query.setParameter("Password", Password);

		return (RegisterInfo) query.uniqueResult();
	}

	@Override
	public boolean isOldPassword(UserRegisterDTO userRegisterDTO) throws Exception {

		HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());

		String email = (String) session.getAttribute("emailId");

		// RegisterInfo UserModelInfo =
		// userRegisterlnfoRepository.findUserInfoModelByemail(String email );
		// UserModelInfo usermodelinfo =
		// registrationrepo.findUserModelInfoByEmail(email);
		// RegisterInfo isAccountExistCheckByEmailId(String emailId);
		RegisterInfo registerinfo = userRegisterService.isAccountExistCheckByEmailId(email);

		String oldpassword = registerinfo.getPassword();
		String decryptPassword = EncryptDecrypt.decrypt(oldpassword);

		if (decryptPassword.equals(userRegisterDTO.getOldPassword())) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean isChangePassword(UserRegisterDTO userRegisterDTO) {

		HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");

		// UserModelInfo artcoinUserModelInfo = registrationrepo
		// .findUserModelInfoByEmail(email);
		RegisterInfo registerinfo = userRegisterService.isAccountExistCheckByEmailId(email);
		String oldPassword = userRegisterDTO.getPassword();
		String confirmChangePassword = userRegisterDTO.getConfirmPassword();
		if (confirmChangePassword != null) {
			try {
				String encryptconfirmPassword = EncryptDecrypt.encrypt(confirmChangePassword);

				if (encryptconfirmPassword != null) {
					registerinfo.setPassword(encryptconfirmPassword);
					userRegisterService.update(registerinfo);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}

		return false;
	}

	@Transactional
	public long getTotalUserCount() {
		// TODO Auto-generated method stub
		org.hibernate.Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select count(*) from RegisterInfo where userType='U'");

		return (long) query.uniqueResult();
	}

	@Override
	public String isAccountHolderActiveOrNot(UserRegisterDTO userRegisterDTO) throws Exception {
		RegisterInfo registerInfo = userRegisterService.getRegisterInfoByEmailIdAndPassword(
				userRegisterDTO.getEmailId(), EncryptDecrypt.encrypt(userRegisterDTO.getPassword()));
		if (registerInfo != null) {
			int checkStatus = registerInfo.getGmailstatus();
			if (checkStatus == 1) {
				return "success";
			} else {
				return "Please click biocoin activation link in your register email id";
			}

		}
		return "Please enter valid username and password";
	}

	@Override
	public List<UserRegisterDTO> userList(UserRegisterDTO userRegisterDTO) throws Exception {
		// TODO Auto-generated method stub
		List<UserRegisterDTO> regiUserList = new ArrayList<UserRegisterDTO>();
		Sort sort = new Sort(new Sort.Order(Direction.ASC, "id"));
		Pageable pageable = new PageRequest(userRegisterDTO.getPageNum(), userRegisterDTO.getPageSize(), sort);
		Page<RegisterInfo> userlist = userRegisterRepo.findByUserType("U", pageable);
		userRegisterDTO.setTotalPages(userlist.getTotalPages());
		System.out.println("transactionHistory.getTotalPages() : " + userlist.getTotalPages() + "TotalElements : " + userlist.getTotalElements());
		userRegisterDTO.setTotalElements(Math.toIntExact(userlist.getTotalElements()));
		
		for (RegisterInfo userList : userlist) {
			UserRegisterDTO transaction = bioCoinUtils.userList(userList);
			regiUserList.add(transaction);
		}

		return regiUserList;
	}

//	@SuppressWarnings("unchecked")
//	@Transactional
//	public List<RegisterInfo> registerUserList() {
//		// TODO Auto-generated method stub
//		org.hibernate.Session session = sessionFactory.getCurrentSession();
//		Query query = session.createQuery("from RegisterInfo where userType='U'");
//		return query.list();
//	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	private void qrCodeGenerater(RegisterInfo registerInfo, String walletAddress) throws Exception {
		int id = registerInfo.getId();
		String dynamicQRFolder = Integer.toString(id);
		if (registerInfo != null) {
			QRCode qrcode = qrCodeRepository.findQRCodeByQrKey("QRKey");
			// String walletAddr = EncryptDecrypt.decrypt(walletAddress);
			String qrFileLocation = null;
			String path = env.getProperty("user.qrcode.folder");
			// File file = new File(env.getProperty("user.qrcode.folder"));
			// if (!file.exists()) {
			// LOG.info(" In mkdir : " + env.getProperty("user.qrcode.folder"));
			// file.mkdirs();
			// }
			if (qrcode != null) {

				qrFileLocation = qrcode.getQrcodeValue() + File.separator + path + File.separator;
				File createfolder = new File(qrFileLocation.concat(dynamicQRFolder));
				LOG.info("qrfilelocation" + qrFileLocation);
				if (!createfolder.exists()) {
					createfolder.mkdir();
					qrFileLocation = createfolder.getPath().replace("//", "/");
					qrFileLocation = qrFileLocation + "/";
				}
			}
			String filePath = qrFileLocation + File.separator + id + ".png";
			LOG.info("filePath" + filePath);
			String charset = "UTF-8"; // or "ISO-8859-1"
			Map hintMap = new HashMap();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
			qrCodeGenerateService.createQRCode(walletAddress, filePath, charset, hintMap, 200, 200);
			qrCodeGenerateService.readQRCode(filePath, charset, hintMap);
		}
	}

	@Override
	public String userDocumentUpload(MultipartFile uploadedFileRef, String emailId, String docType) throws Exception {
		FileInputStream reader = null;
		FileOutputStream writer = null;
		String path = null;
		String dbPath = null;
		Integer userRegId = 0;

		LOG.info("Mail Id--------->" + emailId);
		LOG.info("In  userDocumentUpload : start");
		try {
			Config configInfo = configInfoRepository.findConfigByConfigKey("location");
			String basePath = configInfo.getConfigValue();

			RegisterInfo userModelInfo = userRegisterRepo.findRegisterInfoByEmailIdIgnoreCase(emailId);

			if (userModelInfo != null) {
				userRegId = userModelInfo.getId();
				LOG.info("userRegId : " + userRegId);
				LOG.info("USerRegisterId " + userModelInfo.getId());
			}
			LOG.info(" Base path from config table : " + basePath);
			String uploadingdir = basePath + File.separator + env.getProperty("user.document.location") + File.separator
					+ userRegId + File.separator + docType;
			LOG.info(" uploadingdir : " + uploadingdir);
			File file = new File(uploadingdir);

			if (file.exists()) {
				file.delete();
			}
			if (!file.exists()) {
				LOG.info(" In mkdir : " + uploadingdir);
				file.mkdirs();
			}
			LOG.info(" uploadingdir : " + uploadingdir);

			String fileType = Files.getFileExtension(uploadedFileRef.getOriginalFilename());
			String fileName = Files.getNameWithoutExtension(uploadedFileRef.getOriginalFilename());

			double fileSize = uploadedFileRef.getSize() / 1024;

			LOG.info("File size in MB " + fileSize);
			LOG.info("File Type " + fileType);

			if (fileType.equalsIgnoreCase("jpeg") || fileType.equalsIgnoreCase("png")
					|| fileType.equalsIgnoreCase("jpg") || fileType.equalsIgnoreCase("pdf")) {

				LOG.info("Uploaded File Name " + fileName);
				path = uploadingdir + File.separator + fileName + "-" + userRegId + "." + fileType;
				dbPath = File.separator + env.getProperty("user.document.location") + File.separator + userRegId
						+ File.separator + docType + File.separator + fileName + "-" + userRegId + "." + fileType;
				LOG.info(" file path : " + path);
				LOG.info("dbPath : " + dbPath);
				byte[] buffer = new byte[1000];
				File outputFile = new File(path);

				int totalBytes = 0;
				outputFile.createNewFile();
				reader = (FileInputStream) uploadedFileRef.getInputStream();
				writer = new FileOutputStream(outputFile);

				int bytesRead = 0;
				while ((bytesRead = reader.read(buffer)) != -1) {
					writer.write(buffer);
					totalBytes += bytesRead;
				}
				dbPath = dbPath.replace(File.separator, "/");
				LOG.info("totalBytes:::" + totalBytes);
				reader.close();
				writer.close();
			} else {
				path = null;
				dbPath = null;
			}

		} catch (IOException e) {
			path = null;
			dbPath = null;
			reader.close();
			writer.close();
			LOG.error("Problem in userDocumentUpload file path : " + path);
			e.printStackTrace();
		} finally {

		}
		LOG.info("In  userDocumentUpload : end : dbPath : " + dbPath);
		return dbPath;
	}

	@SuppressWarnings("unused")
	@Override
	public KycDTO saveKycInfo(KycDTO kycDTO, String kycDoc1FilePath, String kycDoc2FilePath, String kycDoc3FilePath,
			String kycDoc4FilePath, MultipartFile kycDoc1, MultipartFile kycDoc2, MultipartFile kycDoc3,
			MultipartFile kycDoc4) {

		RegisterInfo userModelInfo = userRegisterRepo.findRegisterInfoByEmailIdIgnoreCase(kycDTO.getEmailId());
		LOG.info("userModelInfo.getKycStatus() ::" + userModelInfo.getKycStatus());
		userModelInfo.setKycStatus(1);
		if (userModelInfo != null) {
			KycInfo kycInfo = kycInfoRepo.findByEmailId(kycDTO.getEmailId());
			if (kycInfo == null) {
				KycInfo kycInfo2 = new KycInfo();
				kycInfo2.setCountry(kycDTO.getCountry());
				kycInfo2.setCity(kycDTO.getCity());
				kycInfo2.setCreationTime(new Date());
				kycInfo2.setDob(kycDTO.getDob());
				kycInfo2.setEmailId(kycDTO.getEmailId());
				kycInfo2.setFullName(kycDTO.getFullName());
				kycInfo2.setHomeAddress(kycDTO.getAddress());
				kycInfo2.setPhoneNo(kycDTO.getMobileNo());
				kycInfo2.setGender(kycDTO.getGender());
				LOG.info("kycDoc1FilePath  " + kycDoc1FilePath);
				LOG.info("kycDoc2FilePath  " + kycDoc2FilePath);
				LOG.info("kycDoc3FilePath  " + kycDoc3FilePath);
				LOG.info("kycDoc4FilePath  " + kycDoc4FilePath);
				kycInfo2.setKycDoc1Path(kycDoc1FilePath);
				kycInfo2.setKycDoc2Path(kycDoc2FilePath);
				kycInfo2.setKycDoc3Path(kycDoc3FilePath);
				kycInfo2.setKycDoc4Path(kycDoc4FilePath);
				kycInfo2.setKycStatus(2);
				kycInfo2.setKycUploadStatus(1);
				kycInfo2.setTypeOfVerifications("");
				kycInfo2.setKycDoc1Name(Files.getNameWithoutExtension(kycDoc1.getOriginalFilename()));

				kycInfo2.setKycDoc2Name(Files.getNameWithoutExtension(kycDoc2.getOriginalFilename()));

				kycInfo2.setKycDoc3Name(Files.getNameWithoutExtension(kycDoc3.getOriginalFilename()));

				kycInfo2.setKycDoc4Name(Files.getNameWithoutExtension(kycDoc4.getOriginalFilename()));

				KycInfo kycResponse = kycInfoRepo.save(kycInfo2);
				userUtils.listKYC(kycResponse);
				userRegisterRepo.save(userModelInfo);
				kycDTO.setKycStatus(userModelInfo.getKycStatus());
				return userUtils.listKYC(kycResponse);
			} else {

				kycInfo.setCity(kycDTO.getCity());
				kycInfo.setCountry(kycDTO.getCountry());
				kycInfo.setCreationTime(new Date());
				kycInfo.setDob(kycDTO.getDob());
				kycInfo.setEmailId(kycDTO.getEmailId());
				kycInfo.setGender(kycDTO.getGender());
				kycInfo.setFullName(kycDTO.getFullName());
				kycInfo.setHomeAddress(kycDTO.getAddress());
				kycInfo.setPhoneNo(kycDTO.getMobileNo());
				LOG.info("kycDoc1FilePath  " + kycDoc1FilePath);
				LOG.info("kycDoc2FilePath  " + kycDoc2FilePath);
				kycInfo.setKycDoc1Path(kycDoc1FilePath);
				kycInfo.setKycDoc2Path(kycDoc2FilePath);
				kycInfo.setKycDoc3Path(kycDoc3FilePath);
				kycInfo.setKycDoc4Path(kycDoc4FilePath);
				kycInfo.setKycStatus(2);
				kycInfo.setKycUploadStatus(1);
				kycInfo.setTypeOfVerifications("");
				kycInfo.setKycDoc1Name(Files.getNameWithoutExtension(kycDoc1.getOriginalFilename()));

				kycInfo.setKycDoc2Name(Files.getNameWithoutExtension(kycDoc2.getOriginalFilename()));

				kycInfo.setKycDoc3Name(Files.getNameWithoutExtension(kycDoc3.getOriginalFilename()));

				kycInfo.setKycDoc4Name(Files.getNameWithoutExtension(kycDoc4.getOriginalFilename()));

				KycInfo kycResponse = kycInfoRepo.save(kycInfo);
				userRegisterRepo.save(userModelInfo);
				kycDTO.setKycStatus(userModelInfo.getKycStatus());
				return userUtils.listKYC(kycResponse);
			}
		}
		return null;
	}

	@Override

	public KycDTO getKycDetails(TokenDTO tokenDTO) {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());

		String email = (String) session.getAttribute("emailId");
		LOG.info("Email : " + email);

		KycInfo kycInfo = kycInfoRepo.findByEmailId(email);
		if (kycInfo != null) {
			KycDTO kycDetails = userUtils.listKYC(kycInfo);
			return kycDetails;
		}
		return null;
	}

	@SuppressWarnings("unused")
	@Override
	public boolean updateKYC(KycDTO kycDTO) {
		LOG.info("KYC update function");

		KycInfo kycInfo = kycInfoRepo.findKycInfoById(kycDTO.getId());
		String mailId = kycInfo.getEmailId();

		LOG.info("KYC Mail :" + mailId);

		LOG.info("mailId ::" + mailId);
		RegisterInfo userModelInfo = userRegisterRepo.findRegisterInfoByEmailIdIgnoreCase(mailId);

		LOG.info("Register ::" + userModelInfo.getEmailId());

		if (kycInfo != null) {
			LOG.info("KYC info is not null");
			LOG.info("Kyc Status inside updateKYC:" + kycDTO.getKycStatus());

			if (kycDTO.getKycStatus() == 1) {
				LOG.info("KYC approved");
				userModelInfo.setKycStatus(1);
				kycInfo.setTypeOfVerifications(kycDTO.getTypeOfVerifications());
				userModelInfo.setTypeOfVerifications(kycDTO.getTypeOfVerifications());
				kycInfo.setKycStatus(kycDTO.getKycStatus());
				kycDTO.setMessage("User KYC Details has been Approved Successfully!");
				String verificationLink = "Hi " + userModelInfo.getFirstName() + ",<br><br>"
						+ "Your KYC details approved successfully." + "<br><br>" + "" + "With Regards,<br>"
						+ "Support Team<br>" + "Bio Coin<br>" + "email : info@biocoin.io";
				boolean isEmailSent = emailNotificationService.sendEmail(userModelInfo.getEmailId(), "BioCoin Team",
						verificationLink);
			} else if (kycDTO.getKycStatus() == 0) {
				LOG.info("KYC rejected");
				userModelInfo.setKycStatus(0);
				userModelInfo.setTypeOfVerifications(kycDTO.getTypeOfVerifications());
				kycInfo.setKycStatus(kycDTO.getKycStatus());

				kycDTO.setMessage("User KYC Details has been Rejected!");
				String verificationLink = "Hi " + userModelInfo.getFirstName() + ",<br><br>"
						+ "Your KYC details rejected due to improper (or) mismatch information that you have provided. Kindly reapply with proper information"
						+ "<br><br>" + "" + "With Regards,<br><br>" + "Support Team<br>" + "Bio Coin<br>"
						+ "email : info@biocoin.io";
				boolean isEmailSent = emailNotificationService.sendEmail(userModelInfo.getEmailId(), "BioCoin Team",
						verificationLink);
			}
			kycInfoRepo.save(kycInfo);
			userRegisterRepo.save(userModelInfo);
			return true;
		}
		return false;
	}

	@Override
	public List<KycDTO> kycList() {
		List<KycDTO> kycList = new ArrayList<KycDTO>();
		List<KycInfo> kycInfo = (List<KycInfo>) kycInfoRepo.findAllByOrderByIdDesc();

		for (KycInfo info : kycInfo) {
			KycDTO list = userUtils.listKYC(info);
			kycList.add(list);
		}
		return kycList;
	}

	@Override
	public boolean getUserInfo(UserRegisterDTO userRegisterDTO) throws Exception {
		String encryptPassword = EncryptDecrypt.encrypt(userRegisterDTO.getPassword().trim());
		RegisterInfo registerInfo = userRegisterService
				.getRegisterInfoByEmailIdAndPassword(userRegisterDTO.getEmailId().trim(), encryptPassword.trim());
		if (registerInfo.getUserType().equalsIgnoreCase("U")) {
			return true;
		}
		return false;
	}

	public List<KycDTO> getFeedItemsForKycUsers(Pageable pageRequest) {
		List<KycDTO> kycList = new ArrayList<KycDTO>();
		List<KycInfo> kycItems = (List<KycInfo>) kycInfoRepo.getAllUsersKycDetails(pageRequest);
		for (KycInfo kycInfo : kycItems) {

			KycDTO list = userUtils.listKYC(kycInfo);
			kycList.add(list);
		}

		return kycList;
	}

	@Override
	public List<KycDTO> filterKyCStatus(KycDTO kycDTO) {
		List<KycDTO> kycList = new ArrayList<KycDTO>();

		List<KycInfo> kycInfos = (List<KycInfo>) kycInfoRepo.findByKycStatus(kycDTO.getKycStatus());

		for (KycInfo kycInfo : kycInfos) {

			KycDTO list = userUtils.listKYC(kycInfo);

			kycList.add(list);
		}
		return kycList;
	}

	@Override
	public List<KycDTO> getFeedItemsForKycUsers(Integer kycStatus, Pageable pageRequest) {
		List<KycDTO> kycList = new ArrayList<KycDTO>();

		List<KycInfo> kycInfos = (List<KycInfo>) kycInfoRepo.findByKycStatus(kycStatus, pageRequest);

		for (KycInfo kycInfo : kycInfos) {

			KycDTO list = userUtils.listKYC(kycInfo);

			kycList.add(list);
		}
		return kycList;
	}

	@Override
	public List<KycDTO> getFeedItemsForKycUsers(String fullName, Pageable pageRequest) {
		List<KycDTO> kycList = new ArrayList<KycDTO>();

		List<KycInfo> kycInfos = (List<KycInfo>) kycInfoRepo.findByFullName(fullName, pageRequest);

		for (KycInfo kycInfo : kycInfos) {

			KycDTO list = userUtils.listKYC(kycInfo);

			kycList.add(list);
		}
		return kycList;
	}

	@Override
	public List<KycDTO> getFeedItemsForKycUsersByEmail(String emailId, Pageable pageRequest) {
		List<KycDTO> kycList = new ArrayList<KycDTO>();

		List<KycInfo> kycInfos = (List<KycInfo>) kycInfoRepo.findByEmailId(emailId,emailId, pageRequest);

		for (KycInfo kycInfo : kycInfos) {

			KycDTO list = userUtils.listKYC(kycInfo);

			kycList.add(list);
		}
		return kycList;
	}
	@Override
	public TokenDTO getKycCount(TokenDTO tokenDTO) {
		Pageable pageRequest = new PageRequest(tokenDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC,
				"creationTime");

		Page<KycInfo> kycCount1 = kycInfoRepo.getAllUsersKycDet(pageRequest);
		TokenDTO kycCount = new TokenDTO();
		kycCount.setTotalElements(Math.toIntExact(kycCount1.getTotalElements()));
		kycCount.setTotalPages(kycCount1.getTotalPages());		
		return kycCount;

	}

	@Override
	public KycDTO getKycCountByStatus(KycDTO kycDTO) {
		Pageable pageRequest = new PageRequest(kycDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC,
				"creationTime");
		Page<KycInfo> kycCount1 = kycInfoRepo.getAllUsersByKycStatus(kycDTO.getKycStatus(),pageRequest);
		KycDTO kycCount = new KycDTO();
		kycCount.setTotalElements(kycCount1.getTotalElements());
		kycCount.setTotalPages(kycCount1.getTotalPages());		
		return kycCount;
	}

	@Override
	public KycDTO getKycCountByUsername(KycDTO kycDTO) {
		Pageable pageRequest = new PageRequest(kycDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC,
				"creationTime");
		Page<KycInfo> kycCount1 = kycInfoRepo.getAllUsersByKycUsername(kycDTO.getFilterKyc(),pageRequest);
		KycDTO kycCount = new KycDTO();
		kycCount.setTotalElements(kycCount1.getTotalElements());
		kycCount.setTotalPages(kycCount1.getTotalPages());		
		return kycCount;
	}

	
@Override
	public KycDTO getKycCountByEmailid(KycDTO kycDTO) {
		Pageable pageRequest = new PageRequest(kycDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC,
				"creationTime");
		Page<KycInfo> kycCount1 = kycInfoRepo.getAllUsersByKycEmailid(kycDTO.getFilterKyc(),kycDTO.getFilterKyc(),pageRequest);
		KycDTO kycCount = new KycDTO();
		kycCount.setTotalElements(kycCount1.getTotalElements());
		kycCount.setTotalPages(kycCount1.getTotalPages());		
		return kycCount;
	}

	@Override
	public List<KycDTO> getFeedItemsForKycUsersByUsers(String filterKyc, Pageable pageRequest) {
		List<KycDTO> kycList = new ArrayList<KycDTO>();

		List<KycInfo> kycInfos = (List<KycInfo>) kycInfoRepo.findByUserName(filterKyc, pageRequest);

		for (KycInfo kycInfo : kycInfos) {

			KycDTO list = userUtils.listKYC(kycInfo);

			kycList.add(list);
		}
		return kycList;
	}
	
	@Override
	public BigDecimal getBtcBalance(TokenDTO tokenDTO) throws Exception {
		return bitCoinUtils.getBitcoinBalance(tokenDTO);		
	} 

}
