package com.biocoin.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.bitcoinj.core.NetworkParameters;

import org.bitcoinj.params.TestNet3Params;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.utils.Convert;

import com.biocoin.dto.TokenDTO;
import com.biocoin.model.BiocoinValue;
import com.biocoin.model.Config;
import com.biocoin.model.HoldTokensInfo;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.Token_info;
import com.biocoin.model.TransactionHistory;
import com.biocoin.repo.ConfigInfoRepository;
import com.biocoin.repo.HoldTokensInfoRepo;
import com.biocoin.repo.TokensInfoRepo;
import com.biocoin.repo.TransactionInfoRepository;
import com.biocoin.repo.UserRegisterRepository;
import com.biocoin.service.BiocoinValueService;
import com.biocoin.service.EmailNotificationService;
import com.biocoin.service.TokenService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.solidityHandler.SolidityHandler;
import com.biocoin.solidityToJava.Biokkoin;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.CurrentValueUtils;
import com.biocoin.utils.EncryptDecrypt;
import com.biocoin.utils.FcmUtils;
import com.biocoin.utils.SessionCollector;

@Service
public class TokenServiceImplementation extends HibernateServiceDaoImpl<Token_info, Serializable>
		implements TokenService {

	private TransactionReceipt transactionReceipt;

	private static final Logger LOG = LoggerFactory.getLogger(TokenServiceImplementation.class);

	private final Web3j web3j = Web3j.build(new HttpService("https://rinkeby.infura.io/"));

	public static NetworkParameters params = TestNet3Params.get();

	@Autowired
	private SolidityHandler solidityHandler;
	@Autowired
	private ConfigInfoRepository configInfoRepository;
	@Autowired
	private BioCoinUtils bioCoinUtils;
	@Autowired
	private Environment env;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private CurrentValueUtils currentValueUtils;
	@Autowired
	private BiocoinValueService biocoinValueService;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private UserRegisterRepository userRegisterRepository;
	@Autowired
	private FcmUtils fcmUtils;
	@Autowired
	private HoldTokensInfoRepo holdTokensInfoRepo;
	@Autowired
	private TransactionInfoRepository transactionInfoRepository;
	@Autowired
	private EmailNotificationService emailNotificationService;
	@Autowired
	private TokensInfoRepo tokensInfoRepo;

	@Override
	public boolean validAmount(TokenDTO tokenDTO) throws Exception {
		// RegisterInfo registerInfo = new RegisterInfo();
		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());

		String email = (String) session.getAttribute("emailId");

		RegisterInfo registerInfo = userRegisterService.isAccountExistCheckByEmailId(email);
		String decryptedWalletAddress = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());
		String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(), decryptedWalletAddress);
		tokenDTO.setEthWalletAddress(etherWalletAddress);
		// tokenDTO.setCentralAdmin(tokenDTO.getEtherWalletAddress());

		EthGetBalance ethGetBalance;
		ethGetBalance = web3j.ethGetBalance(tokenDTO.getEthWalletAddress(), DefaultBlockParameterName.LATEST)
				.sendAsync().get();
		BigInteger wei = ethGetBalance.getBalance();
		BigDecimal amountCheck = Convert.fromWei(wei.toString(), Convert.Unit.ETHER);
		tokenDTO.setEtherbalance(amountCheck);

		if (amountCheck != null) {
			BigDecimal bal = tokenDTO.getEtherbalance();

			Double reqToken = tokenDTO.getRequestToken();

			BigDecimal reqtoken = new BigDecimal(reqToken);
			// BigDecimal bal=new BigDecimal(balance);

			double currentethvalue = currentValueUtils.getEtherValueForOneDollar();
			// 1Bio coin=$15USD taken from biocoinvalue table dynamically
			BiocoinValue biocoinval = biocoinValueService.findById(1);
			double current_biocoin_ethvalue = (biocoinval.getBiocoin_value()) * (currentethvalue);
			BigDecimal k = BigDecimal.valueOf(current_biocoin_ethvalue);
			BigDecimal resultethvalue = (k).multiply(reqtoken);

			int res = bal.compareTo(resultethvalue);
			if (res == 1) {
				return true;
			}
		}
		return false;

	}

	@Override
	public String burnToken(TokenDTO tokenDTO) throws IOException, CipherException, Exception {

		String isSuccess = solidityHandler.burntoken(tokenDTO);
		if (isSuccess != null) {
			return isSuccess;
		}
		return isSuccess;
	}

	@Override
	public BigInteger tokenBalance(TokenDTO tokenDTO) throws Exception {

		Credentials credentials;

		Web3j web3j = Web3j.build(new HttpService());

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		Config configInfo = configInfoRepository.findConfigByConfigKey("walletFile");
		String mail = (String) session.getAttribute("emailId");
		// UserModelInfo koinUserModelInfo
		// =regrep.findUserModelInfoByEmail(mail);
		RegisterInfo reginfo = userRegisterService.isAccountExistCheckByEmailId(mail);

		if (reginfo != null && configInfo != null) {
			String decryptEtherWalletAddress = EncryptDecrypt.decrypt(reginfo.getEthWalletAddress());
			String dercyptEtherWalletPassword = EncryptDecrypt.decrypt(reginfo.getEthWalletPassword());
			tokenDTO.setToAddress(configInfo.getConfigValue() + "//" + decryptEtherWalletAddress);

			String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(),
					decryptEtherWalletAddress);
			if (etherWalletAddress == null) {
				return null;
			}
			// tokenDTO.setEtherWalletAddress(etherWalletAddress);
			// tokenDTO.setEtherWalletPassword(dercyptEtherWalletPassword);

			credentials = WalletUtils.loadCredentials(dercyptEtherWalletPassword,
					new File(configInfo.getConfigValue() + "//" + decryptEtherWalletAddress));

			Biokkoin assetToken = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials,
					BigInteger.valueOf(3000000), BigInteger.valueOf(3000000));

			BigInteger balance = assetToken.balanceOf(etherWalletAddress).send();
			tokenDTO.setTokenBalance(balance.doubleValue() / 100000000);
			return balance;

		}

		return null;
	}

	@Override
	public String mintToken(TokenDTO tokenDTO) throws IOException, CipherException, Exception {
		// TODO Auto-generated method stub
		String mintToken = solidityHandler.mintToken(tokenDTO);
		if (mintToken.equalsIgnoreCase(env.getProperty("token.mint.success"))) {
			return mintToken;

		}
		return mintToken;
	}

	@Override
	public String isValidTokenBalForTokenTransfer(TokenDTO tokenDTO) {
		// TODO Auto-generated method stub
		// BigInteger crowdsaleBal =
		// loadTokenCredentials().balanceOf(env.getProperty("main.address")).send();
		Token_info token_info = tokenService.findById(1);

		if (token_info != null) {

			Double crowdsaleBal = token_info.getManualAvailableTokens();
			if (BigInteger.ZERO.equals(token_info.getManualAvailableTokens())) {
				return "Token sold out";
			}

			int count = crowdsaleBal.compareTo(tokenDTO.getRequestToken());

			if (count == -1) {
				return "Only  " + crowdsaleBal + "  token available . if you want means try that available range";
			}

			return "success";
		}
		return "No record found";

	}

	@Override
	public String validateTypeOfPersons(TokenDTO tokenDTO) throws Exception {
		System.out.println("Type of Verifications : " + tokenDTO.getTypeOfVerifications());
		
		if (!tokenDTO.getTypeOfVerifications().equalsIgnoreCase(env.getProperty("transfer.type.mode.others")) 
				&& !tokenDTO.getTypeOfVerifications().equalsIgnoreCase(env.getProperty("transfer.type.mode.users"))) {
			System.out.println("Inside Validation for Type of Persons");
			String toAddress = EncryptDecrypt.encrypt(tokenDTO.getToAddress());
			System.out.println("To Address : " + toAddress);
			RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByDummywalletaddress(toAddress);
			if (registerInfo != null) {
				System.out.println("Type of Verifications : " + tokenDTO.getTypeOfVerifications() + " : " + registerInfo.getTypeOfVerifications());
				if (tokenDTO.getTypeOfVerifications().equalsIgnoreCase(registerInfo.getTypeOfVerifications())) {
					return env.getProperty("valid.typeofverification");
				} else {
					return env.getProperty("invalid.typeofverification");
				}
			}
			return env.getProperty("invalid.to.address");
		}
		return env.getProperty("valid.typeofverification");
	}

	@Override
	public String manualTokenTransferForAdmin(TokenDTO tokenDTO) throws Exception {

		Config config = configInfoRepository.findConfigByConfigKey("walletfile");
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");

		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		if (registerInfo != null) {
			String fromAddress = bioCoinUtils.getWalletAddress(config.getConfigValue(),
					EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress()));
			TransactionHistory transactionHistory = new TransactionHistory();
			transactionHistory.setFromAddress(fromAddress);
			transactionHistory.setToAddress(tokenDTO.getToAddress());
			transactionHistory.setCryptoAmount(new BigDecimal(0.0));
			transactionHistory.setTransferToken(tokenDTO.getSendToken());
			transactionHistory.setTxDate(new Date());
			transactionHistory.setPaymentMode("Manual");
			transactionHistory.setTxstatus("Waiting for another admin approval");
			transactionHistory.setTypeOfVerifications(tokenDTO.getTypeOfVerifications());
			transactionHistory.setSenderId(registerInfo.getId());
			transactionHistory.setReceiverId(0);
			transactionHistory.setRegisterInfo(registerInfo);
			transactionHistory.setEmailId(mail);
			transactionInfoRepository.save(transactionHistory);

			List<RegisterInfo> registerInfo1 = userRegisterRepository.findByUserType("A");

			for (RegisterInfo reg : registerInfo1) {
				if (!mail.equalsIgnoreCase(reg.getEmailId())) {

					String encryptedTransactionId = EncryptDecrypt.encrypt(transactionHistory.getId().toString());
//					String id = URLEncoder.encode(encryptedTransactionId, "UTF-8");
					
					String encryptedStatus = EncryptDecrypt.encrypt(transactionHistory.getTxstatus());
//					String status = URLEncoder.encode(encryptedStatus, "UTF-8");
					
					String url = env.getProperty("transfer.id") + encryptedTransactionId
							+ env.getProperty("transfer.status") + encryptedStatus;

					String content = "<b>Dear Admin,<br><br>"
							+ "<b>You have received a request for Token Transfer Approval. Please review and Approve !!!<br><br></b>"
							+ "toAddress=" + StringUtils.trim(tokenDTO.getToAddress()) + "<br>" + "fromAddress= "
							+ StringUtils.trim(transactionHistory.getFromAddress()) + "<br>" + "requestToken= "
							+ StringUtils.trim(tokenDTO.getSendToken().toString()) + "<br><br>" + "<a href=" + url
							+ "> Click here to Approve Token Transfer. </a>" + "<br>" + "<br>"
							+ "<b>If you need to contact us, please click on the contact us link at the bottom of our website or below :</b>"
							+ "<a href=" + env.getProperty("contactus") + "><br>Contact Us.</a>" + "<br><br>"
							+ "<b>Thank you,</b><br>" + "<b>Biokkoin team.</b>";

					boolean isEmailSent = emailNotificationService.sendEmailTokenTransfer(reg.getEmailId(),
							"Token Transfer Info from Biokkoin", content);
					if (isEmailSent) {
						System.out.println("Email sent");
					} else {
						System.out.println("Email Not sent");
					}
				}
			}
			return env.getProperty("token.transfer.success.approval");
		}
		return env.getProperty("token.transfer.failed");
	}

	public boolean validatingAndUpdating(TokenDTO tokenDTO) throws Exception {
		String toAddress = EncryptDecrypt.encrypt(tokenDTO.getToAddress());
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByDummywalletaddress(toAddress);
		if (registerInfo != null) {
			HoldTokensInfo holdTokensInfo = holdTokensInfoRepo.findByEmailId(registerInfo.getEmailId());
			if (holdTokensInfo != null) {
				holdTokensInfo.setLockedTokens(holdTokensInfo.getLockedTokens() + tokenDTO.getSendToken());
				holdTokensInfoRepo.save(holdTokensInfo);

				registerInfo.setLockedTokenBalance(registerInfo.getLockedTokenBalance() + tokenDTO.getSendToken());
				userRegisterRepository.save(registerInfo);
			} else {
				HoldTokensInfo holdTokensInfo1 = new HoldTokensInfo();
				holdTokensInfo1.setLockedTokens(tokenDTO.getSendToken());
				holdTokensInfo1.setEmailId(registerInfo.getEmailId());
				holdTokensInfo1.setTypeOfVerifications(tokenDTO.getTypeOfVerifications());
				holdTokensInfo1.setPeriodEnd(new Date());
				holdTokensInfoRepo.save(holdTokensInfo1);

				registerInfo.setLockedTokenBalance(registerInfo.getLockedTokenBalance() + tokenDTO.getSendToken());
				userRegisterRepository.save(registerInfo);
				tokenDTO.setReceiver(registerInfo.getId().toString());
			}
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public String ValidateInputParamsOfApproval(TokenDTO tokenDTO) throws Exception {
		transactionReceipt = new TransactionReceipt();
		Config config = configInfoRepository.findConfigByConfigKey("walletfile");
		DecimalFormat df = new DecimalFormat("0.00000000");
		
//		String decodeId = URLDecoder.decode(tokenDTO.getApproveId(), "UTF-8");
//		System.out.println("decodeId : " + decodeId);
		String decryptId = EncryptDecrypt.decrypt(tokenDTO.getApproveId().replaceAll("\\s", "+"));
		
//		String decodeStatus = URLDecoder.decode(tokenDTO.getTxStatus(), "UTF-8");
//		System.out.println("decodeId : " + decodeStatus);
		String decryptStatus = EncryptDecrypt.decrypt(tokenDTO.getTxStatus().replaceAll("\\s", "+"));
		
		TransactionHistory transactionHistory = transactionInfoRepository.findOne(Integer.parseInt(decryptId));
		String mail = transactionHistory.getEmailId();
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		Token_info token_info = tokenService.findById(1);
		if (token_info.getManualAvailableTokens() >= transactionHistory.getTransferToken()) {
			if (transactionHistory.getTxstatus().equalsIgnoreCase(decryptStatus)) {

				Credentials credentials = WalletUtils.loadCredentials(
						EncryptDecrypt.decrypt(registerInfo.getEthWalletPassword()),
						config.getConfigValue() + EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress()));
				Biokkoin assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
						Contract.GAS_PRICE, Contract.GAS_LIMIT);
				BigDecimal amount = BigDecimal.valueOf(transactionHistory.getTransferToken());

				amount = amount.multiply(new BigDecimal("100000000"));
				BigInteger transVal = amount.toBigInteger();

				transactionReceipt = assetToken.transfer(transactionHistory.getToAddress(), transVal).send();
				System.out.println(transactionReceipt.getStatus());

				if (transactionReceipt.getStatus().equalsIgnoreCase("0x1")) {

					transactionHistory.setTxstatus("Success");
					transactionInfoRepository.save(transactionHistory);

					token_info.setTotalAvailableTokens(
							token_info.getTotalAvailableTokens() - transactionHistory.getTransferToken());
					token_info.setManualAvailableTokens(
							token_info.getManualAvailableTokens() - transactionHistory.getTransferToken());
					token_info.setTotalSoldTokens(
							token_info.getTotalSoldTokens() + transactionHistory.getTransferToken());
					tokensInfoRepo.save(token_info);

					tokenDTO.setToAddress(transactionHistory.getToAddress());
					tokenDTO.setSendToken(transactionHistory.getTransferToken());

					if (transactionHistory.getTypeOfVerifications()
							.equalsIgnoreCase(env.getProperty("transfer.type.mode.others"))
							|| transactionHistory.getTypeOfVerifications()
									.equalsIgnoreCase(env.getProperty("transfer.type.mode.ico.users"))) {

						System.out.println("ICO Users or Others : " + transactionHistory.getTypeOfVerifications());

					} else if (!transactionHistory.getTypeOfVerifications()
							.equalsIgnoreCase(env.getProperty("transfer.type.mode.others"))
							|| !transactionHistory.getTypeOfVerifications()
									.equalsIgnoreCase(env.getProperty("transfer.type.mode.ico.users"))) {

						Date date = new Date();
						System.out.println("Current Date : " + date);
						Date vestingEndDate = token_info.getLockedTokensEndDate();
						System.out.println("Vesting period End Date : " + vestingEndDate);

						if (date.before(vestingEndDate)) {
							System.out.println("Inside Vesting period");

							boolean isValid;
							try {
								isValid = validatingAndUpdating(tokenDTO);
								if (!isValid) {
									System.out.println("Not properly saved");

									transactionHistory.setReceiverId(Integer.parseInt(tokenDTO.getReceiver()));
									transactionInfoRepository.save(transactionHistory);
								}

							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} else {
							System.out.println("Vesting period Ends");
						}
					}
				} else if (transactionReceipt.getStatus().equalsIgnoreCase("0x0")) {
					transactionHistory.setTxstatus("Failed");
					transactionInfoRepository.save(transactionHistory);
				}
				return env.getProperty("token.transfer.success");
			}
			return env.getProperty("token.transfer.status.varied");
		}
		return env.getProperty("token.transfer.failed");

	}

	@Transactional
	public Token_info getCountCurrentDateBetweenIcoDate(Date icostartDate, Date icoendDate) {
		// TODO Auto-generated method stub
		org.hibernate.Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Token_info where SYSDate() between :icostartDate and :icoendDate");
		query.setParameter("icostartDate", icostartDate);
		query.setParameter("icoendDate", icoendDate);
		// query.setParameter("currentDate", currentDate);

		return (Token_info) query.uniqueResult();
	}

	@Override
	public void requestcoinPushNotification(TokenDTO tokenDTO) throws Exception {

		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		RegisterInfo userInfoModel1 = userRegisterRepository
				.findRegisterInfoByDummywalletaddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));
		String appId = userInfoModel1.getAppId();
		if (appId.length() >= 1) {
			String server_key = env.getProperty("server_key");

			String message = userInfoModel.getFirstName() + " Has Requested " + tokenDTO.getRequestTokens() + " "
					+ tokenDTO.getPaymentMode() + " From You";
			JSONObject infoJson = new JSONObject();

			infoJson.put("title", "BIORequestCoin Notification");

			infoJson.put("body", message);

			infoJson.put("id", tokenDTO.getId());
			String deviceType = "notification";
			if (userInfoModel1.getDeviceType().equals("ios")) {
				fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
			} else {
				fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
				deviceType = "data";
				fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
			}
		}
	}

	@Override
	public void sendETHCoinPushNotificationFrom(RegisterInfo registerInfo, TokenDTO tokenDTO) {
		System.out.println(
				"INSIDE PUSH PAYMENT MODE" + tokenDTO.getPaymentMode() + "req Token" + tokenDTO.getRequestTokens());
		String message = "Successfully Sent Your " + tokenDTO.getPaymentMode() + " " + tokenDTO.getRequestTokens()
				+ " Coin To " + tokenDTO.getReceiver();
		String appId = registerInfo.getAppId();
		String server_key = env.getProperty("server_key");
		JSONObject infoJson = new JSONObject();
		try {
			infoJson.put("title", "BIOSendCoin Notification");

			infoJson.put("body", message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String deviceType = "notification";
		if (registerInfo.getDeviceType().equals("ios")) {
			fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
		} else {
			fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
			deviceType = "data";
			fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
		}
	}

	@Override
	public void sendETHCoinPushNotificationTo(RegisterInfo registerInfo, TokenDTO tokenDTO) {
		String message = "Received " + tokenDTO.getPaymentMode() + " " + tokenDTO.getTransferAmount() + " from "
				+ tokenDTO.getSender();
		String appId = registerInfo.getAppId();
		String server_key = env.getProperty("server_key");
		JSONObject infoJson = new JSONObject();
		try {
			infoJson.put("title", "BIOSendCoin Notification");
			infoJson.put("body", message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String deviceType = "notification";
		if (registerInfo.getDeviceType().equals("ios")) {
			fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
		} else {
			fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
			deviceType = "data";
			fcmUtils.send_FCM_Notification(appId, server_key, message, infoJson, deviceType);
		}

	}

	@Override
	public boolean checkMainbalance(TokenDTO tokenDTO) {

		EthGetBalance ethGetBalance;
		try {
			DecimalFormat df = new DecimalFormat("#.########");
			ethGetBalance = web3j.ethGetBalance(tokenDTO.getCentralAdmin(), DefaultBlockParameterName.LATEST)
					.sendAsync().get();

			BigDecimal wei = new BigDecimal(ethGetBalance.getBalance());
			LOG.info("wei " + wei);
			// BigInteger wei = ethGetBalance.getBalance();
			BigDecimal amountCheck = Convert.fromWei(wei, Convert.Unit.ETHER);

			tokenDTO.setMainBalance(new BigDecimal(df.format(amountCheck.doubleValue())));

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public String manualTokenTransferToUsers(TokenDTO tokenDTO) throws Exception {

		String transfer = solidityHandler.manualTokenTransferToUsers(tokenDTO);
		if (transfer != null) {
			return transfer;
		}
		return null;
	}

}
