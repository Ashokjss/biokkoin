package com.biocoin.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.biocoin.dto.PaypalDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.BiocoinValue;
import com.biocoin.model.ICOPeriodInfo;
import com.biocoin.model.PurchaseTokenInfo;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.TokenAllocationForTopManagementsInfo;
import com.biocoin.model.Token_info;
import com.biocoin.repo.ICOPeriodInfoRepo;
import com.biocoin.repo.PurchaseTokenInfoRepository;
import com.biocoin.repo.TokenAllocationForTopManagementsInfoRepo;
import com.biocoin.repo.TokensInfoRepo;
import com.biocoin.service.BiocoinValueService;
import com.biocoin.solidityHandler.SolidityHandler;
import com.biocoin.utils.EncryptDecrypt;
import com.biocoin.utils.SessionCollector;

import com.biocoin.repo.UserRegisterRepository;

@Service
public class BiocoinValueServiceImpl extends HibernateServiceDaoImpl<BiocoinValue, Serializable>
		implements BiocoinValueService {

	static final Logger LOG = LoggerFactory.getLogger(BiocoinValueServiceImpl.class);

	private static Hashtable<String, String> parameters;

	@Autowired
	private Environment env;
	@Autowired
	private TokenAllocationForTopManagementsInfoRepo tokenAllocationForTopManagementsInfoRepo;
	@Autowired
	private TokensInfoRepo tokensInfoRepo;
	@Autowired
	private ICOPeriodInfoRepo icoPeriodInfoRepo;
	@Autowired
	private SolidityHandler solidityHandler;
	@Autowired
	private UserRegisterRepository userRegisterRepository;
	@Autowired
	private PurchaseTokenInfoRepository purchaseTokenInfoRepository;

	@Override
	public String saveAllocateTokens(UserRegisterDTO userRegisterDTO) {

		HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		if (registerInfo.getUserType().equalsIgnoreCase("A")) {
			RegisterInfo registerInfo1 = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(userRegisterDTO.getEmailId());
			if (registerInfo1 != null) {
				System.out.println("Email Id : " + registerInfo1.getEmailId());
				if(registerInfo1.getTypeOfVerifications().equalsIgnoreCase(env.getProperty("top.members"))) {
					System.out.println("registerInfo.getTypeOfVerifications() : " + registerInfo1.getTypeOfVerifications());
					TokenAllocationForTopManagementsInfo tokenAllocation = tokenAllocationForTopManagementsInfoRepo
							.findTokenAllocationForTopManagementsInfoByEmailId(userRegisterDTO.getEmailId());
					if (tokenAllocation != null) {
						tokenAllocation.setAllocatedTokens(userRegisterDTO.getAllocatedTokens());
						tokenAllocationForTopManagementsInfoRepo.save(tokenAllocation);
					} else {
						TokenAllocationForTopManagementsInfo tokenAllocationInfo = new TokenAllocationForTopManagementsInfo();
						tokenAllocationInfo.setEmailId(userRegisterDTO.getEmailId());
						tokenAllocationInfo.setAllocatedTokens(userRegisterDTO.getAllocatedTokens());
						tokenAllocationInfo.setTypeOfVerifications(env.getProperty("top.members"));
						tokenAllocationForTopManagementsInfoRepo.save(tokenAllocationInfo);
					}

					registerInfo.setTypeOfVerifications(env.getProperty("top.members"));
					userRegisterRepository.save(registerInfo);
				} else {
					return "User is not in Top Management list...";
				}
			} else {
				return env.getProperty("update.charity.failed.not.user");
			}
			return env.getProperty("update.charity.success");
		}
		return env.getProperty("not.admin");
	}

	@Override
	public List<UserRegisterDTO> listAllocatedTokens(UserRegisterDTO userRegisterDTO) {
		List<UserRegisterDTO> list = new ArrayList<UserRegisterDTO>();

		Sort sort = new Sort(new Sort.Order(Direction.ASC, "id"));
		Pageable pageable = new PageRequest(userRegisterDTO.getPageNum(), userRegisterDTO.getPageSize(), sort);

		Page<TokenAllocationForTopManagementsInfo> tokenAllocationInfo = tokenAllocationForTopManagementsInfoRepo
				.findAll(pageable);
		userRegisterDTO.setTotalPages(tokenAllocationInfo.getTotalPages());
		System.out.println("transactionHistory.getTotalPages() : " + tokenAllocationInfo.getTotalPages()
				+ "TotalElements : " + tokenAllocationInfo.getTotalElements());
		userRegisterDTO.setTotalElements(Math.toIntExact(tokenAllocationInfo.getTotalElements()));

		for (TokenAllocationForTopManagementsInfo tokenAllocation : tokenAllocationInfo) {
			UserRegisterDTO users = new UserRegisterDTO();

			users.setId(tokenAllocation.getId());
			users.setEmailId(tokenAllocation.getEmailId());
			users.setAllocatedTokens(tokenAllocation.getAllocatedTokens());
			users.setTypeOfVerifications(tokenAllocation.getTypeOfVerifications());
			list.add(users);
		}
		return list;
	}

	@Override
	public boolean deleteAllocateTokens(UserRegisterDTO userRegisterDTO) {

		TokenAllocationForTopManagementsInfo tokenAllocation = tokenAllocationForTopManagementsInfoRepo
				.findById(userRegisterDTO.getId());
		if (tokenAllocation != null) {
			tokenAllocationForTopManagementsInfoRepo.delete(tokenAllocation.getId());
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<UserRegisterDTO> listTokensRate(UserRegisterDTO userRegisterDTO) throws Exception {
		List<UserRegisterDTO> list = new ArrayList<UserRegisterDTO>();
		List<Token_info> tokenInfo = (List<Token_info>) tokensInfoRepo.findAll();
		for (Token_info token : tokenInfo) {
			userRegisterDTO.setId(token.getId());
			userRegisterDTO.setIcoStart(token.getStartDate());
			userRegisterDTO.setIcoEnd(token.getEndDate());
			userRegisterDTO.setTokenValuesInUSD(EncryptDecrypt.decrypt(token.getTokenValuesInUSD()));
			list.add(userRegisterDTO);
		}
		return list;
	}

	@Override
	public boolean updateTokensRate(UserRegisterDTO userRegisterDTO) throws Exception {

		Token_info tokenInfo = tokensInfoRepo.findById(1);
		if (tokenInfo != null) {
			tokenInfo.setTokenValuesInUSD(EncryptDecrypt.encrypt(userRegisterDTO.getTokenValuesInUSD().toString()));
			// tokenInfo.setStartDate(userRegisterDTO.getIcoStart());
			// tokenInfo.setEndDate(userRegisterDTO.getIcoEnd());
			tokensInfoRepo.save(tokenInfo);
			return true;
		}
		return false;
	}

	@Override
	public List<UserRegisterDTO> listICOPeriod(UserRegisterDTO userRegisterDTO) {
		List<UserRegisterDTO> list = new ArrayList<UserRegisterDTO>();

		Sort sort = new Sort(new Sort.Order(Direction.ASC, "icoStart"));
		Pageable pageable = new PageRequest(userRegisterDTO.getPageNum(), userRegisterDTO.getPageSize(), sort);

		Page<ICOPeriodInfo> icoPeriodInfo = icoPeriodInfoRepo.findAll(pageable);
		userRegisterDTO.setTotalPages(icoPeriodInfo.getTotalPages());
		System.out.println("transactionHistory.getTotalPages() : " + icoPeriodInfo.getTotalPages() + "TotalElements : "
				+ icoPeriodInfo.getTotalElements());
		userRegisterDTO.setTotalElements(Math.toIntExact(icoPeriodInfo.getTotalElements()));

		for (ICOPeriodInfo periodInfo : icoPeriodInfo) {
			UserRegisterDTO users = new UserRegisterDTO();
			users.setId(periodInfo.getId());
			users.setIcoStart(periodInfo.getIcoStart());
			users.setIcoEnd(periodInfo.getIcoEnd());
			users.setIcoAllocationTokens(periodInfo.getIcoAllocationTokens());
			users.setIcoSoldTokens(periodInfo.getIcoSoldTokens());
			users.setIcoAvailableTokens(periodInfo.getIcoAvailableTokens());
			users.setStatus(periodInfo.getIcoStatus());
			list.add(users);
		}
		return list;
	}

	@Override
	public String updateICOPeriods(UserRegisterDTO userRegisterDTO) {

		HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		
		if (userRegisterDTO.getIcoEnd().after(userRegisterDTO.getIcoStart())) {
			if (registerInfo.getUserType().equalsIgnoreCase("A")) {
				ICOPeriodInfo icoPeriodInfo = new ICOPeriodInfo();
				if (userRegisterDTO.getId() == null) {
					icoPeriodInfo.setIcoAllocationTokens(userRegisterDTO.getIcoAllocationTokens());
					icoPeriodInfo.setIcoSoldTokens(0.0);
					icoPeriodInfo.setIcoAvailableTokens(userRegisterDTO.getIcoAllocationTokens());
					icoPeriodInfo.setIcoStart(userRegisterDTO.getIcoStart());
					icoPeriodInfo.setIcoEnd(userRegisterDTO.getIcoEnd());
					icoPeriodInfo.setIcoStatus(0);
					icoPeriodInfoRepo.save(icoPeriodInfo);
				} else {
					ICOPeriodInfo icoPeriod = icoPeriodInfoRepo.findOne(userRegisterDTO.getId());
					icoPeriod.setIcoAllocationTokens(userRegisterDTO.getIcoAllocationTokens());
					icoPeriod.setIcoSoldTokens(0.0);
					icoPeriod.setIcoAvailableTokens(
							icoPeriod.getIcoAvailableTokens() + userRegisterDTO.getIcoAllocationTokens());
					icoPeriod.setIcoStart(userRegisterDTO.getIcoStart());
					icoPeriod.setIcoEnd(userRegisterDTO.getIcoEnd());
					icoPeriodInfoRepo.save(icoPeriod);
				}
				return env.getProperty("update.charity.success");
			} else {
				return env.getProperty("not.admin");
			}
		}
		return env.getProperty("date.invalid");
	}

	@Override
	public boolean deleteICOPeriods(UserRegisterDTO userRegisterDTO) {

		ICOPeriodInfo icoPeriod = icoPeriodInfoRepo.findOne(userRegisterDTO.getId());
		if (userRegisterDTO.getId() != null) {
			icoPeriodInfoRepo.delete(icoPeriod.getId());
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean UpdateTheICOPeriodSimultanously() {
		// Create formatter
		DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("yyyy");
		// Zoned datetime instance
		ZonedDateTime currentDate = ZonedDateTime.now();
		// Get formatted String
		String year = FOMATTER.format(currentDate);
		System.out.println(year);
		try {
			ICOPeriodInfo icoPeriodInfo = icoPeriodInfoRepo.findOne(1);
			System.out.println(icoPeriodInfo.getIcoStatus());
			icoPeriodInfo.setIcoStatus(3435);
			System.out.println("3435");
			icoPeriodInfoRepo.save(icoPeriodInfo);
			System.out.println("after update");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("exception in " + e.getMessage());
		}
		return true;
	}

	@Override
	public String tokenAmountValidationForPurchase(TokenDTO tokenDTO) {
		int icoStatus = 1;
		ICOPeriodInfo icoPeriodInfo = icoPeriodInfoRepo.findICOPeriodInfoByicoStatus(icoStatus);
		if (icoPeriodInfo != null) {
			if (icoPeriodInfo.getIcoAvailableTokens() > tokenDTO.getTokenAmount()) {
				return env.getProperty("token.amount.success");
			}
			return "Token Balance in this slab is less than the you entered " + icoPeriodInfo.getIcoAvailableTokens()
					+ " BIOKKOIN";
		}
		return "ICO is not available. Please check the ICO slabs for more details";
	}
	
	@Override
	public String tokenAmountValidationForPurchaseForPayPal(PaypalDTO paypalDTO) {
		int icoStatus = 1;
		ICOPeriodInfo icoPeriodInfo = icoPeriodInfoRepo.findICOPeriodInfoByicoStatus(icoStatus);
		if (icoPeriodInfo != null) {
			if (icoPeriodInfo.getIcoAvailableTokens() > paypalDTO.getTokenAmount()) {
				return env.getProperty("token.amount.success");
			}
			return "Token Balance in this slab is less than the you entered " + icoPeriodInfo.getIcoAvailableTokens()
					+ " BIOKKOIN";
		}
		return "ICO is not available. Please check the ICO slabs for more details";
	}

	@Override
	public boolean userPurchaseTokens(TokenDTO tokenDTO)
			throws FileNotFoundException, IOException, ParseException, Exception {

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		LOG.info("Email : " + mail);
		tokenDTO.setEmailId(mail);
		String purchase = solidityHandler.purchaseTokens(tokenDTO);
		if (purchase != null) {
			return true;
		}
		return false;
	}

	@Override
	public JSONObject userPurchaseTokensPaymentGateway(PaypalDTO paypalDTO) throws Exception {

		HttpSession session = SessionCollector.find(paypalDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		LOG.info("Email : " + mail);
		paypalDTO.setEmailId(mail);
		JSONObject purchase = solidityHandler.purchaseTokensViaPaymentGateway(paypalDTO);
		if (purchase != null) {
			return purchase;
		}
		return null;
	}

	@Override
	public List<TokenDTO> purchaseList(TokenDTO tokenDTO) {
		List<TokenDTO> purchaseList = new ArrayList<TokenDTO>();
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		if (registerInfo.getUserType().equalsIgnoreCase("A")) {
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "purchasedDate"));
			Pageable pageable = new PageRequest(tokenDTO.getPageNum(), tokenDTO.getPageSize(), sort);

			Page<PurchaseTokenInfo> purchaseTokenInfo = purchaseTokenInfoRepository.findAll(pageable);
			tokenDTO.setTotalPages(purchaseTokenInfo.getTotalPages());
			System.out.println("purchaseTokenInfo.getTotalPages() : " + purchaseTokenInfo.getTotalPages()
					+ "TotalElements : " + purchaseTokenInfo.getTotalElements());
			tokenDTO.setTotalElements(Math.toIntExact(purchaseTokenInfo.getTotalElements()));

			for (PurchaseTokenInfo list : purchaseTokenInfo) {
				TokenDTO tokenDTOs = new TokenDTO();
				tokenDTOs.setTxStatus(list.getAsynchStatus());
				tokenDTOs.setTypeOfPurchase(list.getTypeOfPurchase());
				tokenDTOs.setTxDate(list.getPurchasedDate());
				tokenDTOs.setCryptoAmount(list.getCryptoAmount());
				tokenDTOs.setEmailId(list.getEmailId());
				tokenDTOs.setPurchasedTokens(list.getPurchaseTokens());
				if (!list.getEtherWalletAddress().isEmpty()) {
					tokenDTOs.setWalletAddress(list.getEtherWalletAddress());
				} else {
					tokenDTOs.setWalletAddress(list.getBtcWalletAddress());
				}
				purchaseList.add(tokenDTOs);
			}
		} else {
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "purchasedDate"));
			Pageable pageable = new PageRequest(tokenDTO.getPageNum(), tokenDTO.getPageSize(), sort);

			Page<PurchaseTokenInfo> purchaseTokenInfo = purchaseTokenInfoRepository
					.findPurchaseTokenInfoByEmailId(registerInfo.getEmailId(), pageable);
			tokenDTO.setTotalPages(purchaseTokenInfo.getTotalPages());
			System.out.println("purchaseTokenInfo.getTotalPages() : " + purchaseTokenInfo.getTotalPages()
					+ "TotalElements : " + purchaseTokenInfo.getTotalElements());
			tokenDTO.setTotalElements(Math.toIntExact(purchaseTokenInfo.getTotalElements()));

			for (PurchaseTokenInfo list : purchaseTokenInfo) {
				TokenDTO tokenDTOs = new TokenDTO();
				tokenDTOs.setTxStatus(list.getAsynchStatus());
				tokenDTOs.setTypeOfPurchase(list.getTypeOfPurchase());
				tokenDTOs.setTxDate(list.getPurchasedDate());
				tokenDTOs.setCryptoAmount(list.getCryptoAmount());
				tokenDTOs.setEmailId(list.getEmailId());
				tokenDTOs.setPurchasedTokens(list.getPurchaseTokens());
				if (!list.getEtherWalletAddress().isEmpty()) {
					tokenDTOs.setWalletAddress(list.getEtherWalletAddress());
				} else {
					tokenDTOs.setWalletAddress(list.getBtcWalletAddress());
				}
				purchaseList.add(tokenDTOs);
			}
		}
		return purchaseList;
	}

	@Override
	public List<TokenDTO> purchaseListFilter(TokenDTO tokenDTO) {
		List<TokenDTO> purchaseListFilter = new ArrayList<TokenDTO>();

		Sort sort = new Sort(new Sort.Order(Direction.ASC, "purchasedDate"));
		Pageable pageable = new PageRequest(tokenDTO.getPageNum(), tokenDTO.getPageSize(), sort);
		if (tokenDTO.getSearchText() != null) {
			Page<PurchaseTokenInfo> purchaseTokenInfo = purchaseTokenInfoRepository.findpurchaseList(
					tokenDTO.getSearchText(), tokenDTO.getSearchText(), tokenDTO.getSearchText(), pageable);
			tokenDTO.setTotalPages(purchaseTokenInfo.getTotalPages());
			System.out.println("purchaseTokenInfo.getTotalPages() : " + purchaseTokenInfo.getTotalPages()
					+ "TotalElements : " + purchaseTokenInfo.getTotalElements());
			tokenDTO.setTotalElements(Math.toIntExact(purchaseTokenInfo.getTotalElements()));

			for (PurchaseTokenInfo list : purchaseTokenInfo) {
				TokenDTO tokenDTOs = new TokenDTO();
				tokenDTOs.setTxStatus(list.getAsynchStatus());
				tokenDTOs.setTypeOfPurchase(list.getTypeOfPurchase());
				tokenDTOs.setTxDate(list.getPurchasedDate());
				tokenDTOs.setCryptoAmount(list.getCryptoAmount());
				tokenDTOs.setEmailId(list.getEmailId());
				tokenDTOs.setPurchasedTokens(list.getPurchaseTokens());
				if (!list.getEtherWalletAddress().isEmpty()) {
					tokenDTOs.setEthWalletAddress(list.getEtherWalletAddress());
				} else {
					tokenDTOs.setBtcWalletAddress(list.getBtcWalletAddress());
				}
				purchaseListFilter.add(tokenDTOs);
			}
		}

		return purchaseListFilter;
	}

	@Override
	public String oauthToken(PaypalDTO paypalDTO) throws Exception {

		URL url = new URL(env.getProperty("oauth.token.url"));
		HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setUseCaches(false);
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		urlConnection.setRequestProperty("Accept", "application/json");
		urlConnection.setRequestProperty("Accept-Language", "en_US");

		String userpass = env.getProperty("client.id") + ":" + env.getProperty("client.secret");
		String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
		urlConnection.setRequestProperty("Authorization", basicAuth);

		parameters = new Hashtable<String, String>();
		parameters.put("grant_type", "client_credentials");

		System.out.println("Json : " + parameters.toString());

		DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
		String urlParameters = buildUrlParameters();
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		StringBuilder sb = new StringBuilder();
		int HttpsResult = urlConnection.getResponseCode();
		System.out.println(HttpsResult);
		if (HttpsResult == HttpsURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Rsponse" + s);

			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(s);
			
			System.out.println("jsonObj" + jsonObj.toString());
			String accessToken = (String) jsonObj.get("access_token");
			
			paypalDTO.setAccessToken(accessToken);
			return accessToken;
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public static String buildUrlParameters() {
		String urlParameters = "";

		Enumeration enumeration = parameters.keys();

		String key = null;
		String value = null;
		String pair = null;

		boolean isFirstData = true;

		while (enumeration.hasMoreElements()) {
			key = (String) enumeration.nextElement();
			value = parameters.get(key);

			pair = key + "=" + value;

			if (isFirstData) // isFirstData = false;
			{
				urlParameters += pair;
			} else {
				urlParameters += "&" + pair;
			}

			isFirstData = false;
		}

		return urlParameters;
	}

}
