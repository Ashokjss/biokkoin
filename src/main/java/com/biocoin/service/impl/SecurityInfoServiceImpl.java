package com.biocoin.service.impl;

import java.io.Serializable;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.biocoin.model.SecurityInfo;
import com.biocoin.service.SecurityInfoService;

@Service
public class SecurityInfoServiceImpl extends HibernateServiceDaoImpl<SecurityInfo, Serializable> implements SecurityInfoService{
	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public SecurityInfo getSecurityInfoByEmailId(String emailId) {
		// TODO Auto-generated method stub
		
		org.hibernate.Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from SecurityInfo where emailId=:emailId").setParameter("emailId", emailId);
		
		return (SecurityInfo) query.uniqueResult();
	}

}
