
package com.biocoin.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.BitcoinInfo;
import com.biocoin.model.RegisterInfo;
import com.biocoin.repo.BitcoinInfoRepository;
import com.biocoin.repo.UserRegisterRepository;

@Service
public class BitCoinUtils {
	private static final Logger LOG = LoggerFactory.getLogger(BitCoinUtils.class);

	@Autowired
	private Environment env;

	@Autowired
	private UserRegisterRepository userRegisterlnfoRepo;

	@Autowired
	private BitcoinInfoRepository bitcoinInfoRepository;

	@Autowired
	private CurrentValueUtils currentValueUtils;

	public Boolean btcWalletCreation(UserRegisterDTO userRegisterDTO) throws IOException, ParseException {
		URL url = new URL(env.getProperty("create.btc.wallet"));
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");
		JSONObject json = new JSONObject();
		json.put("password", userRegisterDTO.getBtcWalletPassword());
		json.put("api_code", env.getProperty("api_code"));
		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		wr.write(json.toString());
		wr.flush();

		StringBuilder sb = new StringBuilder();
		int HttpResult = urlConnection.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Rsponse" + s);

			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(s);

			userRegisterDTO.setBtcwalletAddress(jsonObj.get("address").toString());
			userRegisterDTO.setBtcWalletGuid(jsonObj.get("guid").toString());
			return true;
		}
		return false;

	}

	public BigDecimal getBtcValue(TokenDTO tokenDTO) throws Exception {

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");
		RegisterInfo UserModelInfo = userRegisterlnfoRepo.findRegisterInfoByEmailIdIgnoreCase(email);
		URL url = new URL(
				"http://localhost:3000/merchant/" + EncryptDecrypt.decrypt(UserModelInfo.getBtcWalletGuid()).trim()
						+ "/balance?password=" + EncryptDecrypt.decrypt(UserModelInfo.getBtcWalletPassword()).trim());
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");

		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		// wr.write();
		wr.flush();

		StringBuilder sb = new StringBuilder();
		int HttpResult = urlConnection.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Rsponse" + s);
			JSONParser parser = new JSONParser();
			JSONObject json2 = (JSONObject) parser.parse(s);
			System.out.println("balance ###############==>12 " + json2.get("balance"));
			BigDecimal balance = new BigDecimal((BigInteger) json2.get("balance"));

			System.out.println("balance ###############==>34 " + json2.get("balance"));
			return balance;
		}
		return null;

	}

	public String getBtcWalletAddress(UserRegisterDTO userRegisterDTO) throws Exception {

		// HttpSession session =
		// SessionCollector.find(userRegisterDTO.getSessionId());
		// String email = (String) session.getAttribute("emailId");
		RegisterInfo UserModelInfo = userRegisterlnfoRepo
				.findRegisterInfoByEmailIdIgnoreCase(userRegisterDTO.getEmailId());
		URL url = new URL(
				"http://localhost:3000/merchant/" + EncryptDecrypt.decrypt(UserModelInfo.getBtcWalletGuid()).trim()
						+ "/list?password=" + EncryptDecrypt.decrypt(UserModelInfo.getBtcWalletPassword()).trim());
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");

		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		// wr.write();
		wr.flush();

		StringBuilder sb = new StringBuilder();
		int HttpResult = urlConnection.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Rsponse" + s);
			JSONParser parser = new JSONParser();
			JSONObject json2 = (JSONObject) parser.parse(s);
			JSONArray jsonArray = new JSONArray(json2.get("addresses").toString());
			org.json.JSONObject jsonObject = jsonArray.getJSONObject(0);
			System.out.println("address ==> " + jsonObject.get("address"));
			return jsonObject.get("address").toString();
		}
		return null;

	}

	public String TransferBtcValue(TokenDTO tokenDTO, Double bitcoinValue) throws Exception {

		RegisterInfo UserModelInfo = userRegisterlnfoRepo.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());

		URL url = new URL(
				"http://localhost:3000/merchant/" + EncryptDecrypt.decrypt(UserModelInfo.getBtcWalletGuid()).trim()
						+ "/payment?password=" + EncryptDecrypt.decrypt(UserModelInfo.getBtcWalletPassword()).trim()
						+ "&to=" + env.getProperty("admin.btc.address") + "&amount=" + bitcoinValue + "&from=0");
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");

		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		// wr.write();
		wr.flush();

		StringBuilder sb = new StringBuilder();
		int HttpResult = urlConnection.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Rsponse" + s);
			JSONParser parser = new JSONParser();
			JSONObject json2 = (JSONObject) parser.parse(s);
			String tx_hash = null;
			try {
				tx_hash = json2.get("tx_hash").toString();
			} catch (Exception e) {
				return null;
			}

			return tx_hash;
		}
		return null;

	}

	// ===================================================================================
	// ===================================================================================

	public boolean loginCrypto(UserRegisterDTO userRegisterDTO) throws Exception {

		String url = env.getProperty("bitgo.login.url");
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestMethod("POST");

		JSONObject cred = new JSONObject();
		cred.put("email", env.getProperty("bitgo.login.email"));
		cred.put("password", env.getProperty("bitgo.login.password"));
		cred.put("otp", env.getProperty("bitgo.login.otp"));

		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(cred.toString());
		wr.flush();

		// display what returns the POST request

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println("::::::::::::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("myResponse" + json.get("access_token"));

			String accessToken = json.get("access_token").toString();
			// ***************Encrypting the Token Type******************
			String tokenType = EncryptDecrypt.encrypt(json.get("token_type").toString());

			BitcoinInfo bitcoinInfo1 = bitcoinInfoRepository.findByEmailId(userRegisterDTO.getEmailId());
			if (bitcoinInfo1 != null) {
				bitcoinInfo1.setAccessTokens(accessToken);
				bitcoinInfo1.setTokenType(tokenType);
				bitcoinInfoRepository.save(bitcoinInfo1);
			} else {
				BitcoinInfo bitcoinInfo = new BitcoinInfo();
				bitcoinInfo.setEmailId(userRegisterDTO.getEmailId());
				bitcoinInfo.setAccessTokens(accessToken);
				bitcoinInfo.setTokenType(tokenType);
				bitcoinInfoRepository.save(bitcoinInfo);
			}
		} else {
			System.out.println(con.getResponseMessage());
			return false;
		}
		return true;

	}

	public boolean loginCrypto1(TokenDTO tokenDTO) throws Exception {

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");

		String url = env.getProperty("bitgo.login.url");
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestMethod("POST");

		JSONObject cred = new JSONObject();
		cred.put("email", env.getProperty("bitgo.login.email"));
		cred.put("password", env.getProperty("bitgo.login.password"));
		cred.put("otp", env.getProperty("bitgo.login.otp"));

		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(cred.toString());
		wr.flush();

		// display what returns the POST request

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println("::::::::::::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			LOG.info("myResponse" + json.get("access_token"));

			String accessToken = json.get("access_token").toString();
			// ***************Encrypting the Token Type******************
			String tokenType = EncryptDecrypt.encrypt(json.get("token_type").toString());

			BitcoinInfo bitcoinInfo1 = bitcoinInfoRepository.findByEmailId(email);
			if (bitcoinInfo1 != null) {
				bitcoinInfo1.setAccessTokens(accessToken);
				bitcoinInfo1.setTokenType(tokenType);
				bitcoinInfoRepository.save(bitcoinInfo1);
			} else {
				BitcoinInfo bitcoinInfo = new BitcoinInfo();
				bitcoinInfo.setEmailId(email);
				bitcoinInfo.setAccessTokens(accessToken);
				bitcoinInfo.setTokenType(tokenType);
				bitcoinInfoRepository.save(bitcoinInfo);
			}
		} else {
			System.out.println(con.getResponseMessage());
			return false;
		}
		return true;

	}
	// ==========================================================================
	// ==========================================================================

	public boolean createBitcoinWallet(UserRegisterDTO userRegisterDTO) throws Exception {

		BitcoinInfo bitcoinInfo = bitcoinInfoRepository.findByEmailId(userRegisterDTO.getEmailId());

		String tokenType = EncryptDecrypt.decrypt(bitcoinInfo.getTokenType());
		String accessToken = bitcoinInfo.getAccessTokens();

		String url = env.getProperty("bitcoin.wallet.generate.url");
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", tokenType + " " + accessToken);
		con.setRequestMethod("POST");

		JSONObject cred = new JSONObject();
		// JSONObject auth = new JSONObject();
		// JSONObject parent = new JSONObject();

		System.out.println("Label" + userRegisterDTO.getEmailId());

		cred.put("label", userRegisterDTO.getEmailId());
		cred.put("passphrase", userRegisterDTO.getEathwalletPassword());
		bitcoinInfo.setLabel(userRegisterDTO.getEmailId());

		System.out.println(accessToken);
		System.out.println(tokenType);

		// auth.put("Authorization", bitcoinCashInfo.getTokenType() + " " +
		// bitcoinCashInfo.getAccessTokens());
		// auth.put("body", cred.toString());
		// parent.put("auth", auth.toString());

		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(cred.toString());
		wr.flush();

		// System.out.println(parent.toString());

		// display what returns the POST request

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println(":::::::::::::::::::::::::::::::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("myResponse" + json.get("id"));

			// BitcoinCashInfo bitcoinCashInfo = new BitcoinCashInfo();

			JSONObject json1 = new JSONObject(json.get("receiveAddress").toString());
			System.out.println("myResponse" + json1.get("address"));
			System.out.println("myResponse" + json1.get("wallet"));

			String walletId = EncryptDecrypt.encrypt(json1.get("wallet").toString());
			String btcAddress = EncryptDecrypt.encrypt(json1.get("address").toString());
			String btcPassword = EncryptDecrypt.encrypt(userRegisterDTO.getEathwalletPassword());

			userRegisterDTO.setBtcwalletAddress(btcAddress);
			userRegisterDTO.setBtcWalletGuid(walletId);
			bitcoinInfo.setWalletId(walletId);
			bitcoinInfo.setBtcWalletAddress(btcAddress);
			bitcoinInfo.setBtcWalletPassword(btcPassword);
			bitcoinInfoRepository.save(bitcoinInfo);

		} else {
			System.out.println(con.getResponseMessage());
			System.out.println("Getting failure response in create btc wallet");
			return false;
		}
		return true;
	}

	public String getWalletAddress(UserRegisterDTO userRegisterDTO) throws Exception {

		System.out.println("Email Id ::::::::::::::::" + userRegisterDTO.getEmailId());

		BitcoinInfo bitcoinInfo = bitcoinInfoRepository.findByEmailId(userRegisterDTO.getEmailId());
		String tokenType = EncryptDecrypt.decrypt(bitcoinInfo.getTokenType());
		String accessToken = bitcoinInfo.getAccessTokens();
		String walletId = EncryptDecrypt.decrypt(bitcoinInfo.getWalletId());

		String url = env.getProperty("bitcoin.get.wallet.address") + walletId;
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Authorization", tokenType + " " + accessToken);
		con.setRequestMethod("GET");
 
		System.out.println(accessToken);
		System.out.println(tokenType);

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println("::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("myResponse" + json.get("id"));

			JSONObject json1 = new JSONObject(json.get("receiveAddress").toString());
			System.out.println("myResponse" + json1.get("address"));
			System.out.println("myResponse" + json1.get("wallet"));

			String walletIds = EncryptDecrypt.encrypt(json1.get("wallet").toString());
			String btcAddress = EncryptDecrypt.encrypt(json1.get("address").toString());

			bitcoinInfo.setWalletId(walletIds);
			bitcoinInfo.setBtcWalletAddress(btcAddress);
			bitcoinInfoRepository.save(bitcoinInfo);
			userRegisterDTO.setBtcwalletAddress(json1.get("address").toString());
			return json1.get("address").toString();
		} else
			System.out.println(con.getResponseMessage());
		System.out.println("Not Getting response in get bitcoin address");
		return null;
	}

	public BigDecimal getBitcoinBalance(TokenDTO tokenDTO) throws Exception {

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");

		BitcoinInfo bitcoinInfo = bitcoinInfoRepository.findByEmailId(email);

		String tokenType = EncryptDecrypt.decrypt(bitcoinInfo.getTokenType());
		String accessToken = bitcoinInfo.getAccessTokens();
		String walletId = EncryptDecrypt.decrypt(bitcoinInfo.getWalletId());

		String url = env.getProperty("bitcoin.get.wallet.address") + walletId;
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);

		con.setRequestProperty("Authorization", tokenType + " " + accessToken);
		con.setRequestMethod("GET");

		System.out.println(accessToken);
		System.out.println(tokenType);

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println("::::::::::::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("myResponse" + json.get("balance"));

			Integer balance = (Integer) json.get("balance");

			Double btcBalance = balance.doubleValue() / 100000000;
			// userRegisterDTO.setBitcoinWalletBalance(btcBalance);
			return BigDecimal.valueOf(btcBalance);
		} else
			System.out.println(con.getResponseMessage());
		System.out.println("Not getting response in getting btc balance");
		return null;
	}

	@SuppressWarnings("unused")
	public String btcSendCoins(TokenDTO tokenDTO) throws Exception {

		System.out.println("Email Id : " + tokenDTO.getEmailId());

		boolean unlock = unlock(tokenDTO);

		BitcoinInfo bitcoinInfo = bitcoinInfoRepository.findByEmailId(tokenDTO.getEmailId());

		String tokenType = EncryptDecrypt.decrypt(bitcoinInfo.getTokenType());
		String accessToken = bitcoinInfo.getAccessTokens();
		String walletId = EncryptDecrypt.decrypt(bitcoinInfo.getWalletId());
		String walletPassphrase = EncryptDecrypt.decrypt(bitcoinInfo.getBtcWalletPassword());

		String url = env.getProperty("bitcoin.wallet.sendcoins.url") + walletId + env.getProperty("sendcoins.url");
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", tokenType + " " + accessToken);
		con.setRequestMethod("POST");

		JSONObject cred = new JSONObject();
		// JSONObject auth = new JSONObject();
		// JSONObject parent = new JSONObject();

		// TokenRateInfo tokenRateInfo =
		// tokenRateInfoRopository.findBySaletype(tokenDTO.getSaleType());
		// Double ethValue = tokenDTO.getTokenAmount() *
		// tokenRateInfo.getUsdValue().doubleValue();

		Double btcValue = currentValueUtils.getBitcoinValueForOneDollar();
		System.out.println("Btc Value : " + btcValue);
		// Double btcValueForOneToken = btcValue * tokenDTO.getTokenAmount();
		// System.out.println("current Bch Value for 1 usd : " +
		// btcValueForOneToken);

		Double btcAmount = tokenDTO.getTokenAmount() * btcValue;

		System.out.println("currentBchValue : " + btcAmount);

		Double btc = btcAmount * 100000000;

		Integer bitcoin = btc.intValue();
		System.out.println("Btc Value : " + bitcoin);
		cred.put("address", env.getProperty("bitcoin.admin.wallet.address"));
		cred.put("amount", bitcoin);
		cred.put("walletPassphrase", walletPassphrase);
		// bitcoinCashInfo.setLabel(userRegisterDTO.getEmailId());

		System.out.println(bitcoinInfo.getAccessTokens());
		System.out.println(bitcoinInfo.getTokenType());

		// auth.put("Authorization", bitcoinCashInfo.getTokenType() + " " +
		// bitcoinCashInfo.getAccessTokens());
		// auth.put("body", cred.toString());
		// parent.put("auth", auth.toString());

		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(cred.toString());
		wr.flush();

		// display what returns the POST request

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println("::::::::::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("myResponse" + json.get("txid"));

			// BitcoinCashInfo bitcoinCashInfo = new BitcoinCashInfo();

			// JSONObject json1 = new
			// JSONObject(json.get("receiveAddress").toString());
			System.out.println("myResponse" + json.get("tx"));
			System.out.println("myResponse" + json.get("status"));

			// tokenDTO.setBtcAmount(btcAmount);

			String status = (String) json.get("status");
			return status;
		} else {
			System.out.println(con.getResponseMessage());
			System.out.println("::::::::::::");
			return null;
		}
	}

	public boolean unlock(TokenDTO tokenDTO) throws Exception {

		BitcoinInfo bitcoinInfo = bitcoinInfoRepository.findByEmailId(tokenDTO.getEmailId());

		String tokenType = EncryptDecrypt.decrypt(bitcoinInfo.getTokenType());
		String accessToken = bitcoinInfo.getAccessTokens();

		String url = env.getProperty("bitgo.unlock.url");
		URL object = new URL(url);

		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", tokenType + " " + accessToken);
		con.setRequestMethod("POST");

		JSONObject cred = new JSONObject();
		// JSONObject auth = new JSONObject();
		// JSONObject parent = new JSONObject();

		// cred.put("email", env.getProperty("bitgo.login.email"));
		// cred.put("password", env.getProperty("bitgo.login.password"));
		cred.put("otp", env.getProperty("bitgo.login.otp"));

		// auth.put("tenantName", "adm");
		// auth.put("passwordCredentials", cred.toString());

		// parent.put("auth", auth.toString());

		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(cred.toString());
		wr.flush();

		// display what returns the POST request

		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();

		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			System.out.println("::::::::::::" + sb.toString());
			JSONObject json = new JSONObject(sb.toString());
			System.out.println("myResponse" + json.get("session"));

			// BitcoinCashInfo bitcoinCashInfo1 =
			// bitcoinCashInfoRepository.findByEmailId(userRegisterDTO.getEmailId());
			// if(bitcoinCashInfo1 != null) {
			// bitcoinCashInfo1.setAccessTokens(json.get("access_token").toString());
			// bitcoinCashInfo1.setTokenType(json.get("token_type").toString());
			// bitcoinCashInfoRepository.save(bitcoinCashInfo1);
			//
			// } else {
			// BitcoinCashInfo bitcoinCashInfo = new BitcoinCashInfo();
			// bitcoinCashInfo.setEmailId(userRegisterDTO.getEmailId());
			// bitcoinCashInfo.setAccessTokens(json.get("access_token").toString());
			// bitcoinCashInfo.setTokenType(json.get("token_type").toString());
			// bitcoinCashInfoRepository.save(bitcoinCashInfo);
			// }
			//
			// BitcoinInfo bitcoinInfo1 =
			// bitcoinInfoRepository.findByEmailId(userRegisterDTO.getEmailId());
			// if(bitcoinInfo1 != null) {
			// bitcoinInfo1.setAccessTokens(json.get("access_token").toString());
			// bitcoinInfo1.setTokenType(json.get("token_type").toString());
			// bitcoinInfoRepository.save(bitcoinInfo1);
			// } else {
			// BitcoinInfo bitcoinInfo = new BitcoinInfo();
			// bitcoinInfo.setEmailId(userRegisterDTO.getEmailId());
			// bitcoinInfo.setAccessTokens(json.get("access_token").toString());
			// bitcoinInfo.setTokenType(json.get("token_type").toString());
			// bitcoinInfoRepository.save(bitcoinInfo);
			// }
		} else {
			System.out.println(con.getResponseMessage());
			return false;
		}
		return true;

	}

}
