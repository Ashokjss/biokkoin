package com.biocoin.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletCoinsReceivedEventListener;
import org.bitcoinj.wallet.listeners.WalletCoinsSentEventListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

import com.biocoin.dto.LoginDTO;
import com.biocoin.dto.PaypalDTO;
import com.biocoin.dto.StatusResponseDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.BiocoinValue;
import com.biocoin.model.BitcoinConfigInfo;
import com.biocoin.model.Config;
import com.biocoin.model.ExpirationDataInfo;
import com.biocoin.model.ICOPeriodInfo;
import com.biocoin.model.KycInfo;
import com.biocoin.model.PaymentsInfo;
import com.biocoin.model.PurchaseTokenInfo;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.SecurityInfo;
import com.biocoin.model.Token_info;
import com.biocoin.model.TransactionHistory;
import com.biocoin.model.UserRequestCoinInfo;
import com.biocoin.repo.BitcoinConfigInfoRepository;
import com.biocoin.repo.ConfigInfoRepository;
import com.biocoin.repo.ExpirationDataInfoRepo;
import com.biocoin.repo.ICOPeriodInfoRepo;
import com.biocoin.repo.KycInfoRepository;
import com.biocoin.repo.PaymentInfoRepo;
import com.biocoin.repo.PurchaseTokenInfoRepository;
import com.biocoin.repo.TokenAllocationForTopManagementsInfoRepo;
import com.biocoin.repo.TokensInfoRepo;
import com.biocoin.repo.TransactionInfoRepository;
import com.biocoin.repo.UserRegisterRepository;
import com.biocoin.repo.UserRequestCoinRepository;
import com.biocoin.service.BiocoinValueService;
import com.biocoin.service.EmailNotificationService;
import com.biocoin.service.ExpirationDataService;
import com.biocoin.service.SecurityInfoService;
import com.biocoin.service.TokenService;
import com.biocoin.service.TransactionHistoryService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.solidityHandler.SolidityHandler;
import com.biocoin.solidityToJava.Biokkoin;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

@Service
public class BioCoinUtils {
	static final Logger LOG = LoggerFactory.getLogger(BioCoinUtils.class);

	private final Web3j web3j = Web3j.build(new HttpService("https://rinkeby.infura.io/"));

	// BigInteger GASLIMIT = BigInteger.valueOf(4300000);
	// BigInteger GASPRICE = BigInteger.valueOf(2000000);

	BigInteger GASPRICE = Contract.GAS_PRICE;
	BigInteger GASLIMIT = Contract.GAS_LIMIT;

	@Autowired
	private Environment env;
	@Autowired
	private ExpirationDataService expirationservice;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private SessionCollector sessionCollector;
	@Autowired
	private SolidityHandler solidityHandler;
	@Autowired
	private ConfigInfoRepository configInfoRepository;
	@Autowired
	private BitcoinConfigInfoRepository bitcoinConfigInfoRepository;
	@Autowired
	SecurityInfoService securityInfoService;
	@Autowired
	private UserRequestCoinRepository userRequestCoinRepository;
	@Autowired
	private BioCoinUtils bioCoinUtils;
	@Autowired
	private UserRegisterService userregisterservice;
	@Autowired
	private CurrentValueUtils currentValueUtils;
	@Autowired
	private BiocoinValueService biocoinValueService;
	@Autowired
	private TransactionHistoryService transactionHistoryService;
	@Autowired
	private EmailNotificationService emailNotificationService;
	@Autowired
	private UserRegisterRepository userRegisterRepository;

	@Autowired
	private TokensInfoRepo tokensInfoRepo;
	@Autowired
	private ExpirationDataInfoRepo expirationDataInfoRepo;
	@Autowired
	private TokenAllocationForTopManagementsInfoRepo tokenAllocationForTopManagementsInfoRepo;
	@Autowired
	private ICOPeriodInfoRepo icoPeriodInfoRepo;
	@Autowired
	private KycInfoRepository kycInfoRepo;
	@Autowired
	private PaymentInfoRepo paymentInfoRepo;
	@Autowired
	private PurchaseTokenInfoRepository purchaseTokenInfoRepository;
	@Autowired
	private TransactionInfoRepository transactionInfoRepository;
	
	@Autowired
	private BitCoinUtils bitCoinUtils;
	

	public static WalletAppKit bitcoin;
	public static NetworkParameters params = TestNet3Params.get();
	static final String regex = "[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
	public static final MathContext DEFAULT_CONTEXT = new MathContext(0, RoundingMode.UNNECESSARY);
	public static final int DEFAULT_SCALE = Coin.SMALLEST_UNIT_EXPONENT;

	public static final BigDecimal satoshipercoinDecimal = new BigDecimal(Coin.COIN.value, DEFAULT_CONTEXT);

	public String createBitcoinWallet(RegisterInfo registerInfo) {
		if (bitcoin == null) {

			BitcoinConfigInfo bitcoinConfigInfo = bitcoinConfigInfoRepository
					.findBitcoinConfigByConfigKey("walletfile");
			if (bitcoinConfigInfo != null) {
				String bitcoinwalletFileLocation = bitcoinConfigInfo.getConfigValue();
				// System.out.println("bitcoinwalletFileLocation " +
				// bitcoinwalletFileLocation);

				bitcoin = new WalletAppKit(params, new File(bitcoinwalletFileLocation + "."),
						registerInfo.getId().toString()) {

					@Override
					public void onSetupCompleted() {
						bitcoin.wallet().allowSpendingUnconfirmedTransactions();
						// initializeWallet();
					}
				};
			}
			bitcoin.startAsync();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			String receiveAddress = null;
			if (!bitcoin.wallet().currentReceiveAddress().toString().isEmpty()) {
				receiveAddress = bitcoin.wallet().currentReceiveAddress().toString();

			}
			return receiveAddress;
		} else {

			bitcoin.stopAsync();
			bitcoin.awaitTerminated();

			BitcoinConfigInfo bitcoinConfigInfo = bitcoinConfigInfoRepository
					.findBitcoinConfigByConfigKey("walletfile");
			if (bitcoinConfigInfo != null) {
				String bitcoinwalletFileLocation = bitcoinConfigInfo.getConfigValue();

				bitcoin = new WalletAppKit(params, new File(bitcoinwalletFileLocation + "."),
						registerInfo.getId().toString()) {

					@Override
					public void onSetupCompleted() {
						bitcoin.wallet().allowSpendingUnconfirmedTransactions();
					}
				};
			}
			bitcoin.startAsync();
			try {
				Thread.sleep(1000);
			} catch (

			InterruptedException e) {
				e.printStackTrace();
			}
			String receiveAddress = null;
			if (!bitcoin.wallet().currentReceiveAddress().toString().isEmpty()) {
				receiveAddress = bitcoin.wallet().currentReceiveAddress().toString();

			}
			return receiveAddress;
		}
	}

	public void initializeWallet() {
		Wallet wallet = bitcoin.wallet();
		LOG.info("Current Receive Address  :" + wallet.currentReceiveAddress().toString());
	}

	public void getRefreshWallet() {
		System.out.println("Inside Refresh Wallet");
		bitcoin.wallet().addCoinsReceivedEventListener(new WalletCoinsReceivedEventListener() {

			@Override
			public void onCoinsReceived(Wallet arg0, Transaction arg1, Coin arg2, Coin arg3) {

			}
		});

		bitcoin.wallet().addCoinsSentEventListener(new WalletCoinsSentEventListener() {

			@Override
			public void onCoinsSent(Wallet arg0, Transaction arg1, Coin arg2, Coin arg3) {

			}
		});
	}

	public String getWalletAddress(String fileLocation, String fileName)
			throws FileNotFoundException, IOException, ParseException {
		try {
			/*
			 * fileLocation = fileLocation.replace("/", "\\");
			 * System.out.println("wallet created" + fileLocation); JSONParser
			 * parser = new JSONParser(); // Object object = parser.parse(new //
			 * FileReader(WalletUtils.getMainnetKeyDirectory() + "//" + //
			 * fileName)); Object object = parser.parse(new
			 * FileReader(fileLocation + "//" + fileName)); JSONObject
			 * jsonObject = (JSONObject) object;
			 */
			String[] fetchAddress = fileName.split("--");
			String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];

			String address = "0x" + getAddress;

			return address;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	public boolean validateRegistrationParam(UserRegisterDTO userRegisterDTO) {

		if (userRegisterDTO.getFirstName() != null && StringUtils.isNotBlank(userRegisterDTO.getFirstName())
				&& userRegisterDTO.getLastName() != null && StringUtils.isNotBlank(userRegisterDTO.getLastName())
				&& userRegisterDTO.getEmailId() != null && StringUtils.isNotBlank(userRegisterDTO.getEmailId())
				&& userRegisterDTO.getMobileno() != null
				&& StringUtils.isNotBlank(userRegisterDTO.getMobileno().toString())
				&& userRegisterDTO.getPassword() != null && StringUtils.isNotBlank(userRegisterDTO.getPassword())
				&& userRegisterDTO.getConfirmPassword() != null
				&& StringUtils.isNotBlank(userRegisterDTO.getConfirmPassword())
				&& userRegisterDTO.getEathwalletPassword() != null
				&& StringUtils.isNotBlank(userRegisterDTO.getEathwalletPassword())
				&& userRegisterDTO.getConfirmEathwalletPassword() != null
				&& StringUtils.isNotBlank(userRegisterDTO.getConfirmEathwalletPassword())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateEmail(String emailId) {
		emailId = emailId.replaceFirst("^ *", "");
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(emailId);
		LOG.info(emailId + " : " + matcher.matches());
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isEmailId(String emailId) {
		emailId = emailId.replaceFirst("^ *", "");
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(emailId);
		LOG.info(emailId + " : " + matcher.matches());
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validateConfirmPassword(UserRegisterDTO userRegisterDTO) {
		if (userRegisterDTO.getPassword().equals(userRegisterDTO.getConfirmPassword())) {
			return true;
		}
		return false;
	}

	public boolean validateConfirmWalletPassword(UserRegisterDTO userRegisterDTO) {
		if (userRegisterDTO.getEathwalletPassword().equals(userRegisterDTO.getConfirmEathwalletPassword())) {
			return true;
		}
		return false;
	}

	public boolean isResetPasswordLinkVerified(UserRegisterDTO userregisterdto) throws Exception {
		/*
		 * String decryptedToken =
		 * EncryptDecrypt.decrypt(userregisterdto.getTokenAddress().replaceAll(
		 * "\\s","+")); System.out.println("decryptedToken:::::::::::::::::"+
		 * decryptedToken);
		 */

		ExpirationDataInfo expirationDataInfo = expirationservice
				.getexpDataInfoByUseerEmailId(EncryptDecrypt.decrypt(userregisterdto.getEmailId()));

		if (!expirationDataInfo.isTokenStatus()) {
			return true;
		}
		return false;
	}

	public boolean validateTime(UserRegisterDTO userregisterdto) throws Exception {

		ExpirationDataInfo expirationDataInfo = expirationservice
				.getexpDataInfoByUseerEmailId(EncryptDecrypt.decrypt(userregisterdto.getEmailId()));

		Date expiredDate = expirationDataInfo.getExpiredDate();

		Date date = new Date();

		if (expiredDate.before(date)) {
			return false;
		}
		userregisterdto.setEmailId(expirationDataInfo.getEmailId());
		userregisterdto.setTokenAddress(expirationDataInfo.getToken());
		return true;
	}

	public boolean validateResetPassword(UserRegisterDTO userregisterdto) {

		Pattern pattern = Pattern.compile("[a-z A-Z 0-9 @#$%&*~^!+-_~:;><?].{5,15}");
		Matcher matcher = pattern.matcher(userregisterdto.getPassword());
		LOG.info(userregisterdto.getPassword() + " : " + matcher.matches());
		if (matcher.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isResetPassword(UserRegisterDTO userregisterdto) throws Exception {
		ExpirationDataInfo expirationDataInfo = expirationservice
				.getexpDataInfoByUseerEmailId(EncryptDecrypt.decrypt(userregisterdto.getEmailId()));

		RegisterInfo registerinfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(EncryptDecrypt.decrypt(userregisterdto.getEmailId()));
		String changePassword = userregisterdto.getPassword();
		String confirmChangePassword = userregisterdto.getConfirmPassword();
		if (changePassword.equals(confirmChangePassword)) {
			try {
				String encryptPassword = EncryptDecrypt.encrypt(changePassword);
				System.out.println("encryptPassword : " + encryptPassword);
				if (encryptPassword != null) {

					registerinfo.setPassword(encryptPassword);
					userRegisterRepository.save(registerinfo);

					expirationDataInfo.setTokenStatus(true);
					expirationDataInfoRepo.save(expirationDataInfo);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return true;
		}
		return false;
	}

	public boolean validateLoginParam(UserRegisterDTO userRegisterDTO) {
		if (userRegisterDTO.getEmailId() != null && StringUtils.isNotBlank(userRegisterDTO.getEmailId())
				&& userRegisterDTO.getPassword() != null && StringUtils.isNotBlank(userRegisterDTO.getPassword())) {
			return true;
		} else {
			return false;
		}
	}

	public boolean generateSecureKey(UserRegisterDTO userRegisterDTO) {
		try {

			GoogleAuthenticator gAuth = new GoogleAuthenticator();
			final GoogleAuthenticatorKey key = gAuth.createCredentials();
			String secretKey = key.getKey();

			int code = gAuth.getTotpPassword(secretKey);

			System.out.println("seckey" + code);

			// int code = gAuth.getTotpPassword(secretKey);

			// String ACCOUNT_SID = "AC2701ee97af51bac61ab9bbd40a8f811f";
			// String AUTH_TOKEN = "936c474f3b7a5a32215cffb5cbab6494";
			// String AUTH_TOKEN = "b741f4a591bf956f1e02b25459515607";
			// String ACCOUNT_SID = "ACb4cbd2f47c12f58a664e38754b9b1a03";
			/*
			 * int length = 8; String numbers = "0123456789";
			 * 
			 * Random rndm_method = new Random(); String otpNumber = ""; char[]
			 * otp = new char[length]; for (int i = 0; i < length; i++) { otp[i]
			 * = numbers.charAt(rndm_method.nextInt(numbers.length())); }
			 * 
			 * for (int i = 0; i < length; i++) { otpNumber = otpNumber +
			 * otp[i]; }
			 */
			System.out.println("1");

			// Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
			System.out.println("2");
			// Message message = Message
			// .creator(new PhoneNumber("+919578195169"), // to
			// new PhoneNumber("+14178154149"), // from
			// "One Time Password for BIO Coin Login is " + " " + code
			// + " Please use this password to complete the Login. Please do not
			// share this with anyone")
			// .create();

			boolean isEmailSent = emailNotificationService.sendEmailforOTP(userRegisterDTO.getEmailId(),
					"Login info from BIOKKOIN", code);
			if (isEmailSent) {
				System.out.println("3");
				userRegisterDTO.setSecuredKey(code);
				System.out.println("Secured key--->" + code);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Exception is------->" + e.toString());
			return false;
		}

	}

	public boolean validateSecurityKey(UserRegisterDTO userRegisterDTO) {

		if (userRegisterDTO.getSecuredKey() != null
				&& StringUtils.isNotBlank(userRegisterDTO.getSecuredKey().toString())) {
			return true;
		}
		return false;
	}

	public LoginDTO validateSecuredKey(UserRegisterDTO userRegisterDTO, HttpServletRequest request) throws Exception {

		LoginDTO responseDTO = new LoginDTO();
		TokenDTO tokenDTO = new TokenDTO();
		SecurityInfo securityInfo = securityInfoService.getSecurityInfoByEmailId(userRegisterDTO.getEmailId());

		long first = securityInfo.getCreatedTime().getTime() / 1000;
		long second = new Date().getTime() / 1000;

		long diff = second - first;

		if (diff <= 180) {

			if (userRegisterDTO.getSecuredKey().equals(securityInfo.getSecuredKey())) {
				KycInfo kycInfo = kycInfoRepo.findByEmailId(userRegisterDTO.getEmailId());
				Config config = configInfoRepository.findConfigByConfigKey("walletfile");
				RegisterInfo registerInfo = userregisterservice
						.isAccountExistCheckByEmailId(userRegisterDTO.getEmailId());

				if (kycInfo != null) {
					responseDTO.setKycStatus(kycInfo.getKycStatus());
				} else {
					responseDTO.setKycStatus(4);
				}

				if (registerInfo != null && config != null) {

					HttpSession session = request.getSession();
					session.setAttribute("emailId", registerInfo.getEmailId());
					HttpSessionEvent event = new HttpSessionEvent(session);
					sessionCollector.sessionCreated(event);
					responseDTO.setUserType(registerInfo.getUserType());
					responseDTO.setFirstName(registerInfo.getFirstName());
					responseDTO.setLastName(registerInfo.getLastName());
					responseDTO.setEmailId(registerInfo.getEmailId());
					String decryptWalletAddress = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());
					String walletAddress = bioCoinUtils.getWalletAddress(config.getConfigValue(), decryptWalletAddress);
					if (walletAddress != null) {
						userRegisterDTO.setEathwalletAddress(walletAddress);
						tokenDTO.setEthWalletAddress(walletAddress);
						responseDTO.setWalletAddress(walletAddress);
					}

					String btcAddress = bitCoinUtils.getWalletAddress(userRegisterDTO);
					if (btcAddress != null) {
						responseDTO.setBitcoinWalletReceivingAddress(btcAddress);
					}

					List<UserRequestCoinInfo> userRequestCoinInfo = userRequestCoinRepository
							.findByToAddressAndTranscationType(registerInfo.getDummywalletaddress(), 0);
					responseDTO.setRequestCoinCount(userRequestCoinInfo.size());
					responseDTO.setSessionId(session.getId());
//					QRCode qrcode = qrCodeRepository.findQRCodeByQrKey("QRKey");

					String qrcodepath = env.getProperty("qrcode.url") + "//" + env.getProperty("user.qrcode.folder") + "//" + registerInfo.getId() + "//"
							+ registerInfo.getId() + ".png";
					responseDTO.setQrCode(qrcodepath);
					responseDTO.setStatus("success");

					return responseDTO;

				}
			}
		}
		responseDTO.setStatus("failed");
		return responseDTO;

	}

	public boolean validateTokenParam(TokenDTO tokenDTO) {

		if (tokenDTO.getRequestToken() != null && (tokenDTO.getSelectTransactionType().trim().equals("BTC")
				|| tokenDTO.getSelectTransactionType().trim().equals("ETH"))) {
			return true;
		} else {
			return false;
		}
	}

	// Bitcoin Balance
	@SuppressWarnings("unused")
	public BigDecimal bitcoinBalance(TokenDTO tokenDTO) {

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");
		RegisterInfo regiObj = userregisterservice.isAccountExistCheckByEmailId(email);
		createBitcoinWallet(regiObj);

		// tokenDTO.setBitcoinWalletReceivingAddress(bitwallet);
		@SuppressWarnings("static-access")
		BigDecimal satoshi = new BigDecimal(bitcoin.wallet().getBalance().getValue(), bioCoinUtils.DEFAULT_CONTEXT);

		BigDecimal bitcoinBalance = BigDecimal.valueOf(0.0);
		// satoshi.divide(satoshipercoinDecimal, DEFAULT_SCALE,
		// RoundingMode.UNNECESSARY);

		tokenDTO.setBitcoinBalance(bitcoinBalance);

		if (bitcoinBalance != null) {
			return bitcoinBalance;
		} else
			return null;
	}

	// EtherBalance

	public String etherbalance(TokenDTO tokenDTO) throws IOException, CipherException, Exception {

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo regInfo = userregisterservice.isAccountExistCheckByEmailId(mail);
		if (regInfo != null) {
			String etherwalletaddress = EncryptDecrypt.decrypt(regInfo.getEthWalletAddress());
			String[] fetchAddress = etherwalletaddress.split("--");
			String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];
			EthGetBalance ethgetbalance;
			ethgetbalance = web3j.ethGetBalance("0x" + getAddress, DefaultBlockParameterName.LATEST).sendAsync().get();
			BigInteger wei = ethgetbalance.getBalance();
			String s = wei.toString();
			BigDecimal etherbalance = Convert.fromWei(s, Convert.Unit.ETHER);

			// double currenteathervalue =
			// currentValueUtils.getEtherValueForOneDollar();
			// Double etherToUsd = (currenteathervalue / 100) *
			// etherbalance.doubleValue();

			tokenDTO.setEtherbalance(new BigDecimal(etherbalance.toString()));
			// tokenDTO.setEthToUSD(etherToUsd);

			return env.getProperty("ether.balance.success");
		} else {
			return env.getProperty("ether.balance.failed");
		}
	}

	// TokenBalance

	public String tokenbalance(TokenDTO tokenDTO) throws IOException, CipherException, Exception {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo regInfo = userregisterservice.isAccountExistCheckByEmailId(mail);
		if (regInfo.getUserType().equalsIgnoreCase("U")) {
			String password = EncryptDecrypt.decrypt(regInfo.getEthWalletPassword());
			Config config = configInfoRepository.findConfigByConfigKey("walletfile");
			String etherwalletaddress = EncryptDecrypt.decrypt(regInfo.getEthWalletAddress());
			String[] fetchAddress = etherwalletaddress.split("--");
			String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];
			Credentials credential = WalletUtils.loadCredentials(password,
					new File(config.getConfigValue() + "//" + etherwalletaddress));

			Biokkoin biokkoin = Biokkoin.load(env.getProperty("token.address"), web3j, credential, GASPRICE, GASLIMIT);

			BigInteger balance = biokkoin.balanceOf("0x" + getAddress).send();
			Double tokenBalance = balance.doubleValue() / 100000000;

			RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
			registerInfo.setTokenBalance(Double.parseDouble(tokenBalance.toString()));
			userRegisterRepository.save(registerInfo);

			tokenDTO.setTokenBalance(Double.parseDouble(tokenBalance.toString()));

			/* Create formatter */
			SimpleDateFormat sql = new SimpleDateFormat("MM-dd-yyyy");
			Date date = new Date();
			String dates = sql.format(date);
			System.out.println("Current Date : " + dates);

			Token_info token_info = tokensInfoRepo.findOne(1);
			String vestingEndDate = sql.format(token_info.getLockedTokensEndDate());
			System.out.println("Vesting period End Date : " + vestingEndDate);

			if (regInfo.getLockedTokenBalance() > 0) {
				if (date.after(token_info.getLockedTokensEndDate())) {

					System.out.println("Vesting period Ends, so locked balance will be released once Vesting ends");
					regInfo.setLockedTokenBalance(0.0);
					userRegisterRepository.save(regInfo);

				} else {
					System.out.println("Not entered into Vesting period");
				}
			} else {
				System.out.println("No Vesting Balances.....");
			}

			return env.getProperty("token.balance.success");
		} else {

			return env.getProperty("token.balance.failed");
		}

	}

	public String tokenbalanceForAdmin(TokenDTO tokenDTO) throws Exception {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		if (registerInfo.getUserType().equalsIgnoreCase("A")) {
			Token_info token_info = tokensInfoRepo.findOne(1);

			String password = EncryptDecrypt.decrypt(registerInfo.getEthWalletPassword());
			Config config = configInfoRepository.findConfigByConfigKey("walletfile");
			String etherwalletaddress = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());
			String[] fetchAddress = etherwalletaddress.split("--");
			String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];
			Credentials credential = WalletUtils.loadCredentials(password,
					new File(config.getConfigValue() + "//" + etherwalletaddress));

			Biokkoin biokkoin = Biokkoin.load(env.getProperty("token.address"), web3j, credential, GASPRICE, GASLIMIT);

			BigInteger balance = biokkoin.balanceOf("0x" + getAddress).send();
			Double tokenBalance = balance.doubleValue() / 100000000;

			Date currentDate = new Date();

			Date icoEndDate = token_info.getEndDate();
			if (icoEndDate.after(currentDate)) {

				tokenDTO.setTokenBalance(Double.parseDouble(tokenBalance.toString()));

				registerInfo.setTokenBalance(Double.parseDouble(tokenBalance.toString()));
				userRegisterRepository.save(registerInfo);

			} else if (icoEndDate.before(currentDate)) {

				Double b = tokenBalance - token_info.getIcoSoldTokens();
				tokenDTO.setTokenBalance(b);

				registerInfo.setTokenBalance(b);
				userRegisterRepository.save(registerInfo);

			}
			return env.getProperty("token.balance.success");
		}
		return env.getProperty("token.balance.failed");
	}

	public String adminDashboardDetails(TokenDTO tokenDTO) {

		Integer count = tokenAllocationForTopManagementsInfoRepo
				.countTokenAllocationForTopManagementsInfoByTypeOfVerifications(env.getProperty("top.members"));
		if (count != null) {
			System.out.println("count : " + count);
			tokenDTO.setTopManagementCounts(count);

			Token_info token_info = tokensInfoRepo.findOne(1);
			tokenDTO.setTotalTokens(token_info.getTotalToken());
			tokenDTO.setICOAvailableTokens(token_info.getIcoAvailableTokens());
			tokenDTO.setTotalSoldTokens(token_info.getTotalSoldTokens());

			Integer r = userRegisterRepository.countRegisterInfoByUserType("U");
			System.out.println("R : " + r);
			tokenDTO.setTotalUserCount(new BigInteger(r.toString()));
			return env.getProperty("balance.message");
		}

		return env.getProperty("balance.failure");
	}

	public boolean validatePasswordPrams(TokenDTO tokenDTO) throws Exception {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");

		String etherWalletPassword = tokenDTO.getEthWalletPassword();

		RegisterInfo registerinfo = userregisterservice.isAccountExistCheckByEmailId(mail);
		try {
			if (registerinfo != null) {

				String decryptWalletPassword = EncryptDecrypt.decrypt(registerinfo.getEthWalletPassword());
				if (decryptWalletPassword.equals(etherWalletPassword)) {
					tokenDTO.setEthWalletPassword(etherWalletPassword);

					return true;

				}
				return false;

			}
		} catch (Exception e) {

		}
		return false;
	}

	public boolean bitcoinBalanceCheck(TokenDTO tokenDTO) throws JSONException, IOException {

		if (tokenDTO.getSessionId() != null) {
			Double requestToken = tokenDTO.getRequestToken();
			BigDecimal reqtoken = new BigDecimal(requestToken);
			// UserRegisterDTO userRegiDTO = new UserRegisterDTO();

			double currentbitvalue = currentValueUtils.getBitcoinValueForOneDollar();
			// 1Bio coin=$15USD taken from biocoinvalue table dynamically
			BiocoinValue biocoinval = biocoinValueService.findById(1);
			double current_biocoin_ethvalue = (biocoinval.getBiocoin_value()) * (currentbitvalue);

			BigDecimal k = BigDecimal.valueOf(current_biocoin_ethvalue);

			BigDecimal resultethvalue = (k).multiply(reqtoken);

			BigDecimal bitcoinBalance = bitcoinBalance(tokenDTO);

			// BigInteger btcval=bitcoinBalance.toBigInteger();

			int res = bitcoinBalance.compareTo(resultethvalue);
			if (res == 1) {
				return true;
			}
			return false;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public boolean contributeToken(TokenDTO tokenDTO) throws Exception {

		Config configInfo = configInfoRepository.findConfigByConfigKey("walletFile");
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());

		String email = (String) session.getAttribute("emailId");

		RegisterInfo registerObj = userregisterservice.isAccountExistCheckByEmailId(email);

		if (tokenDTO.getSelectTransactionType().equals("ETH")) {

			String etherWalletAddress = EncryptDecrypt.decrypt(registerObj.getEthWalletAddress());
			String etherWalletPassword = EncryptDecrypt.decrypt(registerObj.getEthWalletPassword());

			String ethWalletAddress = bioCoinUtils.getWalletAddress(etherWalletAddress);

			BigInteger reqToken = new BigDecimal(tokenDTO.getRequestToken()).toBigInteger();

			BigDecimal reqtoken = new BigDecimal(reqToken);
			double currentethvalue = currentValueUtils.getEtherValueForOneDollar();

			// 1Bio coin=$15USD taken from biocoinvalue table dynamically
			BiocoinValue biocoinval = biocoinValueService.findById(1);
			double current_biocoin_ethvalue = (biocoinval.getBiocoin_value()) * (currentethvalue);

			BigDecimal k = BigDecimal.valueOf(current_biocoin_ethvalue);

			BigDecimal resultethvalue = (k).multiply(reqtoken);

			DecimalFormat df = new DecimalFormat("#.###############");
			BigDecimal amt = new BigDecimal(df.format(resultethvalue));

			TransactionHistory txHistory = new TransactionHistory();

			CompletableFuture.supplyAsync(() -> {

				txHistory.setFromAddress("0x" + env.getProperty("main.address"));
				txHistory.setToAddress(ethWalletAddress);
				txHistory.setCryptoAmount(amt);
				txHistory.setTransferToken(reqToken.doubleValue());
				txHistory.setTxDate(new Date());
				txHistory.setPaymentMode(tokenDTO.getSelectTransactionType());
				txHistory.setTxstatus("Pending");
				// txHistory.setRegisterInfo(registerObj);
				transactionHistoryService.save(txHistory);

				try {

					Credentials credentials = WalletUtils.loadCredentials(etherWalletPassword,
							new File(configInfo.getConfigValue() + "//" + etherWalletAddress));
					TransactionReceipt transactionReceipt = Transfer
							.sendFunds(web3j, credentials, env.getProperty("main.address"), amt, Convert.Unit.ETHER)
							.send();

					if (transactionReceipt != null) {

						tokenDTO.setToAddress(ethWalletAddress);

						boolean isUserTokenTransfer = solidityHandler
								.TransferCoinfromAdminWalletTouserWallet(ethWalletAddress, reqToken);

						if (isUserTokenTransfer) {

							// Token info table

							txHistory.setTxstatus("Success");

							Token_info token_info = tokenService.findById(1);

							// Double available_tokens =
							// token_info.getAvailableTokens();
							Double sold_tokens = token_info.getIcoSoldTokens();
							Double totalsold_tokens = token_info.getTotalSoldTokens();

							// token_info.setAvailableTokens(available_tokens -
							// reqToken.doubleValue());
							token_info.setIcoSoldTokens(sold_tokens + reqToken.doubleValue());
							token_info.setTotalSoldTokens(totalsold_tokens + reqToken.doubleValue());
							tokenService.update(token_info);

							boolean isEmailSent = emailNotificationService.sendTransferCoinEmail(
									registerObj.getEmailId(), "ALERT MESSAGE FROM BIOCOIN ADMIN !",
									"0x" + env.getProperty("main.address"), ethWalletAddress, reqToken, amt, new Date(),
									tokenDTO.getSelectTransactionType(), txHistory.getTxstatus());

						}

						else {

							txHistory.setTxstatus("Fail");

							boolean isEmailSent = emailNotificationService.sendTransferCoinEmail(
									registerObj.getEmailId(), "ALERT MESSAGE FROM BIOCOIN ADMIN !",
									"0x" + env.getProperty("main.address"), ethWalletAddress, reqToken, amt, new Date(),
									tokenDTO.getSelectTransactionType(), txHistory.getTxstatus());

							// return false;
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return "call blackchain";

			}).thenAccept(product -> {
				if (txHistory.getTxstatus().equalsIgnoreCase("Success")) {
					txHistory.setTxDate(new Date());
					txHistory.setTxstatus("Success");
				}
				if (txHistory.getTxstatus().equalsIgnoreCase("Fail")) {
					txHistory.setTxDate(new Date());
					txHistory.setTxstatus("Fail");
				}
				transactionHistoryService.update(txHistory);

			});
			return true;

		}

		else {

			Address destination = Address.fromBase58(params, env.getProperty("bitcoin.receving.address"));
			if (destination == null) {
				StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
				statusResponseDTO.setBitcoinResponse(env.getProperty("address.not.found"));
			}
			BigInteger requestToken = new BigDecimal(tokenDTO.getRequestToken()).toBigInteger();
			BigDecimal reqtoken = new BigDecimal(requestToken);
			double currentbitvalue = currentValueUtils.getBitcoinValueForOneDollar();
			// 1Bio coin=$15USD taken from biocoinvalue table dynamically
			BiocoinValue biocoinval = biocoinValueService.findById(1);
			double current_biocoin_ethvalue = (biocoinval.getBiocoin_value()) * (currentbitvalue);
			BigDecimal k = BigDecimal.valueOf(current_biocoin_ethvalue);
			BigDecimal resultbitcoinvalue = (k).multiply(reqtoken);

			DecimalFormat df = new DecimalFormat("#.########");
			Coin transferAmount = Coin.parseCoin(df.format(resultbitcoinvalue));

			String bitcoinReveingaddress = createBitcoinWallet(registerObj);

			tokenDTO.setFromAddress(bitcoinReveingaddress);

			String etherWalletAddress = EncryptDecrypt.decrypt(registerObj.getEthWalletAddress());
			String etherWalletPassword = EncryptDecrypt.decrypt(registerObj.getEthWalletPassword());
			String ethWalletAddress = bioCoinUtils.getWalletAddress(etherWalletAddress);
			BigInteger reqToken = new BigDecimal(tokenDTO.getRequestToken()).toBigInteger();
			TransactionHistory txHistory = new TransactionHistory();
			CompletableFuture.supplyAsync(() -> {

				// TransactionHistory txHistory=new TransactionHistory();
				txHistory.setFromAddress("0x" + env.getProperty("main.address"));
				txHistory.setToAddress(ethWalletAddress);
				txHistory.setCryptoAmount(resultbitcoinvalue);
				txHistory.setTransferToken(reqToken.doubleValue());
				txHistory.setTxDate(new Date());
				txHistory.setPaymentMode(tokenDTO.getSelectTransactionType());
				txHistory.setTxstatus("Pending");
				// txHistory.setRegisterInfo(registerObj);
				transactionHistoryService.save(txHistory);

				Wallet.SendResult sendResult;
				try {
					sendResult = bitcoin.wallet().sendCoins(bitcoin.peerGroup(), destination, transferAmount);

				} catch (InsufficientMoneyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				boolean isUserTokenTransfer = solidityHandler.TransferCoinfromAdminWalletTouserWallet(ethWalletAddress,
						reqToken);

				if (isUserTokenTransfer) {

					// txHistory.setTxDate(new Date());

					txHistory.setTxstatus("Success");

					// Token info table

					Token_info token_info = tokenService.findById(1);

					// Double available_tokens =
					// token_info.getAvailableTokens();
					Double sold_tokens = token_info.getIcoSoldTokens();
					Double totalsold_tokens = token_info.getTotalSoldTokens();

					// token_info.setAvailableTokens(available_tokens -
					// reqToken.doubleValue());
					token_info.setIcoSoldTokens(sold_tokens + reqToken.doubleValue());
					token_info.setTotalSoldTokens(totalsold_tokens + reqToken.doubleValue());
					tokenService.update(token_info);

					return true;
				}

				else {

					// txHistory.setTxDate(new Date());

					txHistory.setTxstatus("Fail");

					// transactionHistoryService.update(txHistory);
					return false;
				}

			}).thenAccept(product -> {

				if (txHistory.getTxstatus().equalsIgnoreCase("Success")) {
					txHistory.setTxDate(new Date());
					txHistory.setTxstatus("Success");
				}
				if (txHistory.getTxstatus().equalsIgnoreCase("Fail")) {
					txHistory.setTxDate(new Date());
					txHistory.setTxstatus("Fail");
				}

				transactionHistoryService.update(txHistory);

			});
			return true;
			// return false;

		}
	}

	public String getWalletAddress(String walletAddress) {
		String[] fetchAddress = walletAddress.split("--");
		String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];
		String fromWalletAddress = "0x" + getAddress;

		return fromWalletAddress;
	}

	@SuppressWarnings("unused")
	public TokenDTO listTransactions(TransactionHistory transactionhistory) {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#.####");
		TokenDTO transactions = new TokenDTO();
		try {
			transactions.setFromAddress(EncryptDecrypt.decrypt(transactionhistory.getFromAddress()));
			transactions.setToAddress(EncryptDecrypt.decrypt(transactionhistory.getToAddress()));
			transactions.setAmount(transactionhistory.getCryptoAmount());
			transactions.setRequestToken(transactionhistory.getTransferToken());
			transactions.setSelectTransactionType(transactionhistory.getPaymentMode());
			transactions.setCreateDate(transactionhistory.getTxDate());
			transactions.setTxStatus(transactionhistory.getTxstatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return transactions;
	}

	public boolean tokenAmountValidation(TokenDTO tokenDTO) throws Exception {
		// System.out.println("Tranfer Amount Before: " +
		// tokenDTO.getBurnToken());
		//
		// // BigInteger tokenBalance = tokenService.tokenBalance(tokenDTO);
		// Token_info tokenInfo = tokenService.findById(1);
		// Double tokenBalance = tokenInfo.getAvailableTokens().doubleValue();
		// if (tokenDTO.getBurnToken() != null &&
		// tokenDTO.getBurnToken().toString().trim() != "") {
		// // int transferAmount = tokenDTO.getBurnToken().intValue();
		// if (tokenDTO.getBurnToken().compareTo(0.0) > 0
		// && (tokenBalance.compareTo(tokenDTO.getBurnToken()) >= 0)) {
		// // System.out.println("Inside Token Amount Validation");
		// return true;
		// }
		// }
		return false;
	}

	public boolean isValidateEthAddress(TokenDTO tokenDTO) {
		boolean status = false;
		Pattern pattern = Pattern.compile("^0x.{40}$");
		Matcher matcher = pattern.matcher(tokenDTO.getToAddress());
		if (matcher.matches()) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean logoutParam(UserRegisterDTO userRegisterDTO) {
		if (userRegisterDTO.getSessionId() != null) {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			String email = (String) session.getAttribute("emailId");
			RegisterInfo regiInfo = userregisterservice.isAccountExistCheckByEmailId(email);
			if (regiInfo != null) {
				HttpSessionEvent event = new HttpSessionEvent(session);
				sessionCollector.sessionDestroyed(event);
				session.invalidate();
				System.out.println("Invalidated");
				return true;
			}
			return false;
		}
		return false;
	}

	public boolean isPasswordexist(TokenDTO tokenDTO) throws Exception {
		// TODO Auto-generated method stub

		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");
		RegisterInfo regiInfo = userregisterservice.isAccountExistCheckByEmailId(email);
		if (regiInfo != null) {
			String ethPassword = EncryptDecrypt.decrypt(regiInfo.getEthWalletPassword());
			if (ethPassword.equals(tokenDTO.getEthWalletPassword())) {
				return true;
			}

			return false;
		}

		return false;
	}

	public UserRegisterDTO userList(RegisterInfo userList) throws Exception {
		// TODO Auto-generated method stub
		UserRegisterDTO registelist = new UserRegisterDTO();
		registelist.setFirstName(userList.getFirstName());
		registelist.setLastName(userList.getLastName());
		registelist.setEmailId(userList.getEmailId());
		registelist.setMobileno(userList.getMobileno());
		// String ethaddress =
		// bioCoinUtils.getWalletAddress(EncryptDecrypt.decrypt(userList.getEthWalletAddress()));
		// registelist.setEathwalletAddress(ethaddress);
		registelist.setGmailstatus(userList.getGmailstatus());

		return registelist;
	}

	public boolean validateRequestCoinParam(TokenDTO tokenDTO) {
		boolean status = false;

		if (tokenDTO.getToAddress() != null && StringUtils.isNotBlank(tokenDTO.getToAddress())
				&& tokenDTO.getPaymentMode() != null && StringUtils.isNotBlank(tokenDTO.getPaymentMode())
				&& tokenDTO.getRequestTokens() != null
				&& StringUtils.isNotBlank(tokenDTO.getRequestTokens().toString())) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean validateSendCoinParam(TokenDTO tokenDTO) {
		if (tokenDTO.getRequestTokens() != null && !StringUtils.isEmpty(tokenDTO.getRequestTokens().toString())
				&& tokenDTO.getToAddress() != null && !StringUtils.isEmpty(tokenDTO.getToAddress())
				&& tokenDTO.getSessionId() != null && !StringUtils.isEmpty(tokenDTO.getSessionId())
				&& tokenDTO.getPaymentMode() != null && !StringUtils.isEmpty(tokenDTO.getPaymentMode())
				&& tokenDTO.getEthWalletPassword() != null && !StringUtils.isEmpty(tokenDTO.getEthWalletPassword())) {
			return true;

		}
		return false;
	}

	public boolean isSameaddress(TokenDTO tokenDTO) {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());

		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);

		try {
			String decryptedEtherWalletAddress = EncryptDecrypt.decrypt(registerInfo.getEthWalletAddress());

			String[] fetchAddress = decryptedEtherWalletAddress.split("--");
			String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];

			String etherWalletAddress = "0x" + getAddress;
			LOG.info("User Ether wallet address:" + tokenDTO.getToAddress());
			LOG.info("User Ether wallet address2:" + etherWalletAddress);

			if (etherWalletAddress.equalsIgnoreCase(tokenDTO.getToAddress())) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean isSamePassword(TokenDTO tokenDTO) throws Exception {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
		LOG.info("EtherWalletPassword : " + EncryptDecrypt.decrypt(registerInfo.getEthWalletPassword()));
		if (EncryptDecrypt.decrypt(registerInfo.getEthWalletPassword()).equals(tokenDTO.getEthWalletPassword())) {
			return true;
		} else {
			return false;
		}
	}


public String balanceCheckForCryptoInPurchaseCoins(TokenDTO tokenDTO) throws NumberFormatException, Exception {

		if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("eth.payment"))) {
			/* Getting Value of the 1 USD rate of Ether */
			Token_info token_info = tokensInfoRepo.findOne(1);
			
			Double currentTokenRateInUSD = Double.parseDouble(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
			System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

			Double etherAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
			System.out.println("Current Rate of 1 USD to Ether : " + etherAmount);

			Double etherValueForOneToken = etherAmount * currentTokenRateInUSD;
			System.out.println("Rate of 1 Tokens in Ether : " + etherValueForOneToken);

			Double etherValue = tokenDTO.getTokenAmount() * etherValueForOneToken;

			System.out.println("current Ether Value : " + etherValue);
			BigDecimal eth = new BigDecimal(etherValue);

			System.out.println(eth);
			DecimalFormat df = new DecimalFormat("#.########");
			BigDecimal etherForTokenRate = new BigDecimal(df.format(eth));
			Double etherBalance = etherBalanceRetrieve(tokenDTO);
			if (etherBalance != 0.0) {
				if (etherBalance > etherForTokenRate.doubleValue()) {
					return "You are having sufficient balance";
				}
				return "Insufficient Ether Balance. You need " + etherForTokenRate + " ETH to buy tokens.";
			}
			return "Ether Balance are too low";

		} else {
			if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("btc.payment"))) {
				/* Getting Value of the 1 USD rate of Bitcoin */
				Token_info token_info = tokensInfoRepo.findOne(1);

				/*RegisterInfo userModelInfo = userRegisterRepository
						.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());*/

				Double currentTokenRateInUSD = Double.parseDouble(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
				System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

				Double btcAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
				System.out.println("Current Rate of 1 USD to bitcoin : " + btcAmount);

				Double btcValueForOneToken = btcAmount * currentTokenRateInUSD;
				System.out.println("Rate of 1 Tokens in btc : " + btcValueForOneToken);

				Double bitcoinValue = tokenDTO.getTokenAmount() * btcValueForOneToken;

				System.out.println("current bitcoinValue Value : " + bitcoinValue);
				BigDecimal btc = new BigDecimal(bitcoinValue);

				System.out.println(btc);
				DecimalFormat df = new DecimalFormat("#.########");
				BigDecimal bitcoin = new BigDecimal(df.format(btc));

				BigDecimal amt = bitCoinUtils.getBitcoinBalance(tokenDTO);
				if (amt.doubleValue() != 0.0) {

					if (amt.doubleValue() > bitcoin.doubleValue()) {
						return "You are having sufficient balance";
					}
					return "Insufficient Bitcoin Balance. You need " + bitcoin + " BTC to buy tokens.";
				}
				return "Bitcoin Balance are too low";
			}
		}
		return null;

	}

	
	public boolean etherValidation(TokenDTO tokenDTO) throws Exception {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		UserRegisterDTO registerDTO = new UserRegisterDTO();
		registerDTO.setSessionId(tokenDTO.getSessionId());
		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");
		String mail = (String) session.getAttribute("emailId");
		LOG.info("Email : " + mail);
		RegisterInfo register = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);

		String walletAddress;
		if (register != null) {
			String decryptWalletAddress = EncryptDecrypt.decrypt(register.getEthWalletAddress());
			walletAddress = getEtherWalletAddress(configInfo.getConfigValue(), decryptWalletAddress);
			if (walletAddress == null) {
				return false;
			}
			LOG.info("Ether Address : " + walletAddress);
			EthGetBalance ethGetBalance;
			ethGetBalance = web3j.ethGetBalance(walletAddress, DefaultBlockParameterName.LATEST).sendAsync().get();
			BigInteger wei = ethGetBalance.getBalance();
			LOG.info("ether bal:::::::::::" + wei);
			BigDecimal amountCheck = Convert.fromWei(wei.toString(), Convert.Unit.ETHER);
			LOG.info("ether bal:::::::::::" + amountCheck);

			LOG.info("balance :: Ether :: " + amountCheck);

			if (amountCheck.doubleValue() >= 0.01) {
				return true;
			}
			return false;
		}
		return false;
	}

	public String getEtherWalletAddress(String fileLocation, String fileName)
			throws FileNotFoundException, IOException, ParseException {

		fileLocation = fileLocation.replace("/", "\\");
		LOG.info("WalletCreated" + fileLocation);
		LOG.info("FileName:::" + fileName);

		JSONParser parser = new JSONParser();
		Object object;
		object = parser.parse(new FileReader(fileLocation + "//" + fileName));
		JSONObject jsonObject = (JSONObject) object;
		String address = "0x" + (String) jsonObject.get("address");
		LOG.info("FileName" + fileName);
		LOG.info("Wallet Address" + address);

		return address;
	}

	public String tokenAmountValidationForUser(TokenDTO tokenDTO) throws Exception {

		Double balance = solidityHandler.balanceTokens(tokenDTO);

		LOG.info("Amount Balance " + balance);
		if (tokenDTO.getSendToken() != null && tokenDTO.getSendToken() != 0 && tokenDTO.getSendToken().toString().trim() != "") {

			Double transferAmount = tokenDTO.getSendToken();

			LOG.info("Amount to Transfer :" + transferAmount);

			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			String mail = (String) session.getAttribute("emailId");
			RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);

			Double balanceTokens = balance.doubleValue() - registerInfo.getLockedTokenBalance();
			System.out.println("balanceTokens : " + balanceTokens);
			if (transferAmount.intValue() > 0 && balance.doubleValue() >= transferAmount) {
				LOG.info("Inside Token Amount Validation");
				if (balanceTokens >= transferAmount) {
					return env.getProperty("token.balance");
				}
				return env.getProperty("token.balance.exceed") + "Your Token Balance is : " + balanceTokens;
			}
			return "Your Token Balance " + balance + " are lower than the entered amount of Tokens";
		}
		return env.getProperty("token.balance.valid");
	}

	// public boolean tokenAmountValidationForUserForVesting(TokenDTO tokenDTO)
	// {
	// HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
	// String mail = (String) session.getAttribute("emailId");
	// RegisterInfo registerInfo =
	// userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(mail);
	// if(registerInfo.getLockedTokenBalance() > tokenDTO.getSendToken()) {
	// return true;
	// }
	// return false;
	// }

	public String tokenAmountValidationForAdmin(TokenDTO tokenDTO) throws Exception {

		Double transferAmount = tokenDTO.getSendToken();
		LOG.info("Amount to Transfer :" + transferAmount);
		Token_info token_info = tokenService.findById(1);
		System.out.println("Balance manual available amount : " + token_info.getManualAvailableTokens());
		if (token_info.getManualAvailableTokens() >= transferAmount) {
			return env.getProperty("token.balance");
		}
		return env.getProperty("token.balance.valid") + ". You are having " + token_info.getManualAvailableTokens() + " tokens to transfer Manually";
	}

	public String tokenAmountValidationForBurn(TokenDTO tokenDTO) throws Exception {

		Double transferAmount = tokenDTO.getBurnToken();
		LOG.info("Amount to Transfer :" + transferAmount);
		Token_info token_info = tokenService.findById(1);
		System.out.println("Balance manual available amount : " + token_info.getManualAvailableTokens());
		if (token_info.getManualAvailableTokens() >= transferAmount) {
			return env.getProperty("token.balance");
		}
		return env.getProperty("token.balance.valid");
	}

	public List<UserRegisterDTO> usersListFilter(UserRegisterDTO userRegisterDTO) throws Exception {

		List<UserRegisterDTO> filter = new ArrayList<UserRegisterDTO>();
		if (userRegisterDTO.getSearchText() != null) {
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "id"));
			Pageable pageable = new PageRequest(userRegisterDTO.getPageNum(), userRegisterDTO.getPageSize(), sort);
			Page<RegisterInfo> registerInfo = userRegisterRepository.findBynames(userRegisterDTO.getSearchText(),
					userRegisterDTO.getSearchText(), userRegisterDTO.getSearchText(), pageable);
			userRegisterDTO.setTotalPages(registerInfo.getTotalPages());
			System.out.println("transactionHistory.getTotalPages() : " + registerInfo.getTotalPages()
					+ "TotalElements : " + registerInfo.getTotalElements());
			userRegisterDTO.setTotalElements(Math.toIntExact(registerInfo.getTotalElements()));

			for (RegisterInfo reg : registerInfo) {
				UserRegisterDTO transaction = userList(reg);
				filter.add(transaction);
			}
		}
		return filter;
	}

	public String purchaseTokenEtherValidation(TokenDTO tokenDTO) throws NumberFormatException, Exception {

		/* Getting Value of the 1 USD rate of Ether */
		Token_info token_info = tokensInfoRepo.findOne(1);

		Double currentTokenRateInUSD = Double.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
		System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

		Double etherAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
		System.out.println("Current Rate of 1 USD to Ether : " + etherAmount);

		Double etherValueForOneToken = etherAmount * currentTokenRateInUSD;
		System.out.println("Rate of 1 Tokens in Ether : " + etherValueForOneToken);

		Double etherValue = tokenDTO.getTokenAmount() * etherValueForOneToken;

		System.out.println("current Ether Value : " + etherValue);
		BigDecimal eth = new BigDecimal(etherValue);

		System.out.println(eth);
		DecimalFormat df = new DecimalFormat("#.########");
		BigDecimal etherForTokenRate = new BigDecimal(df.format(eth));
		Double etherBalance = etherBalanceRetrieve(tokenDTO);
		if (etherBalance != 0.0) {
			if (etherBalance > etherForTokenRate.doubleValue()) {
				return "You are having sufficient ETH";
			}
			return "Insufficient Ether Balance. You need " + etherForTokenRate + " ETH to buy tokens.";
		}
		return "Not able to retrieve ether Balance";
	}

	public Double etherBalanceRetrieve(TokenDTO tokenDTO) throws Exception {
		HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
		String mail = (String) session.getAttribute("emailId");
		RegisterInfo regInfo = userregisterservice.isAccountExistCheckByEmailId(mail);
		if (regInfo != null) {
			String etherwalletaddress = EncryptDecrypt.decrypt(regInfo.getEthWalletAddress());
			String[] fetchAddress = etherwalletaddress.split("--");
			String getAddress = fetchAddress[fetchAddress.length - 1].split("\\.")[0];
			EthGetBalance ethgetbalance;
			ethgetbalance = web3j.ethGetBalance("0x" + getAddress, DefaultBlockParameterName.LATEST).sendAsync().get();
			BigInteger wei = ethgetbalance.getBalance();
			String s = wei.toString();
			BigDecimal etherbalance = Convert.fromWei(s, Convert.Unit.ETHER);
			return etherbalance.doubleValue();
		}
		return 0.0;
	}

	public String checkShecdule() {

		/* Create formatter */
		SimpleDateFormat sql = new SimpleDateFormat("MM-dd-yyyy");
		Date date = new Date();
		String currentDate = sql.format(date);
		System.out.println("Current Date : " + currentDate);
		List<ICOPeriodInfo> icoPeriodInfo = (List<ICOPeriodInfo>) icoPeriodInfoRepo.findAll();
		if (icoPeriodInfo != null) {

			Double tokens = 0.0;
			for (ICOPeriodInfo icoDate : icoPeriodInfo) {

				String icoEndDate = sql.format(icoDate.getIcoEnd());
				Date add = DateUtils.addDays(new Date(), -1);
				String dates = sql.format(add);
				System.out.println("Added : " + add + "current Date" + dates);
				if (icoEndDate.equals(dates)) {
					icoDate.setIcoStatus(0);
					tokens = tokens + icoDate.getIcoAvailableTokens();
					icoDate.setIcoAvailableTokens(0.0);
					icoPeriodInfoRepo.save(icoDate);
				}

				String icoStart = sql.format(icoDate.getIcoStart());
				if (icoStart.equals(currentDate)) {
					icoDate.setIcoStatus(1);
					icoDate.setIcoAvailableTokens(tokens + icoDate.getIcoAvailableTokens());
					icoDate.setIcoAllocationTokens(tokens + icoDate.getIcoAllocationTokens());
					icoPeriodInfoRepo.save(icoDate);
				}
			}
			return "Changes success";
		}
		return "Changes failed";
	}

	public String currentRateForPurchaseToken(TokenDTO tokenDTO) throws Exception {
		Token_info token_info = tokensInfoRepo.findOne(1);
		
		if (tokenDTO.getTokenAmount() != null) {
			if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase("ETH")) {
				/* Getting Value of the 1 USD rate of Ether */
				
				Double currentTokenRateInUSD = Double.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
				System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

				Double etherAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
				System.out.println("Current Rate of 1 USD to Ether : " + etherAmount);

				Double etherValueForOneToken = etherAmount * currentTokenRateInUSD;
				System.out.println("Rate of 1 Tokens in Ether : " + etherValueForOneToken);

				Double etherValue = tokenDTO.getTokenAmount() * etherValueForOneToken;

				System.out.println("current Ether Value : " + etherValue);
				BigDecimal eth = new BigDecimal(etherValue);

				System.out.println(eth);
				DecimalFormat df = new DecimalFormat("#.########");
				BigDecimal ether = new BigDecimal(df.format(eth));

				System.out.println(ether);
				String eth1 = ether.toPlainString();
				tokenDTO.setCurrentTokenRate(eth1);
			} else if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase("BTC")) {

				/* Getting Value of the 1 USD rate of BTC */

				Double currentTokenRateInUSD = Double.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
				System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

				Double btcAmount = currentValueUtils.getCryptoRateFromDollar(tokenDTO);
				System.out.println("Current Rate of 1 USD to Ether : " + btcAmount);

				Double btcValueForOneToken = btcAmount * currentTokenRateInUSD;
				System.out.println("Rate of 1 Tokens in Ether : " + btcValueForOneToken);

				Double btcValue = tokenDTO.getTokenAmount() * btcValueForOneToken;

				System.out.println("current Ether Value : " + btcValue);
				BigDecimal btc = new BigDecimal(btcValue);

				System.out.println(btc);
				DecimalFormat df = new DecimalFormat("#.########");
				BigDecimal bitcoin = new BigDecimal(df.format(btc));

				System.out.println(bitcoin);
				String btc1 = bitcoin.toPlainString();
				tokenDTO.setCurrentTokenRate(btc1);
				
			} else if(tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("paypal.payment"))) {
				String value = " ";
				if(tokenDTO.getCurrency().equalsIgnoreCase(env.getProperty("paypal.payment.usd"))) {
					
					Double currentTokenRateInUSD = Double.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
					System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);
					
					DecimalFormat df = new DecimalFormat("#.##");

					Double v = Double.parseDouble(df.format(tokenDTO.getTokenAmount() * currentTokenRateInUSD));
					
					value = v.toString();
					
				} else if(tokenDTO.getCurrency().equalsIgnoreCase(env.getProperty("paypal.payment.inr"))) {
					
					Double currentTokenRateInUSD = Double.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
					System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);
					
					Double rateOfOneUSDInINR = currentValueUtils.getCurrencyRateInINR();
					System.out.println( "Rate Of One USD In INR : " + rateOfOneUSDInINR);
					
					Double multiplyingWithUSDOfTokenRate = rateOfOneUSDInINR * currentTokenRateInUSD;
					DecimalFormat df = new DecimalFormat("#.##");
					Double inr = Double.parseDouble(df.format(multiplyingWithUSDOfTokenRate * tokenDTO.getTokenAmount())); 
					value = inr.toString();
				}
				tokenDTO.setCurrentTokenRate(value);
				
			}
			return "Rate of Token Retrived successfully";
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public JSONObject createPayments(PaypalDTO paypalDTO) throws IOException, ParseException {

		URL url = new URL(env.getProperty("create.payment.url"));
		HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");
		urlConnection.setRequestProperty("Accept-Language", "en_US");
		urlConnection.setRequestProperty("Authorization", "Bearer " + paypalDTO.getAccessToken());
		JSONObject json = new JSONObject();
		json.put("intent", "sale");

		JSONObject objp = new JSONObject();
		objp.put("return_url", "http://localhost:3000/login");
		objp.put("cancel_url", "http://localhost:3000/logout");
		json.put("redirect_urls", objp);

		JSONObject objq = new JSONObject();
		objq.put("payment_method", "paypal");
		json.put("payer", objq);

		JSONArray array2 = new JSONArray();
		JSONObject objr = new JSONObject();
		objr.put("total", paypalDTO.getTotal());
		objr.put("currency", paypalDTO.getCurrency());

		JSONObject objs = new JSONObject();
		objs.put("amount", objr);
		objs.put("description", "Purchasing Biokkoin Tokens");
		array2.put(objs);
		json.put("transactions", array2);

		System.out.println("Json : " + json.toString());

		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		wr.write(json.toString());
		wr.flush();

		StringBuilder sb = new StringBuilder();
		int HttpsResult = urlConnection.getResponseCode();
		System.out.println(HttpsResult);
		if (HttpsResult == HttpsURLConnection.HTTP_CREATED) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Response" + s);

			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(s);

			System.out.println("jsonObj" + jsonObj.toString());

			return jsonObj;

		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public String redirectURL(PaypalDTO paypalDTO) throws Exception {

		System.out.println("payerId : " + paypalDTO.getPayer_id() + " : " + paypalDTO.getPaymentID());

		URL url = new URL(env.getProperty("execute.url") + paypalDTO.getPaymentID() + env.getProperty("execute.nxt.url"));
		HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setRequestMethod("POST");
		urlConnection.setRequestProperty("Content-Type", "application/json");
		urlConnection.setRequestProperty("Accept", "application/json");
		urlConnection.setRequestProperty("Accept-Language", "en_US");
		urlConnection.setRequestProperty("Authorization",
				"Bearer " + paypalDTO.getAccessToken());
		JSONObject json = new JSONObject();
		json.put("payer_id", paypalDTO.getPayer_id());

		System.out.println("Json : " + json.toString());

		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		wr.write(json.toString());
		wr.flush();

		StringBuilder sb = new StringBuilder();
		int HttpsResult = urlConnection.getResponseCode();
		System.out.println(HttpsResult);
		if (HttpsResult == HttpsURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
			}
			br.close();
			String s = new String(sb);
			System.out.println("Json Response" + s);

			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(s);

			System.out.println("jsonObj" + jsonObj.toString());

			PaymentsInfo paymentsInfo = paymentInfoRepo.findPaymentsInfoByPaymentID(paypalDTO.getPaymentID());
			paymentsInfo.setPayer_id(paypalDTO.getPayer_id());
			paymentsInfo.setStatus("Success");
			
			String tokenTransfer = tokenTransfer(paypalDTO);
			if(tokenTransfer != "0x0") {
				return "Payment Executed Successfully";
			}	
		} else {
			PaymentsInfo paymentsInfo = paymentInfoRepo.findPaymentsInfoByPaymentID(paypalDTO.getPaymentID());
			paymentsInfo.setPayer_id(paypalDTO.getPayer_id());
			paymentsInfo.setStatus("Failed");
			return "Payment Failed";
		}
		return null;
	}
	
	public String tokenTransfer(PaypalDTO paypalDTO) throws Exception {
		TransactionReceipt transactionReceipt = new TransactionReceipt();
		PaymentsInfo paymentsInfo = paymentInfoRepo.findPaymentsInfoByPaymentID(paypalDTO.getPaymentID());
		
		RegisterInfo registerInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(paymentsInfo.getEmailId());
		
		TransactionHistory transactionHistory = new TransactionHistory();
		transactionHistory.setFromAddress(env.getProperty("main.address"));
		transactionHistory.setToAddress(EncryptDecrypt.decrypt(paymentsInfo.getEthAddress()));
		transactionHistory.setCryptoAmount(new BigDecimal(Double.valueOf(paymentsInfo.getAmount())));
		transactionHistory.setTransferToken(paymentsInfo.getTokenAmount());
		transactionHistory.setTxDate(new Date());
		transactionHistory.setPaymentMode("Purchase");
		transactionHistory.setEmailId(paymentsInfo.getEmailId());
		transactionHistory.setSenderId(1);
		transactionHistory.setReceiverId(registerInfo.getId());
		transactionHistory.setTxstatus("Pending");
		transactionHistory.setRegisterInfo(registerInfo);
		transactionHistory.setTypeOfVerifications(registerInfo.getTypeOfVerifications());
		transactionInfoRepository.save(transactionHistory);
		if (transactionHistory != null) {
			Credentials credentials = WalletUtils.loadCredentials(env.getProperty("credentials.password"),
					env.getProperty("credentials.address"));
			Biokkoin assetToken = Biokkoin.load(this.env.getProperty("token.address"), web3j, credentials,
					Contract.GAS_PRICE, Contract.GAS_LIMIT);
			if (assetToken != null) {
				LOG.info("Token Getting Amount" + paymentsInfo.getTokenAmount());
				BigInteger value = BigDecimal.valueOf(paymentsInfo.getTokenAmount() * 100000000).toBigInteger();
				transactionReceipt = assetToken
						.transfer(EncryptDecrypt.decrypt(paymentsInfo.getEthAddress()), value).send();

				LOG.info("transactionReceipt : " + transactionReceipt.getTransactionHash() + " : "
						+ transactionReceipt.getGasUsed() + " : " + transactionReceipt.getStatus() + " : "
						+ transactionReceipt.getCumulativeGasUsed());
			}
		}
		PurchaseTokenInfo purchaseTokenInfo = purchaseTokenInfoRepository.findPurchaseTokenInfoByPaymentId(paypalDTO.getPaymentID());
		if (transactionReceipt.getStatus().equalsIgnoreCase("0x1")) {
			System.out.println("Inside Validations for Purchase Tokens after success");
			
			transactionHistory.setTxstatus("Success");
			transactionHistoryService.save(transactionHistory);

			Token_info token_info = tokensInfoRepo.findOne(1);
			token_info.setIcoSoldTokens(token_info.getIcoSoldTokens() + paymentsInfo.getTokenAmount());
			token_info.setTotalSoldTokens(token_info.getTotalSoldTokens() + paymentsInfo.getTokenAmount());

			token_info.setIcoAvailableTokens(token_info.getIcoAvailableTokens() - paymentsInfo.getTokenAmount());
			token_info
					.setTotalAvailableTokens(token_info.getTotalAvailableTokens() - paymentsInfo.getTokenAmount());
			tokensInfoRepo.save(token_info);

			ICOPeriodInfo icoPeriodInfo = icoPeriodInfoRepo.findICOPeriodInfoByicoStatus(1);

			icoPeriodInfo
					.setIcoAvailableTokens(icoPeriodInfo.getIcoAvailableTokens() - paymentsInfo.getTokenAmount());
			icoPeriodInfo.setIcoSoldTokens(icoPeriodInfo.getIcoSoldTokens() + paymentsInfo.getTokenAmount());
			icoPeriodInfoRepo.save(icoPeriodInfo);

			purchaseTokenInfo.setAsynchStatus("Success");
			purchaseTokenInfoRepository.save(purchaseTokenInfo);
		} else {
			transactionHistory.setTxstatus("Failed");
			transactionHistoryService.save(transactionHistory);

			purchaseTokenInfo.setAsynchStatus("Failed");
			purchaseTokenInfoRepository.save(purchaseTokenInfo);
		}
		return transactionReceipt.toString();
	}

	public String redirectURLForCancelPayments(PaypalDTO paypalDTO) {
		
		PaymentsInfo paymentsInfo = paymentInfoRepo.findPaymentsInfoByPaymentID(paypalDTO.getPaymentID());
		if(paymentsInfo != null) {
			if(paypalDTO.getCancelPayment().equalsIgnoreCase("cancel")) {
				paymentsInfo.setStatus("Payment cancelled");
				paymentInfoRepo.save(paymentsInfo);
				return "Payement cancelled successfully";
			} else {
				return null;
			}
			
		}
		
		return "Payment cancellation failed";
	}

	public String validateInputsForPurchase(TokenDTO tokenDTO) {
		if(tokenDTO.getTokenAmount() != null && tokenDTO.getTokenAmount() != 0) {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			String mail = (String) session.getAttribute("emailId");
			KycInfo kycInfo = kycInfoRepo.findByEmailId(mail);
			if(kycInfo != null) {
				if(kycInfo.getKycStatus() != 0 && kycInfo.getKycStatus() != 2 && kycInfo.getKycStatus() != 4) {
					return "Validation success";
				} else { 
					return "You are not allowed to purchase tokens till the admin approve you.";
				}
			} else { 
				return "You are not allowed to purchase tokens till the admin approve you.";
			}
		}
		return env.getProperty("valid.tokens");
	}
	
	public String validateInputsForPurchase(PaypalDTO paypalDTO) {
		if(paypalDTO.getTokenAmount() != null && paypalDTO.getTokenAmount() != 0) {
			HttpSession session = SessionCollector.find(paypalDTO.getSessionId());
			String mail = (String) session.getAttribute("emailId");
			KycInfo kycInfo = kycInfoRepo.findByEmailId(mail);
			if(kycInfo != null) {
				if(kycInfo.getKycStatus() != 0 && kycInfo.getKycStatus() != 2 && kycInfo.getKycStatus() != 4) {
					return "Validation success";
				} else { 
					return "You are not allowed to purchase tokens till the admin approve you.";
				}
			} else { 
				return "You are not allowed to purchase tokens till the admin approve you.";
			}
		}
		return env.getProperty("valid.tokens");
	}
	
	public String validateTheAmountOfUSDAndINR(PaypalDTO paypalDTO) throws Exception {
		
		
		if (paypalDTO.getCurrency().equalsIgnoreCase(env.getProperty("paypal.payment.usd"))) {

			/* Getting Value of the 1 USD rate of Ether */
			Token_info token_info = tokensInfoRepo.findOne(1);

			Double currentTokenRateInUSD = Double
					.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
			System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

			Double rateOfTokensInUSD = currentTokenRateInUSD * paypalDTO.getTokenAmount();

//			DecimalFormat df = new DecimalFormat("#.###");

			Integer value =rateOfTokensInUSD.intValue();
			System.out.println("value : " + value);
			if(value > 2) {
				return null;
			} else {
				return "Please purchase more than the Amount of 2 USD.";
			}
		} else if (paypalDTO.getCurrency().equalsIgnoreCase(env.getProperty("paypal.payment.inr"))) {

			/* Getting Value of the 1 USD rate of Ether */
			Token_info token_info = tokensInfoRepo.findOne(1);

			Double currentTokenRateInUSD = Double
					.valueOf(EncryptDecrypt.decrypt(token_info.getTokenValuesInUSD()));
			System.out.println("currentTokenRateInUSD : " + currentTokenRateInUSD);

			// INR conversion

			Double rateOfOneUSDInINR = currentValueUtils.getCurrencyRateInINR();
			System.out.println("Rate Of One USD In INR : " + rateOfOneUSDInINR);

			Double multiplyingWithUSDOfTokenRate = rateOfOneUSDInINR * currentTokenRateInUSD;

			Double rateOfTokensInINR = multiplyingWithUSDOfTokenRate * paypalDTO.getTokenAmount();

//			DecimalFormat df = new DecimalFormat("#.###");

			Integer value = rateOfTokensInINR.intValue();
			if(value > 2) {
				return null;
			} else {
				return "Please purchase more than the Amount of 2 INR.";
			}
		}
		return null;
	}
		
}
