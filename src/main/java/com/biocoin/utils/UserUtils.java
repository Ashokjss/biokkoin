package com.biocoin.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

import com.biocoin.dto.LoginDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.model.Config;
import com.biocoin.model.RegisterInfo;
import com.biocoin.model.TransactionHistory;
import com.biocoin.model.UserRequestCoinInfo;
import com.biocoin.repo.ConfigInfoRepository;
import com.biocoin.repo.TransactionInfoRepository;
import com.biocoin.repo.UserRegisterRepository;
import com.biocoin.repo.UserRequestCoinRepository;
import com.biocoin.service.TokenService;
import com.biocoin.solidityToJava.Biokkoin;
import javax.servlet.http.HttpSession;
import org.springframework.web.multipart.MultipartFile;
import com.biocoin.dto.KycDTO;
import com.biocoin.model.KycInfo;

@Service
public class UserUtils {

	@Autowired
	private Environment env;
	@Autowired
	private ConfigInfoRepository configInfoRepository;
	@Autowired
	private UserRegisterRepository userRegisterRepository;
	@Autowired
	private BioCoinUtils bioCoinUtils;
	@Autowired
	private UserRequestCoinRepository userRequestCoinRepository;
	@Autowired
	private TransactionInfoRepository transactionInfoRepository;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private CurrentValueUtils currentValueUtils;
//	@Autowired
//	private TokenAllocationForTopManagementsInfoRepo tokenAllocationForTopManagementsInfoRepo;

	private TransactionReceipt transactionReceipt;

	private BigInteger gasPrice = BigInteger.valueOf(3000000);

	private BigInteger gasLimit = BigInteger.valueOf(3000000);

	private final Web3j web3j = Web3j.build(new HttpService("https://rinkeby.infura.io/"));

	// private final Web3j web3j = Web3j.build(new HttpService());

	private static final Logger LOG = LoggerFactory.getLogger(UserUtils.class);

	public boolean validateTokenVal(TokenDTO tokenDTO) {
		boolean status = false;
		if (new BigDecimal(tokenDTO.getRequestTokens()).compareTo(new BigDecimal(0)) == 1) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean isTransferAmtZeroUser(TokenDTO tokenDTO) {
		boolean status = false;
		int length = String.valueOf(tokenDTO.getRequestTokens()).length();
		if (tokenDTO.getRequestTokens() > 0 && length <= 10) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean isFromAndToAddressSame(TokenDTO tokenDTO) {
		boolean status = false;
		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		String decryptedDummyWalletAddress = null;
		try {
			decryptedDummyWalletAddress = EncryptDecrypt.decrypt(userInfoModel.getDummywalletaddress());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!tokenDTO.getToAddress().equalsIgnoreCase(decryptedDummyWalletAddress)) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean saveRequestInfo(TokenDTO tokenDTO) throws Exception {
		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");
		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		RegisterInfo userInfoModel1 = userRegisterRepository
				.findRegisterInfoByDummywalletaddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));

		if (tokenDTO.getPaymentMode().equals(env.getProperty("eth.payment"))
				|| tokenDTO.getPaymentMode().equals(env.getProperty("bio.payment"))) {

			if (userInfoModel != null && userInfoModel1 != null) {
				String walletAddress = EncryptDecrypt.decrypt(userInfoModel.getEthWalletAddress());
				String walletAddress1 = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(), walletAddress);
				UserRequestCoinInfo userRequestCoinInfo = new UserRequestCoinInfo();
				userRequestCoinInfo.setFromAddress(EncryptDecrypt.encrypt(walletAddress1));
				userRequestCoinInfo.setToAddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));
				userRequestCoinInfo.setPaymentMode(tokenDTO.getPaymentMode());
				userRequestCoinInfo.setRequestCoin(tokenDTO.getRequestTokens());
				userRequestCoinInfo.setTranscationType(0);
				userRequestCoinInfo.setCreatedDate(new Date());
				userRequestCoinInfo.setSenderName(userInfoModel.getFirstName());
				userRequestCoinInfo.setReceiveraName(userInfoModel1.getFirstName());
				userRequestCoinInfo.setTypeOfVerifications(userInfoModel.getTypeOfVerifications());
				userRequestCoinRepository.save(userRequestCoinInfo);
				tokenDTO.setId(userRequestCoinInfo.getId());
				tokenDTO.setRequestTokens(userRequestCoinInfo.getRequestCoin());
				return true;
			}
		}
		return false;
	}

	public List<TokenDTO> getRequestCoinCount(TokenDTO tokenDTO) throws Exception {
		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		String decryptedDummyWalletAddress = EncryptDecrypt.decrypt(userInfoModel.getDummywalletaddress());
		tokenDTO.setToAddress(decryptedDummyWalletAddress);
		List<TokenDTO> requestCoinList = new ArrayList<TokenDTO>();
		List<UserRequestCoinInfo> requestCoinHistory = userRequestCoinRepository
				.findByToAddressOrderByCreatedDateDesc(userInfoModel.getDummywalletaddress());
		for (UserRequestCoinInfo requestCoinInfo : requestCoinHistory) {
			if (requestCoinInfo.getTranscationType() == 0) {
				TokenDTO requestCoin = listRequestCoin(requestCoinInfo);
				requestCoinList.add(requestCoin);
			}
		}
		return requestCoinList;
	}

	public TokenDTO listRequestCoin(UserRequestCoinInfo transaction) throws Exception {

		TokenDTO request = new TokenDTO();
		request.setFromAddress(EncryptDecrypt.decrypt(transaction.getFromAddress()));
		request.setToAddress(EncryptDecrypt.decrypt(transaction.getToAddress()));
		request.setPaymentMode(transaction.getPaymentMode());
		request.setRequestTokens(transaction.getRequestCoin());
		request.setCreateDate(transaction.getCreatedDate());
		request.setId(transaction.getId());
		request.setSender(transaction.getSenderName());
		request.setReceiver(transaction.getReceiveraName());
		request.setTypeOfVerifications(transaction.getTypeOfVerifications());
		return request;
	}

	public boolean validateSendRequestCoinParam(TokenDTO tokenDTO) {
		boolean status = false;
		if (tokenDTO.getId() != null && StringUtils.isNotBlank(tokenDTO.getId().toString())) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean validateId(TokenDTO tokenDTO) {
		boolean status = false;
		UserRequestCoinInfo userRequestCoinInfo = userRequestCoinRepository.findOne(tokenDTO.getId());
		if (userRequestCoinInfo != null) {
			status = true;
			return status;
		}
		return status;
	}

	public boolean validPaymentMode(TokenDTO tokenDTO) {
		boolean status = false;
		UserRequestCoinInfo userRequestCoinInfo = userRequestCoinRepository.findOne(tokenDTO.getId());
		if (userRequestCoinInfo.getPaymentMode() != null) {
			if (userRequestCoinInfo.getPaymentMode().equals(env.getProperty("eth.payment"))
					|| userRequestCoinInfo.getPaymentMode().equals(env.getProperty("bio.payment"))) {
				status = true;
				return status;
			}
		}
		return status;
	}

	public boolean validRequestCoin(TokenDTO tokenDTO) throws Exception {

		UserRequestCoinInfo userRequestCoinInfo = userRequestCoinRepository.findOne(tokenDTO.getId());
		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		String decryptedWalletAddress = EncryptDecrypt.decrypt(userInfoModel.getEthWalletAddress());
		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");
		String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(), decryptedWalletAddress);
		if (userRequestCoinInfo.getPaymentMode().equals(env.getProperty("eth.payment"))) {
			tokenDTO.setEthWalletAddress(etherWalletAddress);
			tokenDTO.setCentralAdmin(tokenDTO.getEthWalletAddress());
			EthGetBalance ethGetBalance;
			ethGetBalance = web3j.ethGetBalance(tokenDTO.getCentralAdmin(), DefaultBlockParameterName.LATEST)
					.sendAsync().get();
			BigInteger wei = ethGetBalance.getBalance();
			BigDecimal amountCheck = Convert.fromWei(wei.toString(), Convert.Unit.ETHER);
			if (tokenDTO.getSendToken().doubleValue() < amountCheck.doubleValue()) {
				return true;
			}
		}
		if (userRequestCoinInfo.getPaymentMode().equals(env.getProperty("bio.payment"))) {

			Credentials credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
					configInfo.getConfigValue() + decryptedWalletAddress);
			Biokkoin assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
					Contract.GAS_PRICE, Contract.GAS_LIMIT);
			BigInteger b = assetToken.balanceOf(etherWalletAddress).send();
			BigDecimal requestcoin = BigDecimal.valueOf(userRequestCoinInfo.getRequestCoin());
			if (b.compareTo(requestcoin.toBigInteger()) == 1) {
				return true;
			}
		}
		return false;

	}

	@SuppressWarnings("unused")
	public boolean isSendAmountUser(TokenDTO tokenDTO) throws Exception {

		UserRequestCoinInfo userRequestCoinInfo = userRequestCoinRepository.findOne(tokenDTO.getId());

		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");
		RegisterInfo userInfoModel1 = userRegisterRepository
				.findRegisterInfoByDummywalletaddress(userRequestCoinInfo.getFromAddress());
		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		String decryptedWalletAddress = EncryptDecrypt.decrypt(userInfoModel.getEthWalletAddress());
		TransactionHistory transactionHistory = new TransactionHistory();
		String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(), decryptedWalletAddress);
		LOG.info("paymentMode" + userRequestCoinInfo.getPaymentMode());
		if (userRequestCoinInfo.getPaymentMode().equals(env.getProperty("eth.payment"))) {

			transactionReceipt = new TransactionReceipt();

			// CompletableFuture.supplyAsync(() -> {
			Credentials credentials;
			try {
				transactionHistory.setFromAddress(EncryptDecrypt.encrypt(etherWalletAddress));
				transactionHistory.setToAddress(userRequestCoinInfo.getFromAddress());
				transactionHistory.setCryptoAmount(new BigDecimal(tokenDTO.getSendToken().doubleValue()));
				transactionHistory.setTransferToken(0.00);
				transactionHistory.setPaymentMode(env.getProperty("eth.payment"));
				transactionHistory.setTxDate(new Date());
				transactionHistory.setSenderId(userInfoModel.getId());
				transactionHistory.setReceiverId(userInfoModel1.getId());
				transactionHistory.setTxstatus(env.getProperty("status.pending"));
				transactionHistory.setRegisterInfo(userInfoModel);
				transactionHistory.setTypeOfVerifications(tokenDTO.getTypeOfVerifications());
				transactionHistory.setEmailId(userInfoModel1.getEmailId());
				transactionInfoRepository.save(transactionHistory);
				if (transactionHistory != null) {
					System.out.println("walAdd=" + decryptedWalletAddress + "  " + "fromAdd="
							+ userRequestCoinInfo.getFromAddress());
					credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
							configInfo.getConfigValue() + decryptedWalletAddress);
					transactionReceipt = Transfer
							.sendFunds(web3j, credentials, EncryptDecrypt.decrypt(userRequestCoinInfo.getFromAddress()),
									new BigDecimal(userRequestCoinInfo.getRequestCoin()), Convert.Unit.ETHER)
							.sendAsync().get();
					transactionReceipt = Transfer
							.sendFunds(web3j, credentials, EncryptDecrypt.decrypt(userRequestCoinInfo.getFromAddress()),
									new BigDecimal(tokenDTO.getSendToken().doubleValue()), Convert.Unit.ETHER)
							.send();
					System.out.println("walAdd1=" + decryptedWalletAddress + "  " + "fromAdd1="
							+ userRequestCoinInfo.getFromAddress());
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			// return "call blackchain";
			// }).thenAccept(product -> {
			// if
			// (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.success")))
			// {
			if (transactionHistory != null) {
				transactionHistory.setTxstatus(env.getProperty("status.success"));
				userRequestCoinInfo.setTranscationType(1);
				userRequestCoinRepository.save(userRequestCoinInfo);
				transactionInfoRepository.save(transactionHistory);
				tokenDTO.setPaymentMode(userRequestCoinInfo.getPaymentMode());
				tokenDTO.setReceiver(userRequestCoinInfo.getReceiveraName());
				tokenDTO.setSender(userRequestCoinInfo.getSenderName());
				tokenDTO.setRequestTokens(tokenDTO.getSendToken().doubleValue());
				tokenDTO.setTransferAmount(transactionHistory.getCryptoAmount());
				tokenService.sendETHCoinPushNotificationFrom(userInfoModel, tokenDTO);
				tokenService.sendETHCoinPushNotificationTo(userInfoModel1, tokenDTO);
				return true;
			}
			// } else if
			// (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.failed")))
			// {
			if (transactionHistory != null) {
				transactionHistory.setTxstatus(env.getProperty("status.failed"));
				transactionInfoRepository.save(transactionHistory);
				return false;
			}
			// }
			// });
			return true;

		}

		if (userRequestCoinInfo.getPaymentMode().equals(env.getProperty("bio.payment"))) {

			transactionReceipt = new TransactionReceipt();

			// CompletableFuture.supplyAsync(() -> {
			Credentials credentials;
			try {
				transactionHistory.setCryptoAmount(new BigDecimal(0.0));
//				transactionHistory.setRequesttoken(tokenDTO.getSendToken());
				transactionHistory.setFromAddress(EncryptDecrypt.encrypt(etherWalletAddress));
				LOG.info("form walletAddress" + etherWalletAddress);
				transactionHistory.setToAddress(userRequestCoinInfo.getFromAddress());
				LOG.info("to walletAddress" + EncryptDecrypt.decrypt(userRequestCoinInfo.getFromAddress()));
				transactionHistory.setCryptoAmount(new BigDecimal("0.0"));
				// BigDecimal.valueOf(userRequestCoinInfo.getRequestCoin()).toBigInteger();
				transactionHistory.setTransferToken(tokenDTO.getSendToken().doubleValue());
				LOG.info("request coin" + userRequestCoinInfo.getRequestCoin());
				transactionHistory.setPaymentMode(env.getProperty("bio.payment"));
				transactionHistory.setTxDate(new Date());
				transactionHistory.setSenderId(userInfoModel.getId());
				transactionHistory.setReceiverId(userInfoModel1.getId());
				transactionHistory.setTxstatus(env.getProperty("status.pending"));
				transactionHistory.setRegisterInfo(userInfoModel);
				transactionHistory.setTypeOfVerifications(tokenDTO.getTypeOfVerifications());
				transactionHistory.setEmailId(userInfoModel1.getEmailId());
				transactionInfoRepository.save(transactionHistory);

				if (transactionHistory != null) {
					credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
							configInfo.getConfigValue() + decryptedWalletAddress);
					Biokkoin assetToken = null;
					if (tokenDTO.getGasPrice() == 1) {
						assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
								Contract.GAS_PRICE, Contract.GAS_LIMIT);
					}
					if (tokenDTO.getGasPrice() == 2) {
						assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
								Contract.GAS_PRICE, Contract.GAS_LIMIT);
					}
					if (tokenDTO.getGasPrice() == 3) {
						assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
								Contract.GAS_PRICE, Contract.GAS_LIMIT);
					}
					// BigInteger Token = new
					// BigDecimal(userRequestCoinInfo.getRequestCoin()).toBigInteger();
					BigInteger Token = BigDecimal.valueOf(tokenDTO.getSendToken()).toBigInteger();
					transactionReceipt = assetToken
							.transfer(EncryptDecrypt.decrypt(userRequestCoinInfo.getFromAddress()), Token).send();
				}
			} catch (Exception e) {

				e.printStackTrace();
			}
			// return true;
			// }).thenAccept(product -> {
			 if (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.success"))) {
				if (transactionHistory != null) {
					transactionHistory.setTxstatus(env.getProperty("status.success"));
					userRequestCoinInfo.setTranscationType(1);
					userRequestCoinRepository.save(userRequestCoinInfo);
					transactionInfoRepository.save(transactionHistory);
					tokenDTO.setPaymentMode(userRequestCoinInfo.getPaymentMode());
					tokenDTO.setReceiver(userRequestCoinInfo.getReceiveraName());
					tokenDTO.setSender(userRequestCoinInfo.getSenderName());
					tokenDTO.setRequestTokens(tokenDTO.getSendToken().doubleValue());
					tokenDTO.setTransferAmount(new BigDecimal(transactionHistory.getTransferToken()));
					tokenService.sendETHCoinPushNotificationFrom(userInfoModel, tokenDTO);
					tokenService.sendETHCoinPushNotificationTo(userInfoModel1, tokenDTO);
					return true;
				}
			}
		 } else if(transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.failed"))){
			
				transactionHistory.setTxstatus(env.getProperty("status.failed"));
				transactionInfoRepository.save(transactionHistory);
				return false;
		 }		
		return false;
	}

	public boolean validPaymentModeUser(TokenDTO tokenDTO) {
		if (tokenDTO.getPaymentMode().equals(env.getProperty("eth.payment"))
				|| tokenDTO.getPaymentMode().equals(env.getProperty("bio.payment"))) {
			return true;
		}
		return false;
	}

	public boolean validCoin(TokenDTO tokenDTO) throws Exception {

		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");
		RegisterInfo equacoinUserInfo = userRegisterRepository
				.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		if (equacoinUserInfo != null && configInfo != null) {
			String decryptedWalletAddress = EncryptDecrypt.decrypt(equacoinUserInfo.getEthWalletAddress());
			String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(),
					decryptedWalletAddress);

			if (tokenDTO.getPaymentMode().equals(env.getProperty("eth.payment"))) {

				tokenDTO.setEthWalletAddress(etherWalletAddress);
				tokenDTO.setCentralAdmin(tokenDTO.getEthWalletAddress());
				EthGetBalance ethGetBalance;
				ethGetBalance = web3j.ethGetBalance(tokenDTO.getCentralAdmin(), DefaultBlockParameterName.LATEST)
						.sendAsync().get();
				BigInteger wei = ethGetBalance.getBalance();
				BigDecimal amountCheck = Convert.fromWei(wei.toString(), Convert.Unit.ETHER);

				if (tokenDTO.getRequestTokens().doubleValue() < amountCheck.doubleValue()) {
					return true;
				}
			}

			if (tokenDTO.getPaymentMode().equals(env.getProperty("bio.payment"))) {

				Credentials credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),

						configInfo.getConfigValue() + decryptedWalletAddress);

				Biokkoin assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials, gasPrice,
						gasLimit);
				BigInteger b = assetToken.balanceOf(etherWalletAddress).send();
				BigDecimal k = BigDecimal.valueOf(tokenDTO.getRequestTokens());
				if (b.compareTo(k.toBigInteger()) == 1) {
					return true;
				}
			}

		}
		return false;
	}

	@SuppressWarnings("unused")
	public boolean isSendAmountToUser(TokenDTO tokenDTO) throws Exception {
		Config configInfo = configInfoRepository.findConfigByConfigKey("walletfile");

		TransactionHistory transactionHistory = new TransactionHistory();

		if (tokenDTO.getPaymentMode().equals(env.getProperty("eth.payment"))) {
			RegisterInfo equacoinUserInfos = userRegisterRepository
					.findRegisterInfoByDummywalletaddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));
			RegisterInfo equacoinUserInfo = userRegisterRepository
					.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
			String decryptedWalletAddress = EncryptDecrypt.decrypt(equacoinUserInfo.getEthWalletAddress());
			String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(),
					decryptedWalletAddress);
			transactionReceipt = new TransactionReceipt();

			// CompletableFuture.supplyAsync(() -> {
			Credentials credentials;
			try {

				transactionHistory.setFromAddress(EncryptDecrypt.encrypt(etherWalletAddress));
				transactionHistory.setToAddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));
				transactionHistory.setCryptoAmount(new BigDecimal(tokenDTO.getRequestTokens()));
				transactionHistory.setTransferToken(0.00);
				transactionHistory.setPaymentMode(env.getProperty("eth.payment"));
				transactionHistory.setTxDate(new Date());
				transactionHistory.setSenderId(equacoinUserInfo.getId());
				transactionHistory.setReceiverId(equacoinUserInfos.getId());
				transactionHistory.setTxstatus("status.pending");
				// transactionHistory.setRegisterInfo(equacoinUserInfo);
				transactionInfoRepository.save(transactionHistory);
				if (transactionHistory != null) {
					credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
							configInfo.getConfigValue() + decryptedWalletAddress);
					transactionReceipt = Transfer.sendFunds(web3j, credentials, tokenDTO.getToAddress(),
							new BigDecimal(tokenDTO.getRequestTokens()), Convert.Unit.ETHER).send();
				}
			} catch (Exception e) {

				e.printStackTrace();
			}

			// return "call blackchain";
			// }).thenAccept(product -> {

			// if
			// (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.success")))
			// {
			if (transactionHistory != null) {
				transactionHistory.setTxstatus(env.getProperty("status.success"));
				transactionInfoRepository.save(transactionHistory);
				tokenDTO.setReceiver(equacoinUserInfos.getFirstName());
				tokenDTO.setSender(equacoinUserInfo.getFirstName());
				tokenDTO.setTransferAmount(transactionHistory.getCryptoAmount());
				tokenService.sendETHCoinPushNotificationFrom(equacoinUserInfo, tokenDTO);
				tokenService.sendETHCoinPushNotificationTo(equacoinUserInfos, tokenDTO);
				return true;
			}
			// } else if
			// (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.failed")))
			// {
			if (transactionHistory != null) {
				transactionHistory.setTxstatus(env.getProperty("status.failed"));
				transactionInfoRepository.save(transactionHistory);
				return false;
			}
		}
		// });

		// }
		// System.out.println("outside bio");
		if (tokenDTO.getPaymentMode().equals(env.getProperty("bio.payment"))) {
			System.out.println("inside bio");
			RegisterInfo userInfoModel = userRegisterRepository
					.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
			RegisterInfo userInfoModel1 = userRegisterRepository
					.findRegisterInfoByDummywalletaddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));

			String decryptedWalletAddress = EncryptDecrypt.decrypt(userInfoModel.getEthWalletAddress());
			String etherWalletAddress = bioCoinUtils.getWalletAddress(configInfo.getConfigValue(),
					decryptedWalletAddress);
			transactionReceipt = new TransactionReceipt();

			// CompletableFuture.supplyAsync(() -> {
			System.out.println("inside async");
			Credentials credentials;
			try {

				transactionHistory.setFromAddress(EncryptDecrypt.encrypt(etherWalletAddress));
				transactionHistory.setToAddress(EncryptDecrypt.encrypt(tokenDTO.getToAddress()));
				transactionHistory.setCryptoAmount(new BigDecimal("0.0"));
				transactionHistory.setTransferToken(tokenDTO.getRequestTokens());
				transactionHistory.setPaymentMode(env.getProperty("bio.payment"));
				transactionHistory.setTxDate(new Date());
				transactionHistory.setSenderId(userInfoModel.getId());
				transactionHistory.setReceiverId(userInfoModel1.getId());
				// transactionHistory.setRegisterInfo(userInfoModel);
				transactionHistory.setTxstatus(env.getProperty("status.pending"));
				transactionInfoRepository.save(transactionHistory);
				if (transactionHistory != null) {
					credentials = WalletUtils.loadCredentials(tokenDTO.getEthWalletPassword(),
							configInfo.getConfigValue() + decryptedWalletAddress);
					Biokkoin assetToken = null;
					if (tokenDTO.getGasPrice() == 1) {
						assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
								Contract.GAS_PRICE, Contract.GAS_LIMIT);
					}
					if (tokenDTO.getGasPrice() == 2) {
						assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
								Contract.GAS_PRICE, Contract.GAS_LIMIT);
					}
					if (tokenDTO.getGasPrice() == 3) {
						assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
								Contract.GAS_PRICE, Contract.GAS_LIMIT);
					}

					BigInteger token = new BigDecimal(tokenDTO.getRequestTokens()).toBigInteger();
					transactionReceipt = assetToken.transfer(tokenDTO.getToAddress(), token).send();

				}
			} catch (Exception e) {
				System.out.println("inside async");
				e.printStackTrace();
			}

			// return true;
			// }).thenAccept(product -> {
			System.out.println("then accept");
			// if
			// (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.success")))
			// {
			if (transactionHistory != null) {

				transactionHistory.setTxstatus(env.getProperty("status.success"));
				transactionInfoRepository.save(transactionHistory);
				tokenDTO.setReceiver(userInfoModel1.getFirstName());
				tokenDTO.setSender(userInfoModel.getFirstName());
				tokenDTO.setTransferAmount(new BigDecimal(transactionHistory.getTransferToken()));
				tokenService.sendETHCoinPushNotificationFrom(userInfoModel, tokenDTO);
				tokenService.sendETHCoinPushNotificationTo(userInfoModel1, tokenDTO);
				return true;
			}
			// } else if
			// (transactionReceipt.getStatus().equals(env.getProperty("transactionReceipt.failed")))
			// {
			if (transactionHistory != null) {
				transactionHistory.setTxstatus(env.getProperty("status.failed"));
				transactionInfoRepository.save(transactionHistory);
				return false;
			}
		}
		// });

		// }

		return false;

	}

	public LoginDTO isGetDashboardValues(TokenDTO tokenDTO) throws Exception {
		LoginDTO responseDTO = new LoginDTO();
		RegisterInfo userInfoModel = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(tokenDTO.getEmailId());
		Config config = configInfoRepository.findConfigByConfigKey("walletfile");

		if (userInfoModel != null && config.getConfigValue() != null) {
			String decryptWalletAddress = EncryptDecrypt.decrypt(userInfoModel.getEthWalletAddress());
			// String walletAddress = EncryptDecrypt.decrypt(
			// userInfoModel.getEtherWalletAddress());
			String walletAddress = EncryptDecrypt.decrypt(userInfoModel.getDummywalletaddress());
			String decryptWalletPassword = EncryptDecrypt.decrypt(userInfoModel.getEthWalletPassword());
			if (walletAddress == null) {
				return null;
			}
			tokenDTO.setCentralAdmin(walletAddress);
			boolean isToken = tokenService.checkMainbalance(tokenDTO);
			if (isToken) {
				List<UserRequestCoinInfo> userRequestCoinInfo = userRequestCoinRepository
						.findByToAddressAndTranscationType(userInfoModel.getDummywalletaddress(), 0);
				responseDTO.setRequestCoinCount(userRequestCoinInfo.size());
				responseDTO.setETHBalance(tokenDTO.getMainBalance());
				Credentials credentials = WalletUtils.loadCredentials(decryptWalletPassword,
						config.getConfigValue() + decryptWalletAddress);

				Biokkoin assetToken = Biokkoin.load(env.getProperty("token.address"), web3j, credentials,
						BigInteger.valueOf(3000000), BigInteger.valueOf(3000000));
				BigInteger balance = assetToken.balanceOf(walletAddress).send();
				LOG.info("BigInterToken" + balance);
				BigDecimal balance1 = new BigDecimal(balance);
				Double EQCBalance = balance1.doubleValue();
				BigDecimal b = new BigDecimal(EQCBalance, MathContext.DECIMAL64);
				LOG.info("Bio balance" + b);
				// BigDecimal
				// oneether=currentValueUtils.getEtherValueForOneUSD();
				BigDecimal oneether = currentValueUtils.getEtherValueForOneUSD();
				BigDecimal etherusd = responseDTO.getETHBalance().multiply(oneether);
				MathContext m = new MathContext(8);
				responseDTO.setUserETHUSD(etherusd.round(m));
				BigDecimal onebio = b.multiply(new BigDecimal("15"));
				LOG.info("one Bio balance" + onebio);
				// BigDecimal
				// eqcusd=currentValueUtils.getBTCValueForOneUSD().multiply(oneeqc);
				responseDTO.setUserBIOUSD(onebio.toString());
				responseDTO.setBIOBalance(b.toString());
				responseDTO.setStatus("success");
				return responseDTO;
			}
		} else {
			responseDTO.setStatus("failed");
			return responseDTO;

		}
		return null;
	}

	public boolean ValidateInputParamsOfApproval(TokenDTO tokenDTO) {

		if (tokenDTO.getId() != null && StringUtils.isNotBlank(tokenDTO.getId().toString())
				&& tokenDTO.getTxStatus() != null && StringUtils.isNotBlank(tokenDTO.getTxStatus())) {
			return true;
		}
		return false;
	}

	public boolean validateKYCParams(KycDTO kycDTO, MultipartFile kycDoc1) {
		LOG.info("Insede validateKYCParams");
		if (kycDTO.getDob() != null && StringUtils.isNotBlank(kycDTO.getDob()) && kycDTO.getAddress() != null
				&& StringUtils.isNotBlank(kycDTO.getAddress()) && kycDTO.getCity() != null
				&& StringUtils.isNotBlank(kycDTO.getCity()) && kycDTO.getCountry() != null
				&& StringUtils.isNotBlank(kycDTO.getCountry()) && kycDTO.getEmailId() != null
				&& StringUtils.isNotBlank(kycDTO.getEmailId()) && kycDTO.getFullName() != null
				&& StringUtils.isNotBlank(kycDTO.getFullName()) && kycDTO.getGender() != null
				&& StringUtils.isNotBlank(kycDTO.getGender()) && kycDTO.getMobileNo() != null
				&& StringUtils.isNotBlank(kycDTO.getMobileNo()) && kycDoc1 != null) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unused")
	public boolean compareEmailId(KycDTO kycDTO) {
		HttpSession session = SessionCollector.find(kycDTO.getSessionId());
		String email = (String) session.getAttribute("emailId");

		RegisterInfo userModelInfo = userRegisterRepository.findRegisterInfoByEmailIdIgnoreCase(kycDTO.getEmailId());
		if (userModelInfo != null) {
			return true;
		}
		return false;
	}

	public boolean validateFileExtn(MultipartFile kycDoc1, MultipartFile kycDoc2, MultipartFile kycDoc3,
			MultipartFile kycDoc4) {

		String fileName1 = kycDoc1.getOriginalFilename();
		String fileName2 = kycDoc2.getOriginalFilename();
		String fileName3 = kycDoc3.getOriginalFilename();
		String fileName4 = kycDoc4.getOriginalFilename();

		if (kycDoc1 != null && kycDoc2 != null && kycDoc3 != null && kycDoc4 != null) {

			if (fileName1.substring(fileName1.lastIndexOf(".") + 1).equalsIgnoreCase("png")
					|| fileName1.substring(fileName1.lastIndexOf(".") + 1).equalsIgnoreCase("jpg")
					|| fileName1.substring(fileName1.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg")
					|| fileName1.substring(fileName1.lastIndexOf(".") + 1).equalsIgnoreCase("pdf")) {
				if (fileName2.substring(fileName2.lastIndexOf(".") + 1).equalsIgnoreCase("png")
						|| fileName2.substring(fileName2.lastIndexOf(".") + 1).equalsIgnoreCase("jpg")
						|| fileName2.substring(fileName2.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg")
						|| fileName2.substring(fileName2.lastIndexOf(".") + 1).equalsIgnoreCase("pdf")) {
					if (fileName3.substring(fileName3.lastIndexOf(".") + 1).equalsIgnoreCase("png")
							|| fileName3.substring(fileName3.lastIndexOf(".") + 1).equalsIgnoreCase("jpg")
							|| fileName3.substring(fileName3.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg")
							|| fileName3.substring(fileName3.lastIndexOf(".") + 1).equalsIgnoreCase("pdf")) {
						if (fileName4.substring(fileName4.lastIndexOf(".") + 1).equalsIgnoreCase("jpg")
								|| fileName4.substring(fileName4.lastIndexOf(".") + 1).equalsIgnoreCase("png")
								|| fileName4.substring(fileName4.lastIndexOf(".") + 1).equalsIgnoreCase("jpeg")
								|| fileName4.substring(fileName4.lastIndexOf(".") + 1).equalsIgnoreCase("pdf")) {
							return true;
						}

					}

				}

			}

		}

		return false;
	}

	public boolean validateUpdateKYCParams(KycDTO kycDTO) {
		if (kycDTO.getId() != null && StringUtils.isNotBlank(kycDTO.getId().toString()) && kycDTO.getKycStatus() != null
				&& StringUtils.isNotBlank(kycDTO.getKycStatus().toString()) && kycDTO.getTypeOfVerifications() != null
				&& StringUtils.isNotBlank(kycDTO.getTypeOfVerifications().toString())) {
			return true;
		}
		return false;
	}

	public KycDTO listKYC(KycInfo kycInfo) {

		KycDTO kycList = new KycDTO();
		kycList.setId(kycInfo.getId());
		kycList.setFullName(kycInfo.getFullName());
		kycList.setDob(kycInfo.getDob());
		kycList.setCity(kycInfo.getCity());
		kycList.setCountry(kycInfo.getCountry());
		kycList.setAddress(kycInfo.getHomeAddress());
		kycList.setGender(kycInfo.getGender());
		kycList.setEmailId(kycInfo.getEmailId());
		kycList.setMobileNo(kycInfo.getPhoneNo());
		kycList.setKycDoc1Path(env.getProperty("apache.server") + kycInfo.getKycDoc1Path());

		// if (!kycInfo.getKycDoc2Path().trim().equals("")) {
		kycList.setKycDoc2Path(env.getProperty("apache.server") + kycInfo.getKycDoc2Path());
		kycList.setKycDoc3Path(env.getProperty("apache.server") + kycInfo.getKycDoc3Path());
		kycList.setKycDoc4Path(env.getProperty("apache.server") + kycInfo.getKycDoc4Path());
		// }
		kycList.setKycDoc1Name(kycInfo.getKycDoc1Name());
		kycList.setKycDoc2Name(kycInfo.getKycDoc2Name());
		kycList.setKycDoc3Name(kycInfo.getKycDoc3Name());
		kycList.setKycDoc4Name(kycInfo.getKycDoc4Name());
		kycList.setKycStatus(kycInfo.getKycStatus());
		kycList.setTypeOfVerifications(kycInfo.getTypeOfVerifications());

		return kycList;
	}

}