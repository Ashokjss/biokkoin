package com.biocoin.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.biocoin.dto.TokenDTO;

@Service
public class CurrentValueUtils {

	private static final Logger LOG = LoggerFactory.getLogger(CurrentValueUtils.class);

	@Autowired
	private Environment env;

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	public Double getBitcoinValueForOneDollar() throws JSONException, IOException {
		JSONObject json = readJsonFromUrl("https://api.coinbase.com/v2/prices/BTC-USD/spot");
		JSONObject json2 = (JSONObject) json.get("data");

		String amount = (String) json2.get("amount");
		BigDecimal Dollars = new BigDecimal(amount);

		Double a = 1 / Dollars.doubleValue();
		DecimalFormat df = new DecimalFormat("#.########");

		double value = Double.parseDouble(df.format(a));
		return value;
	}

	public Double getEtherValueForOneDollar() throws JSONException, IOException {
		JSONObject json = readJsonFromUrl("https://api.coinbase.com/v2/prices/ETH-USD/spot");
		JSONObject json2 = (JSONObject) json.get("data");

		String amount = (String) json2.get("amount");
		BigDecimal Dollars = new BigDecimal(amount);

		Double a = 1 / Dollars.doubleValue();
		DecimalFormat df = new DecimalFormat("#.#########");

		double value = Double.parseDouble(df.format(a));
		System.out.println(value);
		return value;
	}

	public BigDecimal getEtherValueForOneUSD() throws JSONException, IOException {
		JSONObject json = readJsonFromUrl("https://api.coinbase.com/v2/prices/ETH-USD/spot");
		JSONObject json2 = (JSONObject) json.get("data");
		String amount = (String) json2.get("amount");
		BigDecimal Dollars = new BigDecimal(amount);
		return Dollars;
	}

	public Double getCurrencyRateInINR() throws Exception {

		JSONObject json = readJsonFromUrl("http://rate-exchange-1.appspot.com/currency?from=USD&to=INR");
		Double json2 = (Double) json.get("rate");

		System.out.println(json2);
//		Double value = (Double) json2.get("val");

		System.out.println(json2);
		return json2;
	}


public Double getCryptoRateFromDollar(TokenDTO tokenDTO) throws JSONException, IOException {

		// URL Converts the 1 USD values to ETH, BCH, BTC

		Double value = 0.0;
		if (tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("eth.payment"))) {

			JSONObject json = readJsonFromUrl(env.getProperty("crypto.compare.url.eth"));
			LOG.info("Inside ETH : " + json.get("data"));
			JSONObject data = new JSONObject(json.get("data").toString());
			JSONObject quotes = new JSONObject(data.get("quotes").toString());

			JSONObject USD = new JSONObject(quotes.get("USD").toString());
			JSONObject ETH = new JSONObject(quotes.get("ETH").toString());
			LOG.info("Price from BTC to USD and ETH : " + ETH.get("price") + "::::::::::::" + USD.get("price"));

			Double eth = (Double) ETH.get("price");
			Double usd = (Double) USD.get("price");
			Double oneUSDValueToEther = eth / usd;

			value = oneUSDValueToEther;
		} else if(tokenDTO.getTypeOfPurchase().equalsIgnoreCase(env.getProperty("btc.payment"))){
			JSONObject json = readJsonFromUrl(env.getProperty("crypto.compare.url"));
			LOG.info("Inside BTC : " + json.get("data"));
			JSONObject data = new JSONObject(json.get("data").toString());
			JSONObject quotes = new JSONObject(data.get("quotes").toString());

			JSONObject USD = new JSONObject(quotes.get("USD").toString());
			JSONObject BTC = new JSONObject(quotes.get("BTC").toString());
			LOG.info("Price from BTC to USD : " + BTC.get("price") + "::::::::::::" + USD.get("price"));

			LOG.info("Inside BTC");
		
			Double btc = Double.parseDouble(BTC.get("price").toString());
			Double usd = Double.parseDouble(USD.get("price").toString());
			Double oneUSDValueToBTC = btc / usd;

			value = oneUSDValueToBTC;
		}
		return value;
	}
}
