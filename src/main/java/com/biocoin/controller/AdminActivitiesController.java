package com.biocoin.controller;

import java.util.List;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biocoin.dto.StatusResponseDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.BiocoinValue;
import com.biocoin.service.BiocoinValueService;
import com.biocoin.service.TokenService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.SessionCollector;
import com.biocoin.utils.UserUtils;
import com.google.gson.Gson;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/biocoin/api/biocoinuser")
@CrossOrigin
public class AdminActivitiesController {
	private static final Logger LOG = LoggerFactory.getLogger(AdminActivitiesController.class);

	@Autowired
	private Environment env;
	@Autowired
	private BioCoinUtils biocoinutils;
	@Autowired
	private TokenService tokenService;
	@Autowired
	private BiocoinValueService biocoinValueService;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private UserUtils userUtils;

	@CrossOrigin
	@RequestMapping(value = "/admin/manual/token/transfer", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Transfer tokens to users", notes = "Need to transfer tokens to users")
	public ResponseEntity<String> transferTokenstoUsersForApproval(
			@ApiParam(value = "Transfer tokens between users", required = true) @RequestBody TokenDTO tokenDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			boolean isValidateEthAddress = biocoinutils.isValidateEthAddress(tokenDTO);
			if (!isValidateEthAddress) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("invalid.eth.address"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			boolean isSameAddress = biocoinutils.isSameaddress(tokenDTO);
			if (isSameAddress) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("Same.wallet.address"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isSamePassword = biocoinutils.isSamePassword(tokenDTO);
			if (!isSamePassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("same.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			boolean etherValidation = biocoinutils.etherValidation(tokenDTO);
			if (!etherValidation) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("ether.balance.valid"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String tokenAmountValidation = biocoinutils.tokenAmountValidationForAdmin(tokenDTO);
			if (!tokenAmountValidation.equalsIgnoreCase(env.getProperty("token.balance"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(tokenAmountValidation);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String validatePersonsType = tokenService.validateTypeOfPersons(tokenDTO);
			if (!validatePersonsType.equalsIgnoreCase(env.getProperty("valid.typeofverification"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(validatePersonsType);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String transferTokens = tokenService.manualTokenTransferForAdmin(tokenDTO);
			if (transferTokens.equalsIgnoreCase(env.getProperty("token.transfer.success.approval"))) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(transferTokens);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(transferTokens);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Manual Token Transfer : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}
	
	@CrossOrigin
	@RequestMapping(value = "/admin/manual/token/transfer/after/approval", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Transfer tokens to users", notes = "Need to transfer tokens to users")
	public ResponseEntity<String> transferTokenstoUsers(
			@ApiParam(value = "Transfer tokens to users", required = true) @RequestBody TokenDTO tokenDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			boolean isValid = userUtils.ValidateInputParamsOfApproval(tokenDTO);
			if(!isValid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("invalid.input"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String transfer = tokenService.ValidateInputParamsOfApproval(tokenDTO);
			if(!transfer.equalsIgnoreCase(env.getProperty("token.transfer.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(transfer);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(transfer);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Manual Token Transfer : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}
	
			
	@CrossOrigin
	@RequestMapping(value = "biocoin/value/change", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Biocoin value change", notes = "biocoin value change")
	public ResponseEntity<String> biocoinValueChange(
			@ApiParam(value = "biocoinValueChange", required = true) @RequestBody TokenDTO tokenDTO) throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			// int update_biocoin_rate=biocoinValueService.
			BiocoinValue biorate = biocoinValueService.findById(1);
			if (biorate == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("table.not.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}
			biorate.setBiocoin_value(tokenDTO.getBiocoinrate());
			biocoinValueService.update(biorate);
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("coinrate.update"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Biocoin Value change : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "users/list", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	@ApiOperation(value = "usersList", notes = "usersList")
	public ResponseEntity<String> usersList(
			@ApiParam(value = "usersList", required = true) @RequestBody UserRegisterDTO userRegisterDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			List<UserRegisterDTO> useList = userRegisterService.userList(userRegisterDTO);
			statusResponseDTO.setTotalPages(userRegisterDTO.getTotalPages());
			statusResponseDTO.setTotalElements(userRegisterDTO.getTotalElements());
			statusResponseDTO.setUserList(useList);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Problem in users list : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/update/allocate/tokens", method = RequestMethod.POST, consumes = {"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Allocate TokensTo Top Managemnets", notes = "Allocate Tokens To Top Managemnets")
	public ResponseEntity<String> allocateTokensToTopManagemnets(
			@ApiParam(value = "Allocate TokensTo Top Managemnets", required = true) @RequestBody UserRegisterDTO userRegisterDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String saveData = biocoinValueService.saveAllocateTokens(userRegisterDTO);
			if (!saveData.equalsIgnoreCase(env.getProperty("update.charity.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(saveData);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(saveData);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Problem in users list : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/list/allocated/tokens", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "To Allocate Tokens", notes = "Need to Allocate Tokens")
	public synchronized ResponseEntity<String> listAllocatedTokens(
			@ApiParam(value = "Required user details", required = true) @RequestBody UserRegisterDTO userRegisterDTO){
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {	
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			List<UserRegisterDTO> getDetails = biocoinValueService.listAllocatedTokens(userRegisterDTO);
			if(getDetails == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("list.allocate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setTotalPages(userRegisterDTO.getTotalPages());
			statusResponseDTO.setTotalElements(userRegisterDTO.getTotalElements());
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("list.allocate.success"));
			statusResponseDTO.setAllocatedDetails(getDetails);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in tokencreation  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/delete/allocate/tokens", method = RequestMethod.POST, consumes = {"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Allocate TokensTo Top Managemnets", notes = "Allocate Tokens To Top Managemnets")
	public ResponseEntity<String> deleteAllocateTokensToTopManagemnets(
			@ApiParam(value = "Allocate TokensTo Top Managemnets", required = true) @RequestBody UserRegisterDTO userRegisterDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValid = biocoinValueService.deleteAllocateTokens(userRegisterDTO);
			if (!isValid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("update.charity.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("delete.charity.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Problem in users list : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/list/tokens/rate", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "To Tokens Rate", notes = "Need to Show Tokens Rate")
	public synchronized ResponseEntity<String> listValuesOfTokens(
			@ApiParam(value = "Required Tokens Rate", required = true) @RequestBody UserRegisterDTO userRegisterDTO){
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {	
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			List<UserRegisterDTO> getRate = biocoinValueService.listTokensRate(userRegisterDTO);
			if(getRate == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("list.allocate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("list.allocate.success"));
			statusResponseDTO.setValuesInUSD(getRate);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in tokencreation  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/update/token/rate", method = RequestMethod.POST, consumes = {"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Update Token Rate", notes = "Update Token Rate")
	public ResponseEntity<String> updateTokensRate(
			@ApiParam(value = "Update Token Rate", required = true) @RequestBody UserRegisterDTO userRegisterDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValid = biocoinValueService.updateTokensRate(userRegisterDTO);
			if (!isValid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("update.charity.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("update.charity.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Problem in users list : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/list/ico/periods", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "To show ico periods", notes = "Need to show ico periods")
	public synchronized ResponseEntity<String> listICOPeriods(
			@ApiParam(value = "Required to show ico periods", required = true) @RequestBody UserRegisterDTO userRegisterDTO){
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {	
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			List<UserRegisterDTO> getICOPeriod = biocoinValueService.listICOPeriod(userRegisterDTO);
			if(getICOPeriod == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("list.allocate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setTotalPages(userRegisterDTO.getTotalPages());
			statusResponseDTO.setTotalElements(userRegisterDTO.getTotalElements());
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("list.allocate.success"));
			statusResponseDTO.setListICOPeriodInfo(getICOPeriod);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in tokencreation  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/update/ico/periods", method = RequestMethod.POST, consumes = {"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Update ico periods", notes = "Update ico periods")
	public ResponseEntity<String> updateICOPeriods(
			@ApiParam(value = "Update ico periods", required = true) @RequestBody UserRegisterDTO userRegisterDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String isValid = biocoinValueService.updateICOPeriods(userRegisterDTO);
			if (!isValid.equalsIgnoreCase(env.getProperty("update.charity.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(isValid);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(isValid);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Problem in ICO Periods : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/delete/ico/periods", method = RequestMethod.POST, consumes = {"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "Delete ico periods", notes = "Delete ico periods")
	public ResponseEntity<String> deleteICOPeriods(
			@ApiParam(value = "Delete ico periods", required = true) @RequestBody UserRegisterDTO userRegisterDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValid = biocoinValueService.deleteICOPeriods(userRegisterDTO);
			if (!isValid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("update.charity.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("delete.charity.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Problem in users list : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/users/list/filter", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Users List filter", notes = "Need to show Users List filter")
	public synchronized ResponseEntity<String> usersListFilter(
			@ApiParam(value = "Users List filter", required = true) @RequestBody UserRegisterDTO userRegisterDTO, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			List<UserRegisterDTO> usersLists = biocoinutils.usersListFilter(userRegisterDTO);
			if (usersLists == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("users.list.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("users.list.success"));
			statusResponseDTO.setTotalPages(userRegisterDTO.getTotalPages());
			statusResponseDTO.setTotalElements(userRegisterDTO.getTotalElements());
			statusResponseDTO.setUserList(usersLists);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in token transaction history  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
}
