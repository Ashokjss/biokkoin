package com.biocoin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biocoin.dto.LoginDTO;
import com.biocoin.dto.StatusResponseDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.RegisterInfo;
import com.biocoin.service.UserRegisterService;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.BitCoinUtils;
import com.biocoin.utils.EncryptDecrypt;
import com.google.gson.Gson;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.biocoin.dto.KycDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.utils.SessionCollector;
import com.biocoin.utils.UserUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.biocoin.utils.BioCoinConstants.ACTIVITY_FEED_PAGE_SIZE;

@RestController
@RequestMapping(value = "/biocoin/api/biocoinuser")
@CrossOrigin
public class UserRegisterController {
	private static final Logger LOG = LoggerFactory.getLogger(UserRegisterController.class);

	@Autowired
	private UserUtils userUtils;
	@Autowired
	private Environment env;
	@Autowired
	private UserRegisterService userRegisterService;
	@Autowired
	private BioCoinUtils BioCoinUtils;
	@Autowired
	private BitCoinUtils bitCoinUtils;

	@CrossOrigin
	@RequestMapping(value = "/userregister", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "register account", notes = "Need to get user details")
	public synchronized ResponseEntity<String> registerUser(
			@ApiParam(value = "Required user details", required = true) @RequestBody UserRegisterDTO userRegisterDTO) throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		boolean isValid = BioCoinUtils.validateRegistrationParam(userRegisterDTO);
		if (!isValid) {
			// User registration validation failed
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("incorrectDetails"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		boolean isValidEmailId = BioCoinUtils.validateEmail(userRegisterDTO.getEmailId());
		if (!isValidEmailId) {

			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("register.validate.email"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

		}

		boolean isConformPassword = BioCoinUtils.validateConfirmPassword(userRegisterDTO);
		if (!isConformPassword) {
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("password.exist"));

			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		boolean isConfirmWalletPassword = BioCoinUtils.validateConfirmWalletPassword(userRegisterDTO);
		if (!isConfirmWalletPassword) {
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("wallet.password.exist"));

			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		RegisterInfo isuserExists = userRegisterService.isAccountExistCheckByEmailId(userRegisterDTO.getEmailId());

		if (isuserExists != null) {
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("email.exist"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

		}
		
		boolean isLogin = bitCoinUtils.loginCrypto(userRegisterDTO);
		if (!isLogin) {
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("login.crypto.failed"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

		try {
			boolean isRegister = userRegisterService.saveAccountInfo(userRegisterDTO);
			if (isRegister) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("register.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("register.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in registration  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/verifyaccount", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "login user", notes = "User verification")
	public ResponseEntity<String> userLoginVerification(
			@ApiParam(value = "Required user mobile no and password ", required = true) @RequestBody UserRegisterDTO userRegisterDTO,
			HttpServletRequest request, HttpServletResponse response) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		try {

			String isEmailVerified = userRegisterService.isEmailVerified(userRegisterDTO);
			if (isEmailVerified.equalsIgnoreCase("falied")) {
				statusResponseDTO.setMessage("user not valid");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(isEmailVerified);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in login  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	// USER LOGIN API and GENERATE OTP SEND TO MOBILE NUMBER API

	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	@ApiOperation(value = "login user", notes = "Validate user Login")
	public ResponseEntity<String> userLogin(
			@ApiParam(value = "Required user Mail ID and password ", required = true) @RequestBody UserRegisterDTO userRegisterDTO,
			HttpServletRequest request, HttpServletResponse response) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			if (!BioCoinUtils.validateLoginParam(userRegisterDTO)) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValidEmail = BioCoinUtils.validateEmail(userRegisterDTO.getEmailId());
			if (!isValidEmail) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("register.validate.email"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String checkStatus = userRegisterService.isAccountHolderActiveOrNot(userRegisterDTO);
			if (!checkStatus.equalsIgnoreCase("success")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(checkStatus);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			// boolean userData =
			// userRegisterService.getUserInfo(userRegisterDTO);
			// if (userData) {
			String encryptPassword = EncryptDecrypt.encrypt(userRegisterDTO.getPassword().trim());
			System.out.println("encryptPassword=" + encryptPassword);
			LoginDTO responseDTO = userRegisterService.isEmailIdAndPasswordExist(userRegisterDTO, request,
					encryptPassword);
			if (responseDTO.getStatus().equalsIgnoreCase("failed")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("login.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			statusResponseDTO.setSecuredKey(userRegisterDTO.getSecuredKey());
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("login.verify.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			// } else {
			// statusResponseDTO.setStatus(env.getProperty("success"));
			// statusResponseDTO.setMessage(env.getProperty("login.verify.success"));
			// return new ResponseEntity<String>(new
			// Gson().toJson(statusResponseDTO), HttpStatus.OK);
			// }

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in login  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	// LOGIN AFTER ENTER OTP API

	@CrossOrigin
	@RequestMapping(value = "/login/secure", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" })
	@ApiOperation(value = "login user", notes = "Validate user Login")
	public ResponseEntity<String> userSecurityVerification(
			@ApiParam(value = "Required user Security PIN ", required = true) @RequestBody UserRegisterDTO userRegisterDTO,
			HttpServletRequest request, HttpServletResponse response) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			if (!BioCoinUtils.validateSecurityKey(userRegisterDTO)) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("login.key"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			RegisterInfo regInfo = userRegisterService.isAccountExistCheckByEmailId(userRegisterDTO.getEmailId());
			if (regInfo == null) {
				statusResponseDTO.setStatus(env.getProperty("mail.failure"));
				statusResponseDTO.setMessage(env.getProperty("mail.message"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			boolean isLogin = bitCoinUtils.loginCrypto(userRegisterDTO);
			if (!isLogin) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("login.crypto.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			LoginDTO responseDTO = BioCoinUtils.validateSecuredKey(userRegisterDTO, request);
			if (responseDTO.getStatus().equalsIgnoreCase("failed")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("login.key.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("login.success"));
			statusResponseDTO.setLoginInfo(responseDTO);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in securelogin  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/uploadkyc", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "register account", notes = "Need to get user details")
	public synchronized ResponseEntity<String> userKycVerification(
			@ApiParam(value = "Required user details", required = true) @RequestParam(name = "userInfo", value = "userInfo", required = true) String kycDTOStr,
			@ApiParam(value = "Required file attachment", required = true) @RequestParam(name = "kycDoc1", value = "kycDoc1", required = true) MultipartFile kycDoc1,
			@ApiParam(value = "Required file attachment", required = true) @RequestParam(name = "kycDoc2", value = "kycDoc2", required = true) MultipartFile kycDoc2,
			@ApiParam(value = "Required file attachment", required = true) @RequestParam(name = "kycDoc3", value = "kycDoc3", required = true) MultipartFile kycDoc3,
			@ApiParam(value = "Required file attachment", required = true) @RequestParam(name = "kycDoc4", value = "kycDoc4", required = true) MultipartFile kycDoc4) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		LOG.info("Inside Uploadkyc");
		try {

			ObjectMapper mapper = new ObjectMapper();
			KycDTO kycDTO = null;

			try {
				kycDTO = mapper.readValue(kycDTOStr, KycDTO.class);
			} catch (Exception e) {
				e.printStackTrace();
				LOG.info("Catch block : " + e.toString());
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			HttpSession sessions = SessionCollector.find(kycDTO.getSessionId());
			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isVAlidParams = userUtils.validateKYCParams(kycDTO, kycDoc1);
			if (!isVAlidParams) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isCompareEmailId = userUtils.compareEmailId(kycDTO);
			{
				if (!isCompareEmailId) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("compare.email.failed"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}

			boolean isVAlidFileExtn = userUtils.validateFileExtn(kycDoc1, kycDoc2, kycDoc3, kycDoc4);
			{
				if (!isVAlidFileExtn) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("upload.req.extension"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}

			LOG.info("Mail id before Upload : " + kycDTO.getEmailId());
			if (kycDoc1 != null && kycDoc2 != null && kycDoc3 != null && kycDoc4 != null) {
				LOG.info("Both available kycDoc1 != null && kycDoc2 != null ");
				String kycDoc1FilePath = userRegisterService.userDocumentUpload(kycDoc1, kycDTO.getEmailId(),
						"KYCDoc1");
				String kycDoc2FilePath = userRegisterService.userDocumentUpload(kycDoc2, kycDTO.getEmailId(),
						"KYCDoc2");
				String kycDoc3FilePath = userRegisterService.userDocumentUpload(kycDoc3, kycDTO.getEmailId(),
						"KYCDoc3");
				String kycDoc4FilePath = userRegisterService.userDocumentUpload(kycDoc4, kycDTO.getEmailId(),
						"KYCDoc4");

				LOG.info("panIdFilePath : " + kycDoc1FilePath);
				if (kycDoc1FilePath == null || kycDoc2FilePath == null || kycDoc3FilePath == null
						|| kycDoc4FilePath == null) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage("Please Upload the Required Document");

					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}

				KycDTO kycDetails = userRegisterService.saveKycInfo(kycDTO, kycDoc1FilePath, kycDoc2FilePath,
						kycDoc3FilePath, kycDoc4FilePath, kycDoc1, kycDoc2, kycDoc3, kycDoc4);
				if (kycDetails == null) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("failure.save"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				} else {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("kyc.success"));
					statusResponseDTO.setKycInfo(kycDetails);
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				}

			} /*
				 * else if (kycDoc1 != null) {
				 * LOG.info("One available kycDoc1 != null"); String
				 * kycDoc1FilePath =
				 * userRegisterService.userDocumentUpload(kycDoc1,
				 * kycDTO.getEmailId(), "KYCDoc1");
				 * 
				 * LOG.info("panIdFilePath : " + kycDoc1FilePath); if
				 * (kycDoc1FilePath == null) {
				 * statusResponseDTO.setStatus(env.getProperty("failure"));
				 * statusResponseDTO.
				 * setMessage("Please Upload the Required Document"); return new
				 * ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
				 * HttpStatus.PARTIAL_CONTENT); } String kycDoc2FilePath = "";
				 * boolean isSaved = userRegisterService.saveKycInfo(kycDTO,
				 * kycDoc1FilePath, kycDoc2FilePath, kycDoc2, kycDoc2); if
				 * (!isSaved) {
				 * statusResponseDTO.setStatus(env.getProperty("failure"));
				 * statusResponseDTO.setMessage(env.getProperty("failure.save"))
				 * ; return new ResponseEntity<String>(new
				 * Gson().toJson(statusResponseDTO),
				 * HttpStatus.PARTIAL_CONTENT); } else {
				 * statusResponseDTO.setStatus(env.getProperty("success"));
				 * statusResponseDTO.setMessage(env.getProperty("kyc.success"));
				 * return new ResponseEntity<String>(new
				 * Gson().toJson(statusResponseDTO), HttpStatus.OK); } }
				 */ else {
				LOG.info("No One available");
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in uploadkyc  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/view/kyc", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Approve Or Reject KYC", notes = "Need to Approve Or Reject KYC")
	public synchronized ResponseEntity<String> viewUserKYC(
			@ApiParam(value = "Required KYC details", required = true) @RequestBody TokenDTO tokenDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		LOG.info("Inside /view/kyc");
		try {

			HttpSession sessions = SessionCollector.find(tokenDTO.getSessionId());

			LOG.info("Sesion value " + sessions);

			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			KycDTO kycInfo = userRegisterService.getKycDetails(tokenDTO);
			LOG.info("Kyc Value " + kycInfo);
			if (kycInfo != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("kyc.user.success"));
				statusResponseDTO.setKycUserInfo(kycInfo);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.user.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in KYC update : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/update/kycstatus", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Approve Or Reject KYC", notes = "Need to Approve Or Reject KYC")
	public synchronized ResponseEntity<String> updateUserKYC(
			@ApiParam(value = "Required KYC details", required = true) @RequestBody KycDTO kycDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		LOG.info("Inside /update/kycstatus");

		try {
			HttpSession sessions = SessionCollector.find(kycDTO.getSessionId());

			LOG.info("Sesion value------>" + sessions);

			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isUpdateKyc = userUtils.validateUpdateKYCParams(kycDTO);
			if (!isUpdateKyc) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("incorrectDetails"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isUpdated = userRegisterService.updateKYC(kycDTO);
			if (isUpdated) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(kycDTO.getMessage());
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.update.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in KYC update : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	/*
	 * @CrossOrigin
	 * 
	 * @RequestMapping(value = "/list/kyc", method = RequestMethod.POST,
	 * produces = { "application/json" })
	 * 
	 * @ApiOperation(value = "List All KYC", notes =
	 * "Need to list out all the KYC uploaded") public synchronized
	 * ResponseEntity<String> getAllKYC(
	 * 
	 * @ApiParam(value = "Required KYC details", required = true) @RequestBody
	 * TokenDTO tokenDTO) { StatusResponseDTO statusResponseDTO = new
	 * StatusResponseDTO();
	 * 
	 * try {
	 * 
	 * HttpSession sessions = SessionCollector.find(tokenDTO.getSessionId()); if
	 * (sessions == null) {
	 * statusResponseDTO.setStatus(env.getProperty("failure"));
	 * statusResponseDTO.setMessage(env.getProperty("session.expired")); return
	 * new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
	 * HttpStatus.PARTIAL_CONTENT); }
	 * 
	 * List<KycDTO> kycList = userRegisterService.kycList(); if (kycList !=
	 * null) { statusResponseDTO.setStatus(env.getProperty("success"));
	 * statusResponseDTO.setMessage(env.getProperty("kyc.list"));
	 * statusResponseDTO.setKycList(kycList); return new
	 * ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
	 * HttpStatus.OK); } else {
	 * statusResponseDTO.setStatus(env.getProperty("failure"));
	 * statusResponseDTO.setMessage(env.getProperty("kyc.not.exist")); return
	 * new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
	 * HttpStatus.PARTIAL_CONTENT); }
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * LOG.error("Problem in KYC List : ", e);
	 * statusResponseDTO.setStatus(env.getProperty("failure"));
	 * statusResponseDTO.setMessage(env.getProperty("server.problem")); return
	 * new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
	 * HttpStatus.PARTIAL_CONTENT); } }
	 */

	@CrossOrigin
	@RequestMapping(value = "/list/kyc", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "List All KYC", notes = "Need to list out all the KYC uploaded")
	public synchronized ResponseEntity<String> getAllKYC(
			@ApiParam(value = "Required KYC details", required = true) @RequestBody TokenDTO tokenDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		System.out.println("Inside of /list/kyc1 ###########################");
		try {

			HttpSession sessions = SessionCollector.find(tokenDTO.getSessionId());
			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			TokenDTO kycCount = userRegisterService.getKycCount(tokenDTO);

			final List<KycDTO> kycList = userRegisterService.getFeedItemsForKycUsers(new PageRequest(
					tokenDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC, "creationTime"));

			System.out.println("**********" + statusResponseDTO.getTotalElements());
			System.out.println("++++++++++++" + statusResponseDTO.getTotalPages());

			if (kycList != null && kycCount != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("kyc.list"));
				statusResponseDTO.setTotalElements(kycCount.getTotalElements());
				statusResponseDTO.setTotalPages(kycCount.getTotalPages());
				statusResponseDTO.setKycList(kycList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.not.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in KYC List : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/filter/kyc/status", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Get Filter Kyc Details Based On Status", notes = "Need Get Filter Kyc Details Based On Status")
	public synchronized ResponseEntity<String> filterKycStatus(
			@ApiParam(value = "Required KYC details", required = true) @RequestBody KycDTO kycDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		LOG.info("Inside /filter/kyc/status");

		try {

			HttpSession sessions = SessionCollector.find(kycDTO.getSessionId());
			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			// List<KycDTO> kycList =
			// userRegisterService.filterKyCStatus(kycDTO);

			KycDTO kycCount = userRegisterService.getKycCountByStatus(kycDTO);

			final List<KycDTO> kycList = userRegisterService.getFeedItemsForKycUsers(kycDTO.getKycStatus(),
					new PageRequest(kycDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC, "creationTime"));

			if (kycList != null && kycCount != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("kyc.list"));
				statusResponseDTO.setTotalElements(Math.toIntExact(kycCount.getTotalElements()));
				statusResponseDTO.setTotalPages(kycCount.getTotalPages());
				statusResponseDTO.setKycList(kycList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.not.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in KYC List : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/filter/kyc/username", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Get Filter Kyc Details Based On username", notes = "Need Get Filter Kyc Details Based On username")
	public synchronized ResponseEntity<String> filterKycUserName(
			@ApiParam(value = "Required KYC details", required = true) @RequestBody KycDTO kycDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		LOG.info("Inside /filter/kyc/username");

		try {

			HttpSession sessions = SessionCollector.find(kycDTO.getSessionId());
			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			KycDTO kycCount = userRegisterService.getKycCountByUsername(kycDTO);

			List<KycDTO> kycList = userRegisterService.getFeedItemsForKycUsers(kycDTO.getFullName(),
					new PageRequest(kycDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC, "creationTime"));

			if (kycList != null && kycCount != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("kyc.list"));
				statusResponseDTO.setTotalElements(Math.toIntExact(kycCount.getTotalElements()));
				statusResponseDTO.setTotalPages(kycCount.getTotalPages());
				statusResponseDTO.setKycList(kycList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.not.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in KYC List : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/filter/kyc", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Get Filter Kyc Details Based On emailid", notes = "Need Get Filter Kyc Details Based On emailid")
	public synchronized ResponseEntity<String> filterKycEmailId(
			@ApiParam(value = "Required KYC details", required = true) @RequestBody KycDTO kycDTO) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		LOG.info("Inside /filter/kyc");

		try {

			HttpSession sessions = SessionCollector.find(kycDTO.getSessionId());
			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			KycDTO kycCount = userRegisterService.getKycCountByEmailid(kycDTO);
			List<KycDTO> kycList = userRegisterService.getFeedItemsForKycUsersByEmail(kycDTO.getFilterKyc(),
					new PageRequest(kycDTO.getPageNum(), ACTIVITY_FEED_PAGE_SIZE, Sort.Direction.DESC, "creationTime"));

			if (kycList != null && kycCount != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("kyc.list"));
				statusResponseDTO.setTotalElements((int) kycCount.getTotalElements());
				statusResponseDTO.setTotalPages(kycCount.getTotalPages());
				statusResponseDTO.setKycList(kycList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("kyc.not.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in KYC List : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
}
