package com.biocoin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biocoin.dto.StatusResponseDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.dto.UserRegisterDTO;
import com.biocoin.model.RegisterInfo;
import com.biocoin.service.ExpirationDataService;
import com.biocoin.service.TransactionHistoryService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.SessionCollector;
import com.google.gson.Gson;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/biocoin/api/biocoinuser")
// @Api(value="gffdgfd" description="dggdgdfgfd")
@CrossOrigin
public class UserActivitiesController {

	private static final Logger LOG = LoggerFactory.getLogger(UserActivitiesController.class);

	@Autowired
	private Environment env;
	@Autowired
	private BioCoinUtils biocoinutils;
	@Autowired
	private TransactionHistoryService transactionHistoryService;
	@Autowired
	private UserRegisterService userregisterservice;
	@Autowired
	private ExpirationDataService expirationDataService;

	@CrossOrigin
	@RequestMapping(value = "/forgot/password", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Forgot password", notes = "Need to get forgot password")
	public synchronized ResponseEntity<String> forgotPassword(
			@ApiParam(value = "Required User details", required = true) @RequestBody UserRegisterDTO userregisterdto,
			HttpServletRequest request) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		try {
			boolean isValidEmailId = biocoinutils.validateEmail(userregisterdto.getEmailId());

			if (!isValidEmailId) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("valid.emailId"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			RegisterInfo isEmailExit = userregisterservice.isAccountExistCheckByEmailId(userregisterdto.getEmailId());

			if (isEmailExit == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("email.notexist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isEmailSend = expirationDataService.isSendEmail(userregisterdto);
			if (!isEmailSend) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("email.sending.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("password.update"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			// LOG.error("Problem in forgotpassword : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/forgot/password/linkVerification", method = RequestMethod.POST, produces = {
			"application/json" })
	@ApiOperation(value = "Forgot password", notes = "Need to get forgot password")
	public synchronized ResponseEntity<String> forgotPasswordLinkVerification(
			@ApiParam(value = "Required User details", required = true) @RequestBody UserRegisterDTO userregisterdto) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			boolean isResetPasswordLinkVerified = biocoinutils.isResetPasswordLinkVerified(userregisterdto);
			if (!isResetPasswordLinkVerified) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("link.verified"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isTime = biocoinutils.validateTime(userregisterdto);
			if (!isTime) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("time.validate.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("password.reset.update"));
			statusResponseDTO.setForgotPasswordInfo(userregisterdto);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			// LOG.error("Problem in forgotpassword : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/forgot/password/reset", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Forgot password Reset", notes = "Need to get forgot password")
	public synchronized ResponseEntity<String> forgotPasswordReset(
			@ApiParam(value = "Required User details", required = true) @RequestBody UserRegisterDTO userregisterdto) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			boolean isValidPassword = biocoinutils.validateResetPassword(userregisterdto);
			if (!isValidPassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("change.validate.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

			boolean changePassword = biocoinutils.isResetPassword(userregisterdto);
			if (!changePassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("change.password.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("change.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// LOG.error("Problem in forgotpassword : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/reset/password", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Reset password", notes = "Need to set new password")
	public synchronized ResponseEntity<String> resetPassword(
			@ApiParam(value = "Reset password", required = true) @RequestBody UserRegisterDTO userregisterdto,
			HttpServletRequest request) throws Exception {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();

		try {
			HttpSession session = SessionCollector.find(userregisterdto.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValidPassword = biocoinutils.validateResetPassword(userregisterdto);
			if (!isValidPassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("change.validate.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			}

			boolean isOldPassword = userregisterservice.isOldPassword(userregisterdto);
			if (!isOldPassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.old.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean changePassword = userregisterservice.isChangePassword(userregisterdto);
			if (!changePassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("change.password.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("change.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// LOG.error("Problem in ResetPassword : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

//	@CrossOrigin
//	@RequestMapping(value = "/transaction/history", method = RequestMethod.POST, produces = { "application/json" })
//	@ApiOperation(value = "Transaction history", notes = "Get transaction history")
//	public synchronized ResponseEntity<String> transactionHistory(
//			@ApiParam(value = "Transaction history", required = true) @RequestBody TokenDTO tokenDTO,
//			HttpSession session) {
//		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
//		try {
//			session = SessionCollector.find(tokenDTO.getSessionId());
//			if (session == null) {
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(env.getProperty("session.expired"));
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//			}
//			List<TokenDTO> trasactionHistory = transactionHistoryService.txHistory(tokenDTO);
//			// List<TransactionHistory> trasactionHistory =
//			// transactionHistoryService.getTransactionHistory(tokenDTO.getUserId());
//
//			if (!statusResponseDTO.equals(null)) {
//				statusResponseDTO.setStatus(env.getProperty("success"));
//				statusResponseDTO.setMessage(env.getProperty("get.transactionhistory.success"));
//				// statusResponseDTO.setTxList(trasactionHistory);
//				statusResponseDTO.setTransactionHistory(trasactionHistory);
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
//			} else {
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(env.getProperty("get.transactionhistory.failure"));
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			LOG.error("Problem in tokencreation  : ", e);
//			statusResponseDTO.setStatus(env.getProperty("failure"));
//			statusResponseDTO.setMessage(env.getProperty("server.problem"));
//			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//		}
//	}
	
	@CrossOrigin
	@RequestMapping(value = "/transaction/history", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "transaction History", notes = "Need to show Transaction History")
	public synchronized ResponseEntity<String> transactionList(
			@ApiParam(value = "transaction History", required = true) @RequestBody TokenDTO tokenDTO, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			if(tokenDTO.getPageNum() != null && tokenDTO.getPageSize() != null) {
				
				List<TokenDTO> transactionLists = transactionHistoryService.transactionHistory(tokenDTO,
						tokenDTO.getEthWalletAddress());
				if (transactionLists == null) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("history.failed"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("history.success"));
				//statusResponseDTO.setTotalPages(tokenDTO.getTotalPages());
				//statusResponseDTO.setTotalElements(tokenDTO.getTotalElements());
				statusResponseDTO.setTransactionHistoryInfo(transactionLists);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				
			} else {
				List<TokenDTO> transactionLists = transactionHistoryService.transactionHistory(tokenDTO,
						tokenDTO.getEthWalletAddress());
				if (transactionLists == null) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("history.failed"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("history.success"));
				statusResponseDTO.setTotalPages(tokenDTO.getTotalPages());
				statusResponseDTO.setTotalElements(tokenDTO.getTotalElements());
				statusResponseDTO.setTransactionHistoryInfo(transactionLists);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in token transaction history  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/transaction/history/filter", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "transaction History filter", notes = "Need to show Transaction History filter")
	public synchronized ResponseEntity<String> transactionFilter(
			@ApiParam(value = "transaction History filter", required = true) @RequestBody TokenDTO tokenDTO, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			List<TokenDTO> transactionLists = transactionHistoryService.transactionHistoryFilter(tokenDTO);
			if (transactionLists == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("history.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("history.success"));
			statusResponseDTO.setTotalPages(tokenDTO.getTotalPages());
			statusResponseDTO.setTotalElements(tokenDTO.getTotalElements());
			statusResponseDTO.setTransactionHistoryInfo(transactionLists);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in token transaction history  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	

	@CrossOrigin
	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "User Logout", notes = "Need to Logout")
	public synchronized ResponseEntity<String> logoutUser(
			@ApiParam(value = "User Logout", required = true) @RequestBody UserRegisterDTO userRegisterDTO,
			HttpServletRequest request, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			session = SessionCollector.find(userRegisterDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean islogout = biocoinutils.logoutParam(userRegisterDTO);
			if (!islogout) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("logout.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("logout.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Logout  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}	
}
