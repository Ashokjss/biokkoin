package com.biocoin.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.biocoin.dto.PaypalDTO;
import com.biocoin.dto.StatusResponseDTO;
import com.biocoin.dto.TokenDTO;
import com.biocoin.model.Token_info;
import com.biocoin.service.BiocoinValueService;
import com.biocoin.service.TokenService;
import com.biocoin.service.UserRegisterService;
import com.biocoin.utils.BioCoinUtils;
import com.biocoin.utils.CurrentValueUtils;
import com.biocoin.utils.SessionCollector;
import com.google.gson.Gson;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.biocoin.utils.BitCoinUtils;

@RestController
@RequestMapping(value = "/biocoin/api/biocoinuser")
public class TokenActivitiesController {

	static final Logger LOG = LoggerFactory.getLogger(TokenActivitiesController.class);

	@Autowired
	private Environment env;
	@Autowired
	BioCoinUtils bioCoinUtils;
	@Autowired
	TokenService tokenService;
	@Autowired
	UserRegisterService userregisterservice;
	@Autowired
	CurrentValueUtils currentValueUtils;
	@Autowired
	BiocoinValueService biocoinValueService;
	@Autowired
	private BioCoinUtils biocoinutils;
	@Autowired
	private BitCoinUtils bitCoinUtils;

	@CrossOrigin
	@RequestMapping(value = "/contributeInCrowdsale", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Contribute in Crowdsale", notes = "Need to Contribute in crowdsale")
	public synchronized ResponseEntity<String> contribution(
			@ApiParam(value = "Contribute in Crowdsale", required = true) @RequestBody TokenDTO tokenDTO,
			HttpServletRequest request) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValid = bioCoinUtils.validateTokenParam(tokenDTO);
			if (!isValid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("validate.token.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			/* This Is Ethereum */
			if (tokenDTO.getSelectTransactionType().equals("ETH")) {
				boolean valid = bioCoinUtils.validatePasswordPrams(tokenDTO);
				if (!valid) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("walletpassword.incorrect"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}

				boolean isToken = tokenService.validAmount(tokenDTO);
				if (!isToken) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("balance.insufficient"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}
			/* This Is Bitcoin */
			if (tokenDTO.getSelectTransactionType().equals("BTC")) {
				boolean isBalance = bioCoinUtils.bitcoinBalanceCheck(tokenDTO);

				if (!isBalance) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("insufficent.fund"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}

			String isValidTokenBal = tokenService.isValidTokenBalForTokenTransfer(tokenDTO);
			if (isValidTokenBal != "success") {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(isValidTokenBal);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			Token_info tokenObj = tokenService.findById(1);

			Token_info token = tokenService.getCountCurrentDateBetweenIcoDate(tokenObj.getStartDate(),
					tokenObj.getEndDate());

			if (token == null) {
				if ((new Date()).before(tokenObj.getStartDate())) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage("ICO Still Not Start...!");
				}
				if ((new Date()).after(tokenObj.getEndDate())) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage("ICO End...!");
				}

				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

			boolean isContribute = bioCoinUtils.contributeToken(tokenDTO);
			if (!isContribute) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("contribute.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("token.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Token Transfer in crowdsale: ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/delete/token", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Token Burn", notes = "Need to Burn token")
	public synchronized ResponseEntity<String> cancelToken(
			@ApiParam(value = "Token Burn", required = true) @RequestBody TokenDTO tokenDTO,
			HttpServletRequest request) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isPasswordexist = bioCoinUtils.isPasswordexist(tokenDTO);
			if (!isPasswordexist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("wallet.password.incorrect"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String tokenBalanceValidation = bioCoinUtils.tokenAmountValidationForBurn(tokenDTO);
			if (!tokenBalanceValidation.equalsIgnoreCase(env.getProperty("token.balance"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(tokenBalanceValidation);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String cancelToken = tokenService.burnToken(tokenDTO);
			if (!cancelToken.equalsIgnoreCase(env.getProperty("token.burn.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(cancelToken);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(cancelToken);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Token Burn: ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/mint/token", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Mint Token", notes = "Need to Mint tokens")
	public synchronized ResponseEntity<String> mintToken(
			@ApiParam(value = "Mint Token", required = true) @RequestBody TokenDTO tokenDTO,
			HttpServletRequest request) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValidateEthAddress = biocoinutils.isValidateEthAddress(tokenDTO);
			if (!isValidateEthAddress) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("invalid.eth.address"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isPasswordexist = bioCoinUtils.isPasswordexist(tokenDTO);
			if (!isPasswordexist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("wallet.password.incorrect"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String mintToken = tokenService.mintToken(tokenDTO);
			if (!mintToken.equalsIgnoreCase(env.getProperty("token.mint.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(mintToken);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(mintToken);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Mint Token: ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/balance/users", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Finding Balance of token , ether, bitcoin", notes = "Need to find the balance of token, ether, bitcoin")
	public synchronized ResponseEntity<String> Balance(
			@ApiParam(value = "Finding Balance of token , ether, bitcoin", required = true) @RequestBody TokenDTO tokenDTO) {
		StatusResponseDTO statusResponseDto = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}

			/* Ether Balance */
			String etherBalance = bioCoinUtils.etherbalance(tokenDTO);
			if (!etherBalance.equalsIgnoreCase(env.getProperty("ether.balance.success"))) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(etherBalance);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}
			
			/* Token Balance */
			String tokenBalance = bioCoinUtils.tokenbalance(tokenDTO);
			if (!tokenBalance.equalsIgnoreCase(env.getProperty("token.balance.success"))) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(tokenBalance);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}
			
			/* Bitcoin Balance */
			BigDecimal btcBalance = userregisterservice.getBtcBalance(tokenDTO);
			System.out.println("btcBalance &&&&&&&&&&&&&&&&&===>>" + btcBalance);
			if (btcBalance != null) {
				tokenDTO.setBitcoinBalance(btcBalance);
			} else {
				tokenDTO.setBitcoinBalance(new BigDecimal(0.0));
			}
			
			
			
			statusResponseDto.setStatus(env.getProperty("success"));
			statusResponseDto.setMessage(env.getProperty("balance.message"));
			statusResponseDto.setUserBalanceInfo(tokenDTO);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			statusResponseDto.setStatus(env.getProperty("failure"));
			statusResponseDto.setMessage(env.getProperty("balance.server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/admin/dashboard/details", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "check Admin wallet params", notes = "Finding admin dashboard view")
	public synchronized ResponseEntity<String> adminView(
			@ApiParam(value = "check Admin wallet params", required = true) @RequestBody TokenDTO tokenDTO) {
		StatusResponseDTO statusResponseDto = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}

			/* Ether Balance */
			String etherBalance = bioCoinUtils.etherbalance(tokenDTO);
			if (!etherBalance.equalsIgnoreCase(env.getProperty("ether.balance.success"))) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(etherBalance);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}
			
			boolean isLogin = bitCoinUtils.loginCrypto1(tokenDTO);
			if (!isLogin) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(env.getProperty("login.crypto.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}
			
			/* Token Balance */
			String tokenBalance = bioCoinUtils.tokenbalanceForAdmin(tokenDTO);
			if (!tokenBalance.equalsIgnoreCase(env.getProperty("token.balance.success"))) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(tokenBalance);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}
			
			/*	 Bitcoin Balance */
			BigDecimal btcBalance = userregisterservice.getBtcBalance(tokenDTO);
			System.out.println("btcBalance : " + btcBalance);
			if (btcBalance != null) {
				tokenDTO.setBitcoinBalance(btcBalance);
			} else {
				tokenDTO.setBitcoinBalance(new BigDecimal(0.0));
			}
			
			/* Dashboard Details */
			String dashboardDetails = bioCoinUtils.adminDashboardDetails(tokenDTO);
			if (!dashboardDetails.equalsIgnoreCase(env.getProperty("balance.message"))) {
				statusResponseDto.setStatus(env.getProperty("failure"));
				statusResponseDto.setMessage(dashboardDetails);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.PARTIAL_CONTENT);
			}
			
			statusResponseDto.setStatus(env.getProperty("success"));
			statusResponseDto.setMessage(env.getProperty("balance.message"));
			statusResponseDto.setAdminBalanceInfo(tokenDTO);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			statusResponseDto.setStatus(env.getProperty("failure"));
			statusResponseDto.setMessage(env.getProperty("balance.server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDto), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/manual/token/transfer/user", method = RequestMethod.POST, produces = {
			"application/json" })
	@ApiOperation(value = "Manual Transfer Token to user", notes = "Need to Transfer Token to user")
	public synchronized ResponseEntity<String> tokenTransferToUser(
			@ApiParam(value = "Transfer Token to user", required = true) @RequestBody TokenDTO tokenDTO,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession sessions = SessionCollector.find(tokenDTO.getSessionId());
			if (sessions == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isValidateTokenName = biocoinutils.isValidateEthAddress(tokenDTO);
			if (!isValidateTokenName) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("valid.tokenAddress"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isSameAddress = biocoinutils.isSameaddress(tokenDTO);
			if (isSameAddress) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("Same.wallet.address"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean isSamePassword = biocoinutils.isSamePassword(tokenDTO);
			if (!isSamePassword) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("same.password"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			boolean etherValidation = biocoinutils.etherValidation(tokenDTO);
			if (!etherValidation) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("ether.balance.valid"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			String tokenAmountValidation = biocoinutils.tokenAmountValidationForUser(tokenDTO);
			if (!tokenAmountValidation.equalsIgnoreCase(env.getProperty("token.balance"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(tokenAmountValidation);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			// boolean tokenAmountValidate =
			// biocoinutils.tokenAmountValidationForUserForVesting(tokenDTO);
			// if (!tokenAmountValidate) {
			// statusResponseDTO.setStatus(env.getProperty("failure"));
			// statusResponseDTO.setMessage(env.getProperty("token.amount.validation.vesting"));
			// return new ResponseEntity<String>(new
			// Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			// }

//			String validatePersonsType = tokenService.validateTypeOfPersons(tokenDTO);
//			if (!validatePersonsType.equalsIgnoreCase(env.getProperty("valid.typeofverification"))) {
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(validatePersonsType);
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//			}

			String isValid = tokenService.manualTokenTransferToUsers(tokenDTO);
			if (isValid == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("token.transfer.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("token.transfer.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOG.error("Problem in User Token Transfer : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/purchase/tokens", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "User Purchase Tokens", notes = "Need to Purchase Tokens")
	public synchronized ResponseEntity<String> purchaseTokens(
			@ApiParam(value = "User Purchase Tokens", required = true) @RequestBody TokenDTO tokenDTO,
			HttpServletRequest request, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			if(tokenDTO.getTypeOfPurchase().equalsIgnoreCase("ETH")) {
				boolean isSamePassword = biocoinutils.isSamePassword(tokenDTO);
				if (!isSamePassword) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("same.password"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}
			
			String isValids = biocoinutils.validateInputsForPurchase(tokenDTO);
			if (!isValids.equalsIgnoreCase("Validation success")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(isValids);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String amount = biocoinutils.balanceCheckForCryptoInPurchaseCoins(tokenDTO);
			
			if (!amount.equalsIgnoreCase("You are having sufficient balance")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(amount);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String tokenAmountValidation = biocoinValueService.tokenAmountValidationForPurchase(tokenDTO);
			if (!tokenAmountValidation.equals(env.getProperty("token.amount.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(tokenAmountValidation);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			/*String amount = biocoinutils.purchaseTokenEtherValidation(tokenDTO);
			if(!amount.equalsIgnoreCase("You are having sufficient ETH")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(amount);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}*/
			
			if (!tokenDTO.getTypeOfPurchase().equalsIgnoreCase("ETH")) {
				boolean isLogin = bitCoinUtils.loginCrypto1(tokenDTO);
				if (!isLogin) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("login.crypto.failed"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}
			
			boolean isValid = biocoinValueService.userPurchaseTokens(tokenDTO);
			if (!isValid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("token.purchase.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("token.purchase.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Purchase Tokens  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/purchase/tokens/create/payment/Paypal", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "User Purchase Tokens paypal", notes = "Need to Purchase Tokens paypal")
	public synchronized ResponseEntity<String> purchaseTokensViaPaymentGateway(
			@ApiParam(value = "User Purchase Tokens payments gateway", required = true) @RequestBody PaypalDTO paypalDTO,
			HttpServletRequest request, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {			
			session = SessionCollector.find(paypalDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String isValids = biocoinutils.validateInputsForPurchase(paypalDTO);
			if (!isValids.equalsIgnoreCase("Validation success")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(isValids);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String amount = biocoinutils.validateTheAmountOfUSDAndINR(paypalDTO);
			if(amount != null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(amount);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String tokenAmountValidation = biocoinValueService.tokenAmountValidationForPurchaseForPayPal(paypalDTO);
			if (!tokenAmountValidation.equals(env.getProperty("token.amount.success"))) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(tokenAmountValidation);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String accessTokens = biocoinValueService.oauthToken(paypalDTO);
			if (accessTokens == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("oauth.not.generated"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			JSONObject data = biocoinValueService.userPurchaseTokensPaymentGateway(paypalDTO);
			if (data == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("token.purchase.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("token.purchase.success"));
			statusResponseDTO.setData(data);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Purchase Tokens via payment gateways  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
			
	@CrossOrigin
	@RequestMapping(value = "/check/slabs/schedule", method = RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "User tokens schedule", notes = "Need to schedule")
	public synchronized ResponseEntity<String> schedule() {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			String checkDate = biocoinutils.checkShecdule();
			if (!checkDate.equalsIgnoreCase("Changes success")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(checkDate);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(checkDate);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in slap API's  : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/purchase/list", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	@ApiOperation(value = "Purchase List", notes = "Purchase List")
	public ResponseEntity<String> purchaseList(
			@ApiParam(value = "Purchase List", required = true) @RequestBody TokenDTO tokenDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			List<TokenDTO> purchaseList = biocoinValueService.purchaseList(tokenDTO);
			statusResponseDTO.setTotalPages(tokenDTO.getTotalPages());
			statusResponseDTO.setTotalElements(tokenDTO.getTotalElements());
			statusResponseDTO.setPurchaseList(purchaseList);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Purchase list : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/purchase/list/filter", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	@ApiOperation(value = "Purchase List filter", notes = "Purchase List filter")
	public ResponseEntity<String> purchaseListFilter(
			@ApiParam(value = "Purchase List filter", required = true) @RequestBody TokenDTO tokenDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			HttpSession session = SessionCollector.find(tokenDTO.getSessionId());
			if (session == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("session.expired"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

			List<TokenDTO> purchaseList = biocoinValueService.purchaseListFilter(tokenDTO);
			statusResponseDTO.setTotalPages(tokenDTO.getTotalPages());
			statusResponseDTO.setTotalElements(tokenDTO.getTotalElements());
			statusResponseDTO.setPurchaseList(purchaseList);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in Purchase list filter : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}
	
	@CrossOrigin
	@RequestMapping(value = "/purchase/token/current/rate", method = RequestMethod.POST)
	@ApiOperation(value = "Purchase Tokens current rate", notes = "Purchase Tokens current rate")
	public ResponseEntity<String> purchaseTokensCurrentRates(
			@ApiParam(value = "Purchase Tokens current rate", required = true) @RequestBody TokenDTO tokenDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			String rate = biocoinutils.currentRateForPurchaseToken(tokenDTO);
			if(rate.equalsIgnoreCase(null)) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(rate);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(rate);
			statusResponseDTO.setCurrentRateInfo(tokenDTO);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in getting current rate for purchase tokens : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}
	
//	@CrossOrigin
//	@RequestMapping(value = "/create/payment/Paypal", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
//			"application/json" })
//	@ApiOperation(value = "Purchase Tokens for create payment paypal", notes = "Purchase Tokens for create payment paypal")
//	public ResponseEntity<String> createPaymentFromPaypal(
//			@ApiParam(value = "Purchase Tokens for create payment paypal", required = true) @RequestBody PaypalDTO paypalDTO
//			)
//			throws Exception {
//		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
//		try {
//			
//			JSONObject rate = biocoinutils.createPayments(paypalDTO);
//			if(rate == null) {
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(env.getProperty("failure"));
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//			}
//			statusResponseDTO.setStatus(env.getProperty("success"));
//			statusResponseDTO.setMessage(env.getProperty("success"));
//			statusResponseDTO.setData(rate);
//			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();
//			LOG.error("Problem in getting redirect url from payapl for purchase tokens : ", e);
//			statusResponseDTO.setStatus(env.getProperty("failure"));
//			statusResponseDTO.setMessage(env.getProperty("server.problem"));
//			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
//		}
//
//	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/redirect/from/Paypal", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	@ApiOperation(value = "Purchase Tokens for paypal", notes = "Purchase Tokens for paypal")
	public ResponseEntity<String> redirectURLForPaypal(
			@ApiParam(value = "Purchase Tokens for paypal", required = true) @RequestBody PaypalDTO paypalDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			String accessTokens = biocoinValueService.oauthToken(paypalDTO);
			if (accessTokens == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("oauth.not.generated"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			String rate = biocoinutils.redirectURL(paypalDTO);
			if(rate.equalsIgnoreCase("Payment Failed")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(rate);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(rate);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in getting redirect url from payapl for purchase tokens : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}
	
	@CrossOrigin
	@RequestMapping(value = "/redirect/from/Paypal/cancel/payments", method = RequestMethod.POST, consumes = { "application/json" }, produces = {
			"application/json" })
	@ApiOperation(value = "Purchase Tokens for paypal", notes = "Purchase Tokens for paypal cancel payments")
	public ResponseEntity<String> redirectURLForPaypalCancelPayments(
			@ApiParam(value = "Purchase Tokens for paypal cancel payments", required = true) @RequestBody PaypalDTO paypalDTO)
			throws Exception {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			String opt = biocoinutils.redirectURLForCancelPayments(paypalDTO);
			if(!opt.equalsIgnoreCase("Payement cancelled successfully")) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(opt);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
						
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(opt);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Problem in getting redirect url from payapl cancel payments for purchase tokens : ", e);
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.CONFLICT);
		}

	}
}
