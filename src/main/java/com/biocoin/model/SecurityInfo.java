package com.biocoin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "USER_SECURITY_INFO")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SecurityInfo implements Serializable {
		private static final long serialVersionUID = 1L;
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;

		@Column(name = "Email_Id")
		@NotNull
		private String emailId;
		
		@Column(name = "created_Time")
		private Date createdTime;
		
		@Column(name="secured_Key")
		private Integer securedKey;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public Date getCreatedTime() {
			return createdTime;
		}

		public void setCreatedTime(Date createdTime) {
			this.createdTime = createdTime;
		}

		public Integer getSecuredKey() {
			return securedKey;
		}

		public void setSecuredKey(Integer securedKey) {
			this.securedKey = securedKey;
		}
	

}
