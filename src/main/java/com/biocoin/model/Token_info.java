package com.biocoin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@Table(name = "token_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Token_info implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "start_Date")
	@NotNull
	private Date startDate;
	
	@Column(name = "end_Date")
	@NotNull
	private Date endDate;
	
	@Column(name = "total_Token")
	private Double totalToken;
	
	@Column(name = "initial_Token")
	private Double initialToken;
	
	@Column(name = "total_available_Tokens")
	private Double totalAvailableTokens;
	
	@Column(name = "ico_available_Tokens")
	private Double icoAvailableTokens;
	
	@Column(name = "manual_available_Tokens")
	private Double manualAvailableTokens;
	
	@Column(name = "ico_sold_Tokens")
	private Double icoSoldTokens;
	
	@Column(name = "totalsold_Tokens")
	private Double totalSoldTokens;
	
	@Column(name = "burned_Tokens")
	private Double burnedTokens;
	
	@Column(name = "mint_Tokens")
	private Double mintTokens;
	
	@Column(name = "token_values_in_usd")
	private String tokenValuesInUSD;
	
	@Column(name = "locked_tokens_end_date")
	private Date lockedTokensEndDate;

	public Date getLockedTokensEndDate() {
		return lockedTokensEndDate;
	}

	public void setLockedTokensEndDate(Date lockedTokensEndDate) {
		this.lockedTokensEndDate = lockedTokensEndDate;
	}

	public Double getTotalAvailableTokens() {
		return totalAvailableTokens;
	}

	public void setTotalAvailableTokens(Double totalAvailableTokens) {
		this.totalAvailableTokens = totalAvailableTokens;
	}

	public Double getIcoAvailableTokens() {
		return icoAvailableTokens;
	}

	public void setIcoAvailableTokens(Double icoAvailableTokens) {
		this.icoAvailableTokens = icoAvailableTokens;
	}

	public Double getManualAvailableTokens() {
		return manualAvailableTokens;
	}

	public void setManualAvailableTokens(Double manualAvailableTokens) {
		this.manualAvailableTokens = manualAvailableTokens;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getTotalToken() {
		return totalToken;
	}

	public void setTotalToken(Double totalToken) {
		this.totalToken = totalToken;
	}

	public Double getInitialToken() {
		return initialToken;
	}

	public void setInitialToken(Double initialToken) {
		this.initialToken = initialToken;
	}

	public Double getIcoSoldTokens() {
		return icoSoldTokens;
	}

	public void setIcoSoldTokens(Double icoSoldTokens) {
		this.icoSoldTokens = icoSoldTokens;
	}

	public Double getTotalSoldTokens() {
		return totalSoldTokens;
	}

	public void setTotalSoldTokens(Double totalSoldTokens) {
		this.totalSoldTokens = totalSoldTokens;
	}

	public Double getBurnedTokens() {
		return burnedTokens;
	}

	public void setBurnedTokens(Double burnedTokens) {
		this.burnedTokens = burnedTokens;
	}

	public Double getMintTokens() {
		return mintTokens;
	}

	public void setMintTokens(Double mintTokens) {
		this.mintTokens = mintTokens;
	}

	public String getTokenValuesInUSD() {
		return tokenValuesInUSD;
	}

	public void setTokenValuesInUSD(String tokenValuesInUSD) {
		this.tokenValuesInUSD = tokenValuesInUSD;
	}
	
}
