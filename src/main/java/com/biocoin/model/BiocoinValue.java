package com.biocoin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Biocoin_Value")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BiocoinValue {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "value_symbal")
	@NotNull
	private String value_symbal;

	@Column(name = "biocoin_value")
	@NotNull
	private double biocoin_value;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue_symbal() {
		return value_symbal;
	}

	public void setValue_symbal(String value_symbal) {
		this.value_symbal = value_symbal;
	}

	public double getBiocoin_value() {
		return biocoin_value;
	}

	public void setBiocoin_value(double biocoin_value) {
		this.biocoin_value = biocoin_value;
	}
	
	
	
}
