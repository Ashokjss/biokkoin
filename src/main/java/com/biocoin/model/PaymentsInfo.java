package com.biocoin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "payments_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class PaymentsInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "currency")
	@NotNull
	private String currency;
	
	@Column(name = "amount")
	@NotNull
	private String amount;
	
	@Column(name = "create_time")
	@NotNull
	private Date create_time;
	
	@Column(name = "payer_id")
	private String payer_id;
	
	@Column(name = "payment_id")
	private String paymentID;
	
	@Column(name = "email_id")
	@NotNull
	private String emailId;
	
	@Column(name = "token_amount")
	@NotNull
	private Double tokenAmount;
	
	@Column(name = "status")
	@NotNull
	private String status;
	
	@Column(name = "eth_address")
	@NotNull
	private String ethAddress;
	

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getEthAddress() {
		return ethAddress;
	}

	public void setEthAddress(String ethAddress) {
		this.ethAddress = ethAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getPayer_id() {
		return payer_id;
	}

	public void setPayer_id(String payer_id) {
		this.payer_id = payer_id;
	}

	public String getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Double getTokenAmount() {
		return tokenAmount;
	}

	public void setTokenAmount(Double tokenAmount) {
		this.tokenAmount = tokenAmount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
