package com.biocoin.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "expiration_data_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)


public class ExpirationDataInfo {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "email_id")
	@NotNull
	private String emailId;
	
	@Column(name = "token")
	@NotNull
	private String token;
	
	@Column(name = "expired_date")
	@NotNull
	private Date expiredDate;
	
	@Column(name = "token_status")
	@NotNull
	private boolean tokenStatus;
	


	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public boolean isTokenStatus() {
		return tokenStatus;
	}

	public void setTokenStatus(boolean tokenStatus) {
		this.tokenStatus = tokenStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
