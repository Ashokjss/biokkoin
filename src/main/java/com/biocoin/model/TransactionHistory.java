package com.biocoin.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "transaction_history")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
//	IDENTITY
	@Column(name = "from_address")
	@NotNull
	private String fromAddress;

	@Column(name = "to_address")
	@NotNull
	private String toAddress;

	@Column(name = "crypto_amount")
	@NotNull
	private BigDecimal cryptoAmount;

	@Column(name = "transaction_date")
	@NotNull
	private Date txDate;
	
	@Column(name = "payment_mode")
	private String paymentMode;
	
	@Column(name = "transaction_status")
	private String txstatus;
	
	@Column(name = "transfer_token")
	private Double transferToken;
	
	@Column(name = "sender_id")
	private Integer senderId;
	
	@Column(name = "receiver_id")
	private Integer receiverId;
	
	@Column(name = "type_of_verifications")
	@NotNull
	private String typeOfVerifications;
	
	@Column(name = "email_id")
	@NotNull
	private String emailId;
	
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName="id")
	private RegisterInfo registerInfo;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public BigDecimal getCryptoAmount() {
		return cryptoAmount;
	}

	public void setCryptoAmount(BigDecimal cryptoAmount) {
		this.cryptoAmount = cryptoAmount;
	}

	public Date getTxDate() {
		return txDate;
	}

	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getTxstatus() {
		return txstatus;
	}

	public void setTxstatus(String txstatus) {
		this.txstatus = txstatus;
	}

	public Double getTransferToken() {
		return transferToken;
	}

	public void setTransferToken(Double transferToken) {
		this.transferToken = transferToken;
	}

	public Integer getSenderId() {
		return senderId;
	}

	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

	public RegisterInfo getRegisterInfo() {
		return registerInfo;
	}

	public void setRegisterInfo(RegisterInfo registerInfo) {
		this.registerInfo = registerInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
