package com.biocoin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "token_allocation_topmanagements_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class TokenAllocationForTopManagementsInfo {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "email_id")
	private String emailId;
	
	@Column(name = "allocated_tokens")
	private Double allocatedTokens;

	@Column(name = "type_of_verifications")
	private String typeOfVerifications;
	
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getAllocatedTokens() {
		return allocatedTokens;
	}

	public void setAllocatedTokens(Double allocatedTokens) {
		this.allocatedTokens = allocatedTokens;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
