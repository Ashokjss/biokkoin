package com.biocoin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "hold_tokens_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class HoldTokensInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "email_id")
	@NotNull
	private String emailId;
	
	@Column(name = "locked_tokens")
	@NotNull
	private Double lockedTokens;
	
	@Column(name = "type_of_verifications")
	@NotNull
	private String typeOfVerifications;
	
	@Column(name = "period_end") 
	@NotNull
	private Date periodEnd;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Double getLockedTokens() {
		return lockedTokens;
	}

	public void setLockedTokens(Double lockedTokens) {
		this.lockedTokens = lockedTokens;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

	public Date getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
