package com.biocoin.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "register_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class RegisterInfo {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "first_name")
	@NotNull
	private String firstName;
	
	@Column(name = "last_name")
	@NotNull
	private String lastName;

	@Column(name = "mobile_no")
	@NotNull
	private String mobileno;
	
	@Column(name = "email_id")
	@NotNull
	private String emailId;

	@Column(name = "password")
	@NotNull
	private String password;

	@Column(name = "signup_date")
	@NotNull
	private Date signup_date;

	@Column(name = "eth_wallet_address")
	private String ethWalletAddress;

	@Column(name = "eth_wallet_password")
	private String ethWalletPassword;

	@Column(name = "user_type")
	@NotNull
	private String userType;
	
	@Column(name = "type_of_verifications")
	@NotNull
	private String typeOfVerifications;
	
	@Column(name = "token_balance")
	@NotNull
	private Double tokenBalance;
	
	@Column(name = "locked_token_balance")
	@NotNull
	private Double lockedTokenBalance;

	@Column(name = "gmailverify_status")
	@NotNull
	private int gmailstatus;

	@Column(name = "app_id")
	@NotNull
	private String appId;
	
	@Column(name = "device_type")
	@NotNull
	private String deviceType;
	
	@Column(name = "dummy_wallet_address")
	private String dummywalletaddress;

	@OneToMany(mappedBy = "registerInfo")
	private List<TransactionHistory> TransactionHistory;
	
	@Column(name = "burned_tokens")
	@NotNull
	private Double burnedTokens;

	@OneToMany(mappedBy = "registerInfo")
	private List<BurnedToken> BurnedToken;

	@OneToMany(mappedBy = "registerInfo")
	private List<MintedToken> MintedToken;
	

	@Column(name = "kyc_status")
	private Integer kycStatus;

	@Column(name = "btc_wallet_address")
	private String btcWalletAddress;

	@Column(name = "btc_wallet_password")
	private String btcWalletPassword;

	@Column(name = "btc_wallet_guid")
	private String btcWalletGuid;

	public String getBtcWalletGuid() {
		return btcWalletGuid;
	}

	public void setBtcWalletGuid(String btcWalletGuid) {
		this.btcWalletGuid = btcWalletGuid;
	}

	public String getBtcWalletAddress() {
		return btcWalletAddress;
	}

	public void setBtcWalletAddress(String btcWalletAddress) {
		this.btcWalletAddress = btcWalletAddress;
	}

	public String getBtcWalletPassword() {
		return btcWalletPassword;
	}

	public void setBtcWalletPassword(String btcWalletPassword) {
		this.btcWalletPassword = btcWalletPassword;
	}

	public Integer getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(Integer kycStatus) {
		this.kycStatus = kycStatus;
	}


	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getSignup_date() {
		return signup_date;
	}

	public void setSignup_date(Date signup_date) {
		this.signup_date = signup_date;
	}

	public String getEthWalletAddress() {
		return ethWalletAddress;
	}

	public void setEthWalletAddress(String ethWalletAddress) {
		this.ethWalletAddress = ethWalletAddress;
	}

	public String getEthWalletPassword() {
		return ethWalletPassword;
	}

	public void setEthWalletPassword(String ethWalletPassword) {
		this.ethWalletPassword = ethWalletPassword;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

	public Double getTokenBalance() {
		return tokenBalance;
	}

	public void setTokenBalance(Double tokenBalance) {
		this.tokenBalance = tokenBalance;
	}

	public Double getLockedTokenBalance() {
		return lockedTokenBalance;
	}

	public void setLockedTokenBalance(Double lockedTokenBalance) {
		this.lockedTokenBalance = lockedTokenBalance;
	}

	public int getGmailstatus() {
		return gmailstatus;
	}

	public void setGmailstatus(int gmailstatus) {
		this.gmailstatus = gmailstatus;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getDummywalletaddress() {
		return dummywalletaddress;
	}

	public void setDummywalletaddress(String dummywalletaddress) {
		this.dummywalletaddress = dummywalletaddress;
	}

	public List<TransactionHistory> getTransactionHistory() {
		return TransactionHistory;
	}

	public void setTransactionHistory(List<TransactionHistory> transactionHistory) {
		TransactionHistory = transactionHistory;
	}

	public Double getBurnedTokens() {
		return burnedTokens;
	}

	public void setBurnedTokens(Double burnedTokens) {
		this.burnedTokens = burnedTokens;
	}

	public List<BurnedToken> getBurnedToken() {
		return BurnedToken;
	}

	public void setBurnedToken(List<BurnedToken> burnedToken) {
		BurnedToken = burnedToken;
	}

	public List<MintedToken> getMintedToken() {
		return MintedToken;
	}

	public void setMintedToken(List<MintedToken> mintedToken) {
		MintedToken = mintedToken;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}