package com.biocoin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "purchase_token_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseTokenInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "email_id")
	@NotNull
	private String emailId;
	
	@Column(name = "purchase_date")
	@NotNull
	private Date purchasedDate;
	
	@Column(name = "ether_wallet_address")
	private String etherWalletAddress;
	
	@Column(name = "btc_wallet_address")
	private String btcWalletAddress;
	
	@Column(name = "purchase_tokens")
	@NotNull
	private Double purchaseTokens;
	
	@Column(name = "crypto_amount")
	private Double cryptoAmount;
	
	@Column(name = "type_purchase")
	@NotNull
	private String typeOfPurchase;
	
	@Column(name = "transfer_type")
	@NotNull
	private Integer transferType;
	
	@Column(name = "asynch_status")
	private String asynchStatus;
	
	@Column(name = "payment_id")
	private String paymentId;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getBtcWalletAddress() {
		return btcWalletAddress;
	}

	public void setBtcWalletAddress(String btcWalletAddress) {
		this.btcWalletAddress = btcWalletAddress;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getPurchasedDate() {
		return purchasedDate;
	}

	public void setPurchasedDate(Date purchasedDate) {
		this.purchasedDate = purchasedDate;
	}

	public String getEtherWalletAddress() {
		return etherWalletAddress;
	}

	public void setEtherWalletAddress(String etherWalletAddress) {
		this.etherWalletAddress = etherWalletAddress;
	}

	public Double getPurchaseTokens() {
		return purchaseTokens;
	}

	public void setPurchaseTokens(Double purchaseTokens) {
		this.purchaseTokens = purchaseTokens;
	}

	public Double getCryptoAmount() {
		return cryptoAmount;
	}

	public void setCryptoAmount(Double cryptoAmount) {
		this.cryptoAmount = cryptoAmount;
	}

	public String getTypeOfPurchase() {
		return typeOfPurchase;
	}

	public void setTypeOfPurchase(String typeOfPurchase) {
		this.typeOfPurchase = typeOfPurchase;
	}

	public Integer getTransferType() {
		return transferType;
	}

	public void setTransferType(Integer transferType) {
		this.transferType = transferType;
	}

	public String getAsynchStatus() {
		return asynchStatus;
	}

	public void setAsynchStatus(String asynchStatus) {
		this.asynchStatus = asynchStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
