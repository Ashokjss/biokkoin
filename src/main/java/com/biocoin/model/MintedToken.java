package com.biocoin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "Minted_Token")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class MintedToken {


	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "email_id")
	@NotNull
	private String emailId;
	
	@Column(name = "eth_wallet_address")
	private String mintedETHwalletaddress;
	
    @Column(name = "minted_tokens")
    private Double mintedtokens;
    
    @Column(name="date")
    private Date date;
    
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName="id")
	private RegisterInfo registerInfo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMintedETHwalletaddress() {
		return mintedETHwalletaddress;
	}

	public void setMintedETHwalletaddress(String mintedETHwalletaddress) {
		this.mintedETHwalletaddress = mintedETHwalletaddress;
	}

	public Double getMintedtokens() {
		return mintedtokens;
	}

	public void setMintedtokens(Double mintedtokens) {
		this.mintedtokens = mintedtokens;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public RegisterInfo getRegisterInfo() {
		return registerInfo;
	}

	public void setRegisterInfo(RegisterInfo registerInfo) {
		this.registerInfo = registerInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
