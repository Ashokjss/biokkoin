package com.biocoin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "ico_period_info")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ICOPeriodInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "ICOPeriodInfo [id=" + id + ", icoAllocationTokens=" + icoAllocationTokens + ", icoSoldTokens="
				+ icoSoldTokens + ", icoSoldTokens=" + icoSoldTokens + ", icoStatus=" + icoStatus + ", icoStart=" + icoStart + ", icoEnd=" + icoEnd + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "ico_allocation_tokens")
	@NotNull
	private Double icoAllocationTokens;
	
	@Column(name = "ico_sold_tokens")
	@NotNull
	private Double icoSoldTokens;
	
	@Column(name = "ico_available_tokens")
	@NotNull
	private Double icoAvailableTokens;
	
	@Column(name = "ico_status")
	@NotNull
	private Integer icoStatus;
	
	@Column(name = "ico_start")
	@NotNull
	private Date icoStart;
	
	@Column(name = "ico_end")
	@NotNull
	private Date icoEnd;

	public Double getIcoSoldTokens() {
		return icoSoldTokens;
	}

	public void setIcoSoldTokens(Double icoSoldTokens) {
		this.icoSoldTokens = icoSoldTokens;
	}

	public Double getIcoAvailableTokens() {
		return icoAvailableTokens;
	}

	public void setIcoAvailableTokens(Double icoAvailableTokens) {
		this.icoAvailableTokens = icoAvailableTokens;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getIcoAllocationTokens() {
		return icoAllocationTokens;
	}

	public void setIcoAllocationTokens(Double icoAllocationTokens) {
		this.icoAllocationTokens = icoAllocationTokens;
	}

	public Integer getIcoStatus() {
		return icoStatus;
	}

	public void setIcoStatus(Integer icoStatus) {
		this.icoStatus = icoStatus;
	}

	public Date getIcoStart() {
		return icoStart;
	}

	public void setIcoStart(Date icoStart) {
		this.icoStart = icoStart;
	}

	public Date getIcoEnd() {
		return icoEnd;
	}

	public void setIcoEnd(Date icoEnd) {
		this.icoEnd = icoEnd;
	}
}
