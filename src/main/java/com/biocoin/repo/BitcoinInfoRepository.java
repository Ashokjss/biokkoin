package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.BitcoinInfo;

public interface BitcoinInfoRepository extends CrudRepository<BitcoinInfo, Integer> {

	public BitcoinInfo findByEmailId(String emialId);

}