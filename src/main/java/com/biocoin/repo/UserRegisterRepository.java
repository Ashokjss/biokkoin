package com.biocoin.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.biocoin.model.RegisterInfo;

@Service
public interface UserRegisterRepository extends JpaRepository<RegisterInfo, Integer> {

	public RegisterInfo findEquocoinUserInfoByEmailIdAndPassword(String trim, String password);

	public Integer countUvcoinUserInfoByEmailIdIgnoreCase(String trim);

	public RegisterInfo findUvcoinUserInfoByEmailId(String email);

	public RegisterInfo findUVcoinUserInfoByEmailIdAndPassword(String trim, String password);

	public RegisterInfo findRegisterInfoByEmailIdIgnoreCase(String emailid);

	public RegisterInfo findRegisterInfoByEthWalletAddress(String walletAddress);

	public RegisterInfo findRegisterInfoByDummywalletaddress(String walletAddress);

	public List<RegisterInfo> findByUserType(String userType);

	public Integer countRegisterInfoByUserType(String userType);
	
	public Page<RegisterInfo> findByUserType(String userType, Pageable pg);
	
	@Query("select r from RegisterInfo r where r.userType='U' and (upper(r.firstName) like concat('%', upper(?1), '%') or upper(r.lastName) like concat('%', upper(?2), '%') or upper(r.emailId) like concat('%', upper(?3), '%'))")
	public Page<RegisterInfo> findBynames(String value1, String value2, String value3, Pageable pg);
	
}
