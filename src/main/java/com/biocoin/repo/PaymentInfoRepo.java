package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.PaymentsInfo;

public interface PaymentInfoRepo extends CrudRepository<PaymentsInfo, Integer> {

	public PaymentsInfo findPaymentsInfoByPaymentID(String id);
	
}
