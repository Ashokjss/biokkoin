package com.biocoin.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.biocoin.model.TransactionHistory;

@Service
public interface TransactionInfoRepository extends JpaRepository<TransactionHistory, Integer> {

	public Page<TransactionHistory> findByFromAddressOrToAddressOrderByTxDateDesc(String etherwalletAddress1, String etherwalletAddress2, Pageable pg);

	public Page<TransactionHistory> findByFromAddressOrderByTxDateDesc(String etherwalletAddress, Pageable pg);
	
	public Page<TransactionHistory> findByToAddressOrderByTxDateDesc(String etherwalletAddress, Pageable pg);
	
	public List<TransactionHistory> findTop3ByFromAddressOrToAddressOrderByTxDateDesc(String etherwalletAddress, String etherwalletAddress2);

	public List<TransactionHistory> findByFromAddressOrToAddressOrderByTxDateDesc(String etherwalletAddress1,String etherwalletAddress2);
	
	@Query("select p from TransactionHistory p where (upper(p.fromAddress) = upper(?1) or upper(p.toAddress) = upper(?2)) and (upper(p.txstatus) like concat('%', upper(?3), '%')) ")
	Page<TransactionHistory> findByTxstatusLike(String etherwalletAddress1, String etherwalletAddress2, String searchTxt, Pageable pg);
	
	@Query("select r from TransactionHistory r where (upper(r.fromAddress) = upper(?1) or upper(r.toAddress) = upper(?2)) and (upper(r.fromAddress) like concat('%', upper(?3), '%') or upper(r.toAddress) like concat('%', upper(?4), '%'))")
	public Page<TransactionHistory> findByWalletAddress(String etherwalletAddress1, String etherwalletAddress2, String searchTxt1, String searchTxt2, Pageable pg);

}
