package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.MintedToken;

public interface MintTokenInfoRepo extends CrudRepository<MintedToken, Integer> {

}
