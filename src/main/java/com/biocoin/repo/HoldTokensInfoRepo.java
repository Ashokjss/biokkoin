package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.HoldTokensInfo;

public interface HoldTokensInfoRepo extends CrudRepository<HoldTokensInfo, Integer> {

	public HoldTokensInfo findByEmailId(String emailId);
	
}
