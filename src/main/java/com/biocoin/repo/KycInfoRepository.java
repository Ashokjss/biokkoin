package com.biocoin.repo;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.biocoin.model.KycInfo;

public interface KycInfoRepository extends CrudRepository<KycInfo, Integer> {

	public KycInfo findByEmailId(String mailId);

	public KycInfo findKycInfoById(Integer id);

	public List<KycInfo> findAllByOrderByIdDesc();

	public List<KycInfo> findByKycStatus(int kycStatus);

	@Query("select ua from KycInfo ua")
	public List<KycInfo> getAllUsersKycDetails(Pageable pageRequest);

	public List<KycInfo> findByKycStatus(Integer kycStatus, Pageable pageRequest);

	public List<KycInfo> findByFullName(String fullName, Pageable pageRequest);

	@Query("select ki from KycInfo ki")
	public Page<KycInfo> getAllUsersKycDet(Pageable pageRequest);

	@Query("select ki from KycInfo ki where ki.kycStatus = :kycStatus")
	public Page<KycInfo> getAllUsersByKycStatus(@Param("kycStatus") Integer kycStatus, Pageable pageRequest);

	@Query("select ki from KycInfo ki where (upper(ki.fullName) like concat('%', upper(?1), '%'))")
	public Page<KycInfo> getAllUsersByKycUsername(String fullName, Pageable pageRequest);

	@Query("select ki from KycInfo ki where (upper(ki.emailId) like concat('%', upper(?1), '%') or upper(ki.fullName) like concat('%', upper(?2), '%'))")
	public Page<KycInfo> getAllUsersByKycEmailid(String filterKyc1, String filterKyc2, Pageable pageRequest);

	@Query("select ki from KycInfo ki where (upper(ki.emailId) like concat('%', upper(?1), '%') or upper(ki.fullName) like concat('%', upper(?2), '%'))")
	public List<KycInfo> findByEmailId(String filterKyc1, String filterKyc2, Pageable pageRequest);

	@Query("select ki from KycInfo ki where (upper(ki.fullName) like concat('%', upper(?1), '%'))")
	public List<KycInfo> findByUserName(String fullName, Pageable pageRequest);

}
