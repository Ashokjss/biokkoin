package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.Config;


public interface ConfigInfoRepository extends CrudRepository<Config, Integer> {

	public Config findConfigByConfigKey(String configKey);
	

}
