package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.BitcoinConfigInfo;



public interface BitcoinConfigInfoRepository extends CrudRepository<BitcoinConfigInfo, Integer>{

	public BitcoinConfigInfo findBitcoinConfigByConfigKey(String configKey);
	
}
