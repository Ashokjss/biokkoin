package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;
import com.biocoin.model.Token_info;

public interface TokensInfoRepo extends CrudRepository<Token_info, Integer> {

	public Token_info findById(Integer id);
	
}
