package com.biocoin.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.biocoin.model.PurchaseTokenInfo;

public interface PurchaseTokenInfoRepository extends CrudRepository<PurchaseTokenInfo, Integer> {

	public Page<PurchaseTokenInfo> findAll(Pageable pg);
	
	public Page<PurchaseTokenInfo> findPurchaseTokenInfoByEmailId(String emailId, Pageable pg);
	
	@Query("select p from PurchaseTokenInfo p where (upper(p.emailId) like concat('%', upper(?1), '%') or upper(p.typeOfPurchase) like concat('%', upper(?2), '%') or upper(p.asynchStatus) like concat('%', upper(?3), '%')) ")
	Page<PurchaseTokenInfo> findpurchaseList(String emailId, String typeOfPurchase, String searchTxt, Pageable pg);
	
	public PurchaseTokenInfo findPurchaseTokenInfoByPaymentId(String id);
	
}
