package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.ExpirationDataInfo;

public interface ExpirationDataInfoRepo extends CrudRepository<ExpirationDataInfo, Integer>{

}
