package com.biocoin.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import com.biocoin.model.UserRequestCoinInfo;


@Service
public interface UserRequestCoinRepository extends CrudRepository<UserRequestCoinInfo, Integer> {

	List<UserRequestCoinInfo> findByToAddressOrderByCreatedDateDesc(String emailId);
	List<UserRequestCoinInfo> findByToAddressAndTranscationType(String WalletAddress,Integer type);




}
