package com.biocoin.repo;

import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.BurnedToken;

public interface BurnedInfoRepo extends CrudRepository<BurnedToken, Integer> {

}
