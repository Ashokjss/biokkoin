package com.biocoin.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import com.biocoin.model.ICOPeriodInfo;

public interface ICOPeriodInfoRepo extends CrudRepository<ICOPeriodInfo, Integer> {

	public ICOPeriodInfo findICOPeriodInfoByicoStatus(int icoStatus);

	public Page<ICOPeriodInfo> findAll(Pageable pg);
	
}
