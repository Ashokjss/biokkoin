package com.biocoin.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.biocoin.model.TokenAllocationForTopManagementsInfo;

public interface TokenAllocationForTopManagementsInfoRepo extends CrudRepository<TokenAllocationForTopManagementsInfo, Integer> {

	public TokenAllocationForTopManagementsInfo findTokenAllocationForTopManagementsInfoByEmailId(String emailId);
	
	public TokenAllocationForTopManagementsInfo findById(Integer id);
	
	public Integer countTokenAllocationForTopManagementsInfoByTypeOfVerifications(String typeOfVerifications);
	
	public Page<TokenAllocationForTopManagementsInfo> findAll(Pageable pg);
	
}
