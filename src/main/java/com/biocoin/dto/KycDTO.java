package com.biocoin.dto;

import java.util.Date;

//import lombok.Data;

//@Data
public class KycDTO {

	private Integer id;
	private Date creationTime;
	private String fullName;
	private String dob;
	private String gender;
	private String emailId;
	private String address;
	private String mobileNo;
	private String city;
	private String country;
	private Integer kycStatus;
	private String sessionId;
	private String kycDoc1Path;
	private String kycDoc2Path;
	private String kycDoc3Path;
	private String kycDoc4Path;
	private String kycDoc1Name;
	private String kycDoc2Name;
	private String kycDoc3Name;
	private String kycDoc4Name;
	private String message;
	private String typeOfVerifications;
	private Integer kycUploadStatus;
	private Integer pageNum;
	private Integer totalPages;
	private long totalElements;
	private String filterKyc;
	
	

	public String getFilterKyc() {
		return filterKyc;
	}

	public void setFilterKyc(String filterKyc) {
		this.filterKyc = filterKyc;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Integer getKycUploadStatus() {
		return kycUploadStatus;
	}

	public void setKycUploadStatus(Integer kycUploadStatus) {
		this.kycUploadStatus = kycUploadStatus;
	}

	public String getKycDoc3Path() {
		return kycDoc3Path;
	}

	public void setKycDoc3Path(String kycDoc3Path) {
		this.kycDoc3Path = kycDoc3Path;
	}

	public String getKycDoc4Path() {
		return kycDoc4Path;
	}

	public void setKycDoc4Path(String kycDoc4Path) {
		this.kycDoc4Path = kycDoc4Path;
	}

	public String getKycDoc1Name() {
		return kycDoc1Name;
	}

	public void setKycDoc1Name(String kycDoc1Name) {
		this.kycDoc1Name = kycDoc1Name;
	}

	public String getKycDoc2Name() {
		return kycDoc2Name;
	}

	public void setKycDoc2Name(String kycDoc2Name) {
		this.kycDoc2Name = kycDoc2Name;
	}

	public String getKycDoc3Name() {
		return kycDoc3Name;
	}

	public void setKycDoc3Name(String kycDoc3Name) {
		this.kycDoc3Name = kycDoc3Name;
	}

	public String getKycDoc4Name() {
		return kycDoc4Name;
	}

	public void setKycDoc4Name(String kycDoc4Name) {
		this.kycDoc4Name = kycDoc4Name;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getKycDoc1Path() {
		return kycDoc1Path;
	}

	public void setKycDoc1Path(String kycDoc1Path) {
		this.kycDoc1Path = kycDoc1Path;
	}

	public String getKycDoc2Path() {
		return kycDoc2Path;
	}

	public void setKycDoc2Path(String kycDoc2Path) {
		this.kycDoc2Path = kycDoc2Path;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(Integer kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/*
	 * private Integer id; private String kycDoc1Path; private String
	 * kycDoc2Path; private Integer kycStatus; private String message; private
	 * Integer kycUploadStatus; private String country; private String
	 * kycDoc1Name; private String kycDoc2Name;
	 */

}
