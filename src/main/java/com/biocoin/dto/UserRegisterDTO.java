package com.biocoin.dto;

import java.util.Date;

public class UserRegisterDTO {

	private Integer id;
	private String userName;
	private String emailId;
	private String password;
	private String confirmPassword;
	private String eathwalletPassword;
	private String confirmEathwalletPassword;
	private String eathwalletAddress;
	private String btcwalletAddress;
	private String userType;
	private int gmailstatus;
	private String mobileno;
	private String tokenAddress;
	private String oldPassword;
	private String appId;
	private String deviceType;
	private String name;
	private Double allocatedTokens;
	private String tokenValuesInUSD;
	private Date icoStart;
	private Date icoEnd;
	private Double icoAllocationTokens;
	private Double icoSoldTokens;
	private Double icoAvailableTokens;
	private Integer status;
	private String firstName;
	private String lastName;
	private String typeOfVerifications;
	private Integer pageNum;
	private Integer pageSize;
	private Integer totalPages;
	private Integer totalElements;
	private String searchText;
	private Double bitcoinWalletBalance;

	public Double getBitcoinWalletBalance() {
		return bitcoinWalletBalance;
	}

	public void setBitcoinWalletBalance(Double bitcoinWalletBalance) {
		this.bitcoinWalletBalance = bitcoinWalletBalance;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Integer totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

	public Double getIcoSoldTokens() {
		return icoSoldTokens;
	}

	public void setIcoSoldTokens(Double icoSoldTokens) {
		this.icoSoldTokens = icoSoldTokens;
	}

	public Double getIcoAvailableTokens() {
		return icoAvailableTokens;
	}

	public void setIcoAvailableTokens(Double icoAvailableTokens) {
		this.icoAvailableTokens = icoAvailableTokens;
	}

	private String btcWalletPassword;
	private String btcWalletGuid;

	public String getBtcWalletGuid() {
		return btcWalletGuid;
	}

	public void setBtcWalletGuid(String btcWalletGuid) {
		this.btcWalletGuid = btcWalletGuid;
	}

	public String getBtcWalletPassword() {
		return btcWalletPassword;
	}

	public void setBtcWalletPassword(String btcWalletPassword) {
		this.btcWalletPassword = btcWalletPassword;
	}

	public String getConfirmEathwalletPassword() {
		return confirmEathwalletPassword;
	}

	public void setConfirmEathwalletPassword(String confirmEathwalletPassword) {
		this.confirmEathwalletPassword = confirmEathwalletPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getIcoStart() {
		return icoStart;
	}

	public void setIcoStart(Date icoStart) {
		this.icoStart = icoStart;
	}

	public Date getIcoEnd() {
		return icoEnd;
	}

	public void setIcoEnd(Date icoEnd) {
		this.icoEnd = icoEnd;
	}

	public Double getIcoAllocationTokens() {
		return icoAllocationTokens;
	}

	public void setIcoAllocationTokens(Double icoAllocationTokens) {
		this.icoAllocationTokens = icoAllocationTokens;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTokenValuesInUSD() {
		return tokenValuesInUSD;
	}

	public void setTokenValuesInUSD(String tokenValuesInUSD) {
		this.tokenValuesInUSD = tokenValuesInUSD;
	}

	public Double getAllocatedTokens() {
		return allocatedTokens;
	}

	public void setAllocatedTokens(Double allocatedTokens) {
		this.allocatedTokens = allocatedTokens;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	private String sessionId;
	private String qrCode;
	private Integer securedKey;

	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEathwalletPassword() {
		return eathwalletPassword;
	}

	public void setEathwalletPassword(String eathwalletPassword) {
		this.eathwalletPassword = eathwalletPassword;
	}

	public String getEathwalletAddress() {
		return eathwalletAddress;
	}

	public void setEathwalletAddress(String eathwalletAddress) {
		this.eathwalletAddress = eathwalletAddress;
	}

	public String getBtcwalletAddress() {
		return btcwalletAddress;
	}

	public void setBtcwalletAddress(String btcwalletAddress) {
		this.btcwalletAddress = btcwalletAddress;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getGmailstatus() {
		return gmailstatus;
	}

	public void setGmailstatus(int gmailstatus) {
		this.gmailstatus = gmailstatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public Integer getSecuredKey() {
		return securedKey;
	}

	public void setSecuredKey(Integer securedKey) {
		this.securedKey = securedKey;
	}

	public String getTokenAddress() {
		return tokenAddress;
	}

	public void setTokenAddress(String tokenAddress) {
		this.tokenAddress = tokenAddress;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

}
