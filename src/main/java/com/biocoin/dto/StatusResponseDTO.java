package com.biocoin.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import org.json.simple.JSONObject;

public class StatusResponseDTO {

	private String status;
	private String message;
	private String walletAddress;
	private BigDecimal etherBalance;
	private LoginDTO loginInfo;
	private TokenDTO userBalanceInfo;
	private TokenDTO adminBalanceInfo;
	private TokenDTO mainAccountInfo;
	private Integer securedKey;
	private String userType;
	private String bitcoinResponse;
	private BigDecimal bitcoinBalance;
	private BigInteger tokenBalance;
	private List<TokenDTO> listToken;
	private List<UserRegisterDTO> allocatedDetails;
	private List<TokenDTO> transactionHistory;
	private List<UserRegisterDTO> userList;
	private List<UserRegisterDTO> valuesInUSD;
	private List<UserRegisterDTO> listICOPeriodInfo;
	private UserRegisterDTO forgotPasswordInfo;
	private Integer totalPages;
	private Integer totalElements;
	private List<TokenDTO> transactionHistoryInfo;
	private KycDTO kycInfo;
	private KycDTO kycUserInfo;
	private List<KycDTO> kycList;
	private List<TokenDTO> purchaseList;
	private TokenDTO currentRateInfo;
	private JSONObject data;

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	public TokenDTO getCurrentRateInfo() {
		return currentRateInfo;
	}

	public void setCurrentRateInfo(TokenDTO currentRateInfo) {
		this.currentRateInfo = currentRateInfo;
	}

	public List<TokenDTO> getPurchaseList() {
		return purchaseList;
	}

	public void setPurchaseList(List<TokenDTO> purchaseList) {
		this.purchaseList = purchaseList;
	}

	public List<KycDTO> getKycList() {
		return kycList;
	}

	public void setKycList(List<KycDTO> kycList) {
		this.kycList = kycList;
	}

	public KycDTO getKycUserInfo() {
		return kycUserInfo;
	}

	public void setKycUserInfo(KycDTO kycUserInfo) {
		this.kycUserInfo = kycUserInfo;
	}

	public KycDTO getKycInfo() {
		return kycInfo;
	}

	public void setKycInfo(KycDTO kycInfo) {
		this.kycInfo = kycInfo;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public List<TokenDTO> getTransactionHistoryInfo() {
		return transactionHistoryInfo;
	}

	public void setTransactionHistoryInfo(List<TokenDTO> transactionHistoryInfo) {
		this.transactionHistoryInfo = transactionHistoryInfo;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Integer totalElements) {
		this.totalElements = totalElements;
	}

	public UserRegisterDTO getForgotPasswordInfo() {
		return forgotPasswordInfo;
	}

	public void setForgotPasswordInfo(UserRegisterDTO forgotPasswordInfo) {
		this.forgotPasswordInfo = forgotPasswordInfo;
	}

	public TokenDTO getUserBalanceInfo() {
		return userBalanceInfo;
	}

	public void setUserBalanceInfo(TokenDTO userBalanceInfo) {
		this.userBalanceInfo = userBalanceInfo;
	}

	public TokenDTO getAdminBalanceInfo() {
		return adminBalanceInfo;
	}

	public void setAdminBalanceInfo(TokenDTO adminBalanceInfo) {
		this.adminBalanceInfo = adminBalanceInfo;
	}

	public List<UserRegisterDTO> getListICOPeriodInfo() {
		return listICOPeriodInfo;
	}

	public void setListICOPeriodInfo(List<UserRegisterDTO> listICOPeriodInfo) {
		this.listICOPeriodInfo = listICOPeriodInfo;
	}

	public List<UserRegisterDTO> getValuesInUSD() {
		return valuesInUSD;
	}

	public void setValuesInUSD(List<UserRegisterDTO> valuesInUSD) {
		this.valuesInUSD = valuesInUSD;
	}

	public List<UserRegisterDTO> getAllocatedDetails() {
		return allocatedDetails;
	}

	public void setAllocatedDetails(List<UserRegisterDTO> allocatedDetails) {
		this.allocatedDetails = allocatedDetails;
	}

	public List<UserRegisterDTO> getUserList() {
		return userList;
	}

	public void setUserList(List<UserRegisterDTO> userList) {
		this.userList = userList;
	}

	public BigDecimal getBitcoinBalance() {
		return bitcoinBalance;
	}

	public void setBitcoinBalance(BigDecimal bitcoinBalance) {
		this.bitcoinBalance = bitcoinBalance;
	}

	public String getBitcoinResponse() {
		return bitcoinResponse;
	}

	public void setBitcoinResponse(String bitcoinResponse) {
		this.bitcoinResponse = bitcoinResponse;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getWalletAddress() {
		return walletAddress;
	}

	public void setWalletAddress(String walletAddress) {
		this.walletAddress = walletAddress;
	}

	public BigDecimal getEtherBalance() {
		return etherBalance;
	}

	public void setEtherBalance(BigDecimal etherBalance) {
		this.etherBalance = etherBalance;
	}

	public BigInteger getTokenBalance() {
		return tokenBalance;
	}

	public void setTokenBalance(BigInteger tokenBalance) {
		this.tokenBalance = tokenBalance;
	}

	public LoginDTO getLoginInfo() {
		return loginInfo;
	}

	public void setLoginInfo(LoginDTO loginInfo) {
		this.loginInfo = loginInfo;
	}

	public TokenDTO getMainAccountInfo() {
		return mainAccountInfo;
	}

	public void setMainAccountInfo(TokenDTO mainAccountInfo) {
		this.mainAccountInfo = mainAccountInfo;
	}

	public List<TokenDTO> getTransactionHistory() {
		return transactionHistory;
	}

	public void setTransactionHistory(List<TokenDTO> transactionHistory) {
		this.transactionHistory = transactionHistory;
	}

	public Integer getSecuredKey() {
		return securedKey;
	}

	public void setSecuredKey(Integer securedKey) {
		this.securedKey = securedKey;
	}

	public List<TokenDTO> getListToken() {
		return listToken;
	}

	public void setListToken(List<TokenDTO> listToken) {
		this.listToken = listToken;
	}

}
