package com.biocoin.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class TokenDTO {

	private Integer id;
	private Integer userId;
	private Double initialValue;
	private String coinName;
	private String coinSymbol;
	private BigInteger decimalUnits;
	private String centralAdmin;
	private BigDecimal mainBalance;
	private Double tokens;
	private String toAddress;
	private String fromAddress;
	private BigDecimal amount;
	private BigDecimal transferAmount;
	private Double requestToken;
	private Double requestTokens;
	private BigInteger gasValue;
	private Integer gasPrice;
	private String paymentMode;
	private String ethWalletAddress;
	private String ethWalletPassword;
	private Double tokenBalance;
	private Double TotaltokenBalance;
	private Double selledTokens;
	private Double initialTokens;
	private Double availableTokens;
	private String tokenAddress;
	private Double burnToken;
	private Double mintToken;
	private long totalUsers;
	private String txTpe;
	private String sessionId;
	private String emailId;
	private Date createDate;
	private BigDecimal etherbalance;
	private BigDecimal bitcoinBalance;
	private BigInteger totalUserCount;
	private BigInteger crowdSaleTokenBalance;
	private BigInteger crowdSaleSoldTokens;
	private BigInteger transferTokenBalance;
	private String selectTransactionType;
	private String txStatus;
	private Integer transferToken;
	private Double totalSoldTokens;
	private double biocoinineather;
	private double biocoininbitcoin;
	private double biocoinrate;
	private String sender;
	private String receiver;
	private Double sendToken;
	private String typeOfPurchase;
	private Double tokenAmount;
	private String typeOfVerifications;
	private Double ethToUSD;
	private Integer pageNum;
	private Integer pageSize;
	private Integer totalPages;
	private Integer totalElements;
	private Integer transactionType;
	private Date txDate;
	private Integer kycId;
	private String searchEtherWalletAddress;
	private String searchStatus;
	private Double totalTokens;
	private Double ICOAvailableTokens;
	private Integer topManagementCounts;
	private String approveId;
	private Double cryptoAmount;
	private String searchText;
	private String currentTokenRate;
	private Double purchasedTokens;
	private String btcWalletAddress;
	private String payer_id;
	private String currency;
	private String walletAddress;

	public String getWalletAddress() {
		return walletAddress;
	}

	public void setWalletAddress(String walletAddress) {
		this.walletAddress = walletAddress;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPayer_id() {
		return payer_id;
	}

	public void setPayer_id(String payer_id) {
		this.payer_id = payer_id;
	}

	public String getBtcWalletAddress() {
		return btcWalletAddress;
	}

	public void setBtcWalletAddress(String btcWalletAddress) {
		this.btcWalletAddress = btcWalletAddress;
	}

	public Double getPurchasedTokens() {
		return purchasedTokens;
	}

	public void setPurchasedTokens(Double purchasedTokens) {
		this.purchasedTokens = purchasedTokens;
	}

	public String getCurrentTokenRate() {
		return currentTokenRate;
	}

	public void setCurrentTokenRate(String currentTokenRate) {
		this.currentTokenRate = currentTokenRate;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public Double getCryptoAmount() {
		return cryptoAmount;
	}

	public void setCryptoAmount(Double cryptoAmount) {
		this.cryptoAmount = cryptoAmount;
	}

	public String getApproveId() {
		return approveId;
	}

	public void setApproveId(String approveId) {
		this.approveId = approveId;
	}

	public Integer getTopManagementCounts() {
		return topManagementCounts;
	}

	public void setTopManagementCounts(Integer topManagementCounts) {
		this.topManagementCounts = topManagementCounts;
	}

	public Double getICOAvailableTokens() {
		return ICOAvailableTokens;
	}

	public void setICOAvailableTokens(Double iCOAvailableTokens) {
		ICOAvailableTokens = iCOAvailableTokens;
	}

	public Double getTotalTokens() {
		return totalTokens;
	}

	public void setTotalTokens(Double totalTokens) {
		this.totalTokens = totalTokens;
	}

	public String getSearchEtherWalletAddress() {
		return searchEtherWalletAddress;
	}

	public void setSearchEtherWalletAddress(String searchEtherWalletAddress) {
		this.searchEtherWalletAddress = searchEtherWalletAddress;
	}

	public String getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(String searchStatus) {
		this.searchStatus = searchStatus;
	}

	public Integer getKycId() {
		return kycId;
	}

	public void setKycId(Integer kycId) {
		this.kycId = kycId;
	}

	public Date getTxDate() {
		return txDate;
	}

	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}

	public Integer getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Integer totalPages) {
		this.totalPages = totalPages;
	}

	public Integer getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Integer totalElements) {
		this.totalElements = totalElements;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Double getEthToUSD() {
		return ethToUSD;
	}

	public void setEthToUSD(Double ethToUSD) {
		this.ethToUSD = ethToUSD;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Double getInitialValue() {
		return initialValue;
	}

	public void setInitialValue(Double initialValue) {
		this.initialValue = initialValue;
	}

	public String getCoinName() {
		return coinName;
	}

	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	public String getCoinSymbol() {
		return coinSymbol;
	}

	public void setCoinSymbol(String coinSymbol) {
		this.coinSymbol = coinSymbol;
	}

	public BigInteger getDecimalUnits() {
		return decimalUnits;
	}

	public void setDecimalUnits(BigInteger decimalUnits) {
		this.decimalUnits = decimalUnits;
	}

	public String getCentralAdmin() {
		return centralAdmin;
	}

	public void setCentralAdmin(String centralAdmin) {
		this.centralAdmin = centralAdmin;
	}

	public BigDecimal getMainBalance() {
		return mainBalance;
	}

	public void setMainBalance(BigDecimal mainBalance) {
		this.mainBalance = mainBalance;
	}

	public Double getTokens() {
		return tokens;
	}

	public void setTokens(Double tokens) {
		this.tokens = tokens;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	public Double getRequestToken() {
		return requestToken;
	}

	public void setRequestToken(Double requestToken) {
		this.requestToken = requestToken;
	}

	public Double getRequestTokens() {
		return requestTokens;
	}

	public void setRequestTokens(Double requestTokens) {
		this.requestTokens = requestTokens;
	}

	public BigInteger getGasValue() {
		return gasValue;
	}

	public void setGasValue(BigInteger gasValue) {
		this.gasValue = gasValue;
	}

	public Integer getGasPrice() {
		return gasPrice;
	}

	public void setGasPrice(Integer gasPrice) {
		this.gasPrice = gasPrice;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getEthWalletAddress() {
		return ethWalletAddress;
	}

	public void setEthWalletAddress(String ethWalletAddress) {
		this.ethWalletAddress = ethWalletAddress;
	}

	public String getEthWalletPassword() {
		return ethWalletPassword;
	}

	public void setEthWalletPassword(String ethWalletPassword) {
		this.ethWalletPassword = ethWalletPassword;
	}

	public Double getTokenBalance() {
		return tokenBalance;
	}

	public void setTokenBalance(Double tokenBalance) {
		this.tokenBalance = tokenBalance;
	}

	public Double getTotaltokenBalance() {
		return TotaltokenBalance;
	}

	public void setTotaltokenBalance(Double totaltokenBalance) {
		TotaltokenBalance = totaltokenBalance;
	}

	public Double getSelledTokens() {
		return selledTokens;
	}

	public void setSelledTokens(Double selledTokens) {
		this.selledTokens = selledTokens;
	}

	public Double getInitialTokens() {
		return initialTokens;
	}

	public void setInitialTokens(Double initialTokens) {
		this.initialTokens = initialTokens;
	}

	public Double getAvailableTokens() {
		return availableTokens;
	}

	public void setAvailableTokens(Double availableTokens) {
		this.availableTokens = availableTokens;
	}

	public String getTokenAddress() {
		return tokenAddress;
	}

	public void setTokenAddress(String tokenAddress) {
		this.tokenAddress = tokenAddress;
	}

	public Double getBurnToken() {
		return burnToken;
	}

	public void setBurnToken(Double burnToken) {
		this.burnToken = burnToken;
	}

	public Double getMintToken() {
		return mintToken;
	}

	public void setMintToken(Double mintToken) {
		this.mintToken = mintToken;
	}

	public long getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(long totalUsers) {
		this.totalUsers = totalUsers;
	}

	public String getTxTpe() {
		return txTpe;
	}

	public void setTxTpe(String txTpe) {
		this.txTpe = txTpe;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public BigDecimal getEtherbalance() {
		return etherbalance;
	}

	public void setEtherbalance(BigDecimal etherbalance) {
		this.etherbalance = etherbalance;
	}

	public BigDecimal getBitcoinBalance() {
		return bitcoinBalance;
	}

	public void setBitcoinBalance(BigDecimal bitcoinBalance) {
		this.bitcoinBalance = bitcoinBalance;
	}

	public BigInteger getTotalUserCount() {
		return totalUserCount;
	}

	public void setTotalUserCount(BigInteger totalUserCount) {
		this.totalUserCount = totalUserCount;
	}

	public BigInteger getCrowdSaleTokenBalance() {
		return crowdSaleTokenBalance;
	}

	public void setCrowdSaleTokenBalance(BigInteger crowdSaleTokenBalance) {
		this.crowdSaleTokenBalance = crowdSaleTokenBalance;
	}

	public BigInteger getCrowdSaleSoldTokens() {
		return crowdSaleSoldTokens;
	}

	public void setCrowdSaleSoldTokens(BigInteger crowdSaleSoldTokens) {
		this.crowdSaleSoldTokens = crowdSaleSoldTokens;
	}

	public BigInteger getTransferTokenBalance() {
		return transferTokenBalance;
	}

	public void setTransferTokenBalance(BigInteger transferTokenBalance) {
		this.transferTokenBalance = transferTokenBalance;
	}

	public String getSelectTransactionType() {
		return selectTransactionType;
	}

	public void setSelectTransactionType(String selectTransactionType) {
		this.selectTransactionType = selectTransactionType;
	}

	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	public Integer getTransferToken() {
		return transferToken;
	}

	public void setTransferToken(Integer transferToken) {
		this.transferToken = transferToken;
	}

	public Double getTotalSoldTokens() {
		return totalSoldTokens;
	}

	public void setTotalSoldTokens(Double totalSoldTokens) {
		this.totalSoldTokens = totalSoldTokens;
	}

	public double getBiocoinineather() {
		return biocoinineather;
	}

	public void setBiocoinineather(double biocoinineather) {
		this.biocoinineather = biocoinineather;
	}

	public double getBiocoininbitcoin() {
		return biocoininbitcoin;
	}

	public void setBiocoininbitcoin(double biocoininbitcoin) {
		this.biocoininbitcoin = biocoininbitcoin;
	}

	public double getBiocoinrate() {
		return biocoinrate;
	}

	public void setBiocoinrate(double biocoinrate) {
		this.biocoinrate = biocoinrate;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Double getSendToken() {
		return sendToken;
	}

	public void setSendToken(Double sendToken) {
		this.sendToken = sendToken;
	}

	public String getTypeOfPurchase() {
		return typeOfPurchase;
	}

	public void setTypeOfPurchase(String typeOfPurchase) {
		this.typeOfPurchase = typeOfPurchase;
	}

	public Double getTokenAmount() {
		return tokenAmount;
	}

	public void setTokenAmount(Double tokenAmount) {
		this.tokenAmount = tokenAmount;
	}

	public String getTypeOfVerifications() {
		return typeOfVerifications;
	}

	public void setTypeOfVerifications(String typeOfVerifications) {
		this.typeOfVerifications = typeOfVerifications;
	}

}
