package com.biocoin.dto;

import java.util.Date;
import java.util.List;

public class PaypalDTO {

	private String id;
	private String intent;
	private String state;
	private List<PaypalDTO> transactions;
	private List<PaypalDTO> links;
	private List<PaypalDTO> payer;
	private String payment_method;
	private String total;
	private List<PaypalDTO> amount;
	private String currency;
	private List<PaypalDTO> related_resources;
	private Date create_time;
	private String href;
	private String rel;
	private String method;
	private String payer_id;
	private String paymentID;
	private String sessionId;
	private String emailId;
	private String typeOfPurchase;
	private Double tokenAmount;
	private String accessToken;
	private String cancelPayment;
	
	
	public String getCancelPayment() {
		return cancelPayment;
	}
	public void setCancelPayment(String cancelPayment) {
		this.cancelPayment = cancelPayment;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Double getTokenAmount() {
		return tokenAmount;
	}
	public void setTokenAmount(Double tokenAmount) {
		this.tokenAmount = tokenAmount;
	}
	public String getTypeOfPurchase() {
		return typeOfPurchase;
	}
	public void setTypeOfPurchase(String typeOfPurchase) {
		this.typeOfPurchase = typeOfPurchase;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getPayer_id() {
		return payer_id;
	}
	public void setPayer_id(String payer_id) {
		this.payer_id = payer_id;
	}
	public String getPaymentID() {
		return paymentID;
	}
	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}
	public List<PaypalDTO> getPayer() {
		return payer;
	}
	public void setPayer(List<PaypalDTO> payer) {
		this.payer = payer;
	}
	public List<PaypalDTO> getAmount() {
		return amount;
	}
	public void setAmount(List<PaypalDTO> amount) {
		this.amount = amount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<PaypalDTO> getTransactions() {
		return transactions;
	}
	public void setTransactions(List<PaypalDTO> transactions) {
		this.transactions = transactions;
	}
	public List<PaypalDTO> getLinks() {
		return links;
	}
	public void setLinks(List<PaypalDTO> links) {
		this.links = links;
	}

	public String getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public List<PaypalDTO> getRelated_resources() {
		return related_resources;
	}
	public void setRelated_resources(List<PaypalDTO> related_resources) {
		this.related_resources = related_resources;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getRel() {
		return rel;
	}
	public void setRel(String rel) {
		this.rel = rel;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
}
