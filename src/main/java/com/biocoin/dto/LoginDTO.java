package com.biocoin.dto;

import java.math.BigDecimal;

public class LoginDTO {

	private String mobileNo;
	private String isdCode;
	private String emailId;
	private String password;
	private String userName;
	private String walletAddress;
	private BigDecimal walletBalance;
	private String id;
	private String userType;
	private String sessionId;
	private String tokenAddress;
	private String qrCode;
	private String securedKey;
	private String bitcoinWalletReceivingAddress;
	private BigDecimal ETHBalance;
	private BigDecimal userETHUSD;
	private String userBIOUSD;
	private String BIOBalance;
	private int requestCoinCount;
	private String firstName;
	private String lastName;
	private Integer kycStatus;

	public Integer getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(Integer kycStatus) {
		this.kycStatus = kycStatus;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBitcoinWalletReceivingAddress() {
		return bitcoinWalletReceivingAddress;
	}

	public void setBitcoinWalletReceivingAddress(String bitcoinWalletReceivingAddress) {
		this.bitcoinWalletReceivingAddress = bitcoinWalletReceivingAddress;
	}

	public String getSecuredKey() {
		return securedKey;
	}

	public void setSecuredKey(String securedKey) {
		this.securedKey = securedKey;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(BigDecimal walletBalance) {
		this.walletBalance = walletBalance;
	}

	public String getWalletAddress() {
		return walletAddress;
	}

	public void setWalletAddress(String walletAddress) {
		this.walletAddress = walletAddress;
	}

	private String status;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsdCode() {
		return isdCode;
	}

	public void setIsdCode(String isdCode) {
		this.isdCode = isdCode;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTokenAddress() {
		return tokenAddress;
	}

	public void setTokenAddress(String tokenAddress) {
		this.tokenAddress = tokenAddress;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public BigDecimal getETHBalance() {
		return ETHBalance;
	}

	public void setETHBalance(BigDecimal eTHBalance) {
		ETHBalance = eTHBalance;
	}

	public BigDecimal getUserETHUSD() {
		return userETHUSD;
	}

	public void setUserETHUSD(BigDecimal userETHUSD) {
		this.userETHUSD = userETHUSD;
	}

	public int getRequestCoinCount() {
		return requestCoinCount;
	}

	public void setRequestCoinCount(int requestCoinCount) {
		this.requestCoinCount = requestCoinCount;
	}

	public String getUserBIOUSD() {
		return userBIOUSD;
	}

	public void setUserBIOUSD(String userBIOUSD) {
		this.userBIOUSD = userBIOUSD;
	}

	public String getBIOBalance() {
		return BIOBalance;
	}

	public void setBIOBalance(String bIOBalance) {
		BIOBalance = bIOBalance;
	}

}
