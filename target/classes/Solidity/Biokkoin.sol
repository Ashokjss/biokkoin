pragma solidity ^0.4.18;

/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
  function mul(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a * b;
    assert(a == 0 || c / a == b);
    return c;
  }

  function div(uint256 a, uint256 b) internal constant returns (uint256) {
    // assert(b > 0); // Solidity automatically throws when dividing by 0
    uint256 c = a / b;
    // assert(a == b * c + a % b); // There is no case in which this doesn't hold
    return c;
  }

  function sub(uint256 a, uint256 b) internal constant returns (uint256) {
    assert(b <= a);
    return a - b;
  }

  function add(uint256 a, uint256 b) internal constant returns (uint256) {
    uint256 c = a + b;
    assert(c >= a);
    return c;
  }

  function toUINT112(uint256 a) internal constant returns(uint112) {
    assert(uint112(a) == a);
    return uint112(a);
  }

  function toUINT120(uint256 a) internal constant returns(uint120) {
    assert(uint120(a) == a);
    return uint120(a);
  }

  function toUINT128(uint256 a) internal constant returns(uint128) {
    assert(uint128(a) == a);
    return uint128(a);
  }
}

contract Owned {

    address public owner;

    function Owned() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }

    function setOwner(address _newOwner) onlyOwner {
	 if(_newOwner == 0x0)revert();
        owner = _newOwner;
    }
}

interface ERC20 {
    function balanceOf(address who) public view returns (uint256);
    function transfer(address to, uint256 value) public returns (bool);
    function allowance(address owner, address spender) public view returns (uint256);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function approve(address spender, uint256 value) public returns (bool);
    function burn( uint256 _value) public returns (bool success);
    event Burn(address indexed from, uint256 value);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

interface ERC223 {
    function transfer(address to, uint value, bytes data) public;
    event Transfer(address indexed from, address indexed to, uint value, bytes indexed data);
}

contract ERC223ReceivingContract { 
    function tokenFallback(address _from, uint _value, bytes _data) public;
}

contract Biokkoin is ERC20, ERC223, Owned{
    using SafeMath for uint;
    
    string internal _name;
    string internal _symbol;
    uint8 internal _decimals=0;
    uint256 internal _totalSupply;
    uint256 public mintCount;
    uint256 public deleteToken;
    uint256 public soldToken;
    mapping (address => uint256) internal balances;
    mapping (address => mapping (address => uint256)) internal allowed;
    
    function Biokkoin(string name, string symbol,  uint256 totalSupply) public {
        _symbol = symbol;
        _name = name;
		 _totalSupply = totalSupply *10**uint256(_decimals); 
         balances[msg.sender] = totalSupply;
    }
    
    function name() public view returns (string) {
        return _name;
    }
    
    function symbol() public view returns (string) {
        return _symbol;
    }
    
    function decimals() public view returns (uint8) {
        return _decimals;
    }
    
    function totalSupply() public view returns (uint256) {
        return _totalSupply;
    }
    
    function balanceOf(address _owner) public view returns (uint256 balance) {
        return balances[_owner];
    }
    
     function transferFrom(
        address _from,
        address _to,
        uint256 _amount
    ) returns (bool success) {
        // according to AssetToken's total supply, never overflow here
        if (balances[_from] >= _amount
            && allowed[_from][msg.sender] >= _amount
            && _amount > 0) {
            balances[_from] = balances[_from].sub(_amount).toUINT112();
            allowed[_from][msg.sender] -= _amount;
            balances[_to] = _amount.add(balances[_to]).toUINT112();
            Transfer(_from, _to, _amount);
            return true;
        } else {
            return false;
        }
    }
      
    function approve(address _spender, uint256 _amount) returns (bool success) {
        allowed[msg.sender][_spender] = _amount;
        Approval(msg.sender, _spender, _amount);
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

    //Mint tokens and assign to some one
    function mint(address _owner, uint256 _amount) onlyOwner{
     
            balances[_owner] = _amount.add(balances[_owner]).toUINT112();
            mintCount =  _amount.add(mintCount).toUINT112();

    }
  
  	function burn(uint256 _count) public returns (bool success) {
          balances[msg.sender] -=uint112( _count);
          deleteToken = _count.add(deleteToken).toUINT112();
          Burn(msg.sender, _count);
		  return true;
    }
  
  	// Transfer the balance from owner's account to another account
    function transfer(address _to, uint256 _amount) returns (bool success) {
        // according to AssetToken's total supply, never overflow here
        if (balances[msg.sender] >= _amount
            && _amount > 0) {            
            balances[msg.sender] -= uint112(_amount);
            balances[_to] = _amount.add(balances[_to]).toUINT112();
            soldToken = _amount.add(soldToken).toUINT112();
            Transfer(msg.sender, _to, _amount);
            return true;
        } else {
            return false;
        }
    }
  	
  	function transfer(address _to, uint _value, bytes _data) public {
    	// Standard function transfer similar to ERC20 transfer with no _data .
    	// Added due to backwards compatibility reasons .
    	uint codeLength;

        assembly {
            // Retrieve the size of the code on target address, this needs assembly .
            codeLength := extcodesize(_to)
        }
        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);      	
        if(codeLength>0) {
          	ERC223ReceivingContract receiver = ERC223ReceivingContract(_to);
            receiver.tokenFallback(msg.sender, _value, _data);
          	Transfer(msg.sender, _to, _value, _data);
        }
    }
}