package com.biocoin.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BiocoinValue.class)
public abstract class BiocoinValue_ {

	public static volatile SingularAttribute<BiocoinValue, String> value_symbal;
	public static volatile SingularAttribute<BiocoinValue, Double> biocoin_value;
	public static volatile SingularAttribute<BiocoinValue, Integer> id;

}

