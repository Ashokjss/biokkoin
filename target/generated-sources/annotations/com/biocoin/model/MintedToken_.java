package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MintedToken.class)
public abstract class MintedToken_ {

	public static volatile SingularAttribute<MintedToken, String> mintedETHwalletaddress;
	public static volatile SingularAttribute<MintedToken, Date> date;
	public static volatile SingularAttribute<MintedToken, RegisterInfo> registerInfo;
	public static volatile SingularAttribute<MintedToken, Double> mintedtokens;
	public static volatile SingularAttribute<MintedToken, String> emailId;
	public static volatile SingularAttribute<MintedToken, Integer> id;

}

