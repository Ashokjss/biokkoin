package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ICOPeriodInfo.class)
public abstract class ICOPeriodInfo_ {

	public static volatile SingularAttribute<ICOPeriodInfo, Date> icoEnd;
	public static volatile SingularAttribute<ICOPeriodInfo, Double> icoAllocationTokens;
	public static volatile SingularAttribute<ICOPeriodInfo, Double> icoSoldTokens;
	public static volatile SingularAttribute<ICOPeriodInfo, Double> icoAvailableTokens;
	public static volatile SingularAttribute<ICOPeriodInfo, Integer> id;
	public static volatile SingularAttribute<ICOPeriodInfo, Integer> icoStatus;
	public static volatile SingularAttribute<ICOPeriodInfo, Date> icoStart;

}

