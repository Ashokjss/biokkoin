package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PurchaseTokenInfo.class)
public abstract class PurchaseTokenInfo_ {

	public static volatile SingularAttribute<PurchaseTokenInfo, String> typeOfPurchase;
	public static volatile SingularAttribute<PurchaseTokenInfo, String> btcWalletAddress;
	public static volatile SingularAttribute<PurchaseTokenInfo, Double> cryptoAmount;
	public static volatile SingularAttribute<PurchaseTokenInfo, String> etherWalletAddress;
	public static volatile SingularAttribute<PurchaseTokenInfo, String> paymentId;
	public static volatile SingularAttribute<PurchaseTokenInfo, Double> purchaseTokens;
	public static volatile SingularAttribute<PurchaseTokenInfo, String> asynchStatus;
	public static volatile SingularAttribute<PurchaseTokenInfo, Date> purchasedDate;
	public static volatile SingularAttribute<PurchaseTokenInfo, Integer> transferType;
	public static volatile SingularAttribute<PurchaseTokenInfo, String> emailId;
	public static volatile SingularAttribute<PurchaseTokenInfo, Integer> id;

}

