package com.biocoin.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TokenAllocationForTopManagementsInfo.class)
public abstract class TokenAllocationForTopManagementsInfo_ {

	public static volatile SingularAttribute<TokenAllocationForTopManagementsInfo, Double> allocatedTokens;
	public static volatile SingularAttribute<TokenAllocationForTopManagementsInfo, String> typeOfVerifications;
	public static volatile SingularAttribute<TokenAllocationForTopManagementsInfo, String> emailId;
	public static volatile SingularAttribute<TokenAllocationForTopManagementsInfo, Integer> id;

}

