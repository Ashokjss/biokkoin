package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(KycInfo.class)
public abstract class KycInfo_ {

	public static volatile SingularAttribute<KycInfo, String> country;
	public static volatile SingularAttribute<KycInfo, Integer> kycStatus;
	public static volatile SingularAttribute<KycInfo, Date> creationTime;
	public static volatile SingularAttribute<KycInfo, String> gender;
	public static volatile SingularAttribute<KycInfo, String> city;
	public static volatile SingularAttribute<KycInfo, String> kycDoc4Name;
	public static volatile SingularAttribute<KycInfo, String> kycDoc2Name;
	public static volatile SingularAttribute<KycInfo, String> fullName;
	public static volatile SingularAttribute<KycInfo, String> kycDoc2Path;
	public static volatile SingularAttribute<KycInfo, String> typeOfVerifications;
	public static volatile SingularAttribute<KycInfo, String> emailId;
	public static volatile SingularAttribute<KycInfo, String> kycDoc4Path;
	public static volatile SingularAttribute<KycInfo, String> phoneNo;
	public static volatile SingularAttribute<KycInfo, String> kycDoc3Name;
	public static volatile SingularAttribute<KycInfo, Integer> kycUploadStatus;
	public static volatile SingularAttribute<KycInfo, String> dob;
	public static volatile SingularAttribute<KycInfo, String> kycDoc1Name;
	public static volatile SingularAttribute<KycInfo, String> kycDoc3Path;
	public static volatile SingularAttribute<KycInfo, Integer> id;
	public static volatile SingularAttribute<KycInfo, String> kycDoc1Path;
	public static volatile SingularAttribute<KycInfo, String> homeAddress;

}

