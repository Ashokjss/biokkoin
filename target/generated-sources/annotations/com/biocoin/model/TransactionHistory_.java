package com.biocoin.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransactionHistory.class)
public abstract class TransactionHistory_ {

	public static volatile SingularAttribute<TransactionHistory, RegisterInfo> registerInfo;
	public static volatile SingularAttribute<TransactionHistory, String> paymentMode;
	public static volatile SingularAttribute<TransactionHistory, String> typeOfVerifications;
	public static volatile SingularAttribute<TransactionHistory, String> emailId;
	public static volatile SingularAttribute<TransactionHistory, String> toAddress;
	public static volatile SingularAttribute<TransactionHistory, Integer> senderId;
	public static volatile SingularAttribute<TransactionHistory, Integer> receiverId;
	public static volatile SingularAttribute<TransactionHistory, BigDecimal> cryptoAmount;
	public static volatile SingularAttribute<TransactionHistory, Double> transferToken;
	public static volatile SingularAttribute<TransactionHistory, String> txstatus;
	public static volatile SingularAttribute<TransactionHistory, String> fromAddress;
	public static volatile SingularAttribute<TransactionHistory, Integer> id;
	public static volatile SingularAttribute<TransactionHistory, Date> txDate;

}

