package com.biocoin.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(QRCode.class)
public abstract class QRCode_ {

	public static volatile SingularAttribute<QRCode, String> qrcodeValue;
	public static volatile SingularAttribute<QRCode, String> qrKey;
	public static volatile SingularAttribute<QRCode, Integer> id;

}

