package com.biocoin.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BitcoinConfigInfo.class)
public abstract class BitcoinConfigInfo_ {

	public static volatile SingularAttribute<BitcoinConfigInfo, String> configKey;
	public static volatile SingularAttribute<BitcoinConfigInfo, Integer> id;
	public static volatile SingularAttribute<BitcoinConfigInfo, String> configValue;

}

