package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RegisterInfo.class)
public abstract class RegisterInfo_ {

	public static volatile SingularAttribute<RegisterInfo, String> lastName;
	public static volatile SingularAttribute<RegisterInfo, Integer> kycStatus;
	public static volatile ListAttribute<RegisterInfo, BurnedToken> BurnedToken;
	public static volatile SingularAttribute<RegisterInfo, String> btcWalletAddress;
	public static volatile ListAttribute<RegisterInfo, TransactionHistory> TransactionHistory;
	public static volatile SingularAttribute<RegisterInfo, String> emailId;
	public static volatile SingularAttribute<RegisterInfo, String> mobileno;
	public static volatile SingularAttribute<RegisterInfo, Integer> gmailstatus;
	public static volatile SingularAttribute<RegisterInfo, String> password;
	public static volatile SingularAttribute<RegisterInfo, Date> signup_date;
	public static volatile SingularAttribute<RegisterInfo, String> appId;
	public static volatile SingularAttribute<RegisterInfo, String> ethWalletAddress;
	public static volatile SingularAttribute<RegisterInfo, Integer> id;
	public static volatile SingularAttribute<RegisterInfo, String> deviceType;
	public static volatile SingularAttribute<RegisterInfo, Double> lockedTokenBalance;
	public static volatile SingularAttribute<RegisterInfo, Double> burnedTokens;
	public static volatile SingularAttribute<RegisterInfo, String> typeOfVerifications;
	public static volatile SingularAttribute<RegisterInfo, String> btcWalletGuid;
	public static volatile SingularAttribute<RegisterInfo, String> btcWalletPassword;
	public static volatile SingularAttribute<RegisterInfo, String> firstName;
	public static volatile SingularAttribute<RegisterInfo, String> dummywalletaddress;
	public static volatile SingularAttribute<RegisterInfo, String> userType;
	public static volatile ListAttribute<RegisterInfo, MintedToken> MintedToken;
	public static volatile SingularAttribute<RegisterInfo, String> ethWalletPassword;
	public static volatile SingularAttribute<RegisterInfo, Double> tokenBalance;

}

