package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SecurityInfo.class)
public abstract class SecurityInfo_ {

	public static volatile SingularAttribute<SecurityInfo, Integer> securedKey;
	public static volatile SingularAttribute<SecurityInfo, Date> createdTime;
	public static volatile SingularAttribute<SecurityInfo, String> emailId;
	public static volatile SingularAttribute<SecurityInfo, Integer> id;

}

