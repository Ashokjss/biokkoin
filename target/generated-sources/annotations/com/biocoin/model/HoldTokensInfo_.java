package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HoldTokensInfo.class)
public abstract class HoldTokensInfo_ {

	public static volatile SingularAttribute<HoldTokensInfo, Double> lockedTokens;
	public static volatile SingularAttribute<HoldTokensInfo, String> typeOfVerifications;
	public static volatile SingularAttribute<HoldTokensInfo, String> emailId;
	public static volatile SingularAttribute<HoldTokensInfo, Integer> id;
	public static volatile SingularAttribute<HoldTokensInfo, Date> periodEnd;

}

