package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PaymentsInfo.class)
public abstract class PaymentsInfo_ {

	public static volatile SingularAttribute<PaymentsInfo, String> amount;
	public static volatile SingularAttribute<PaymentsInfo, Date> create_time;
	public static volatile SingularAttribute<PaymentsInfo, String> paymentID;
	public static volatile SingularAttribute<PaymentsInfo, Double> tokenAmount;
	public static volatile SingularAttribute<PaymentsInfo, String> currency;
	public static volatile SingularAttribute<PaymentsInfo, String> payer_id;
	public static volatile SingularAttribute<PaymentsInfo, String> emailId;
	public static volatile SingularAttribute<PaymentsInfo, Integer> id;
	public static volatile SingularAttribute<PaymentsInfo, String> ethAddress;
	public static volatile SingularAttribute<PaymentsInfo, String> status;

}

