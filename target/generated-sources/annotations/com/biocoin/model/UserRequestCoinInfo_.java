package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRequestCoinInfo.class)
public abstract class UserRequestCoinInfo_ {

	public static volatile SingularAttribute<UserRequestCoinInfo, String> senderName;
	public static volatile SingularAttribute<UserRequestCoinInfo, String> receiveraName;
	public static volatile SingularAttribute<UserRequestCoinInfo, Date> createdDate;
	public static volatile SingularAttribute<UserRequestCoinInfo, String> paymentMode;
	public static volatile SingularAttribute<UserRequestCoinInfo, Double> requestCoin;
	public static volatile SingularAttribute<UserRequestCoinInfo, Integer> transcationType;
	public static volatile SingularAttribute<UserRequestCoinInfo, String> typeOfVerifications;
	public static volatile SingularAttribute<UserRequestCoinInfo, String> fromAddress;
	public static volatile SingularAttribute<UserRequestCoinInfo, Integer> id;
	public static volatile SingularAttribute<UserRequestCoinInfo, String> toAddress;

}

