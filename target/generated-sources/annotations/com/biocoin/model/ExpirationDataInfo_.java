package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ExpirationDataInfo.class)
public abstract class ExpirationDataInfo_ {

	public static volatile SingularAttribute<ExpirationDataInfo, Boolean> tokenStatus;
	public static volatile SingularAttribute<ExpirationDataInfo, Date> expiredDate;
	public static volatile SingularAttribute<ExpirationDataInfo, String> emailId;
	public static volatile SingularAttribute<ExpirationDataInfo, Integer> id;
	public static volatile SingularAttribute<ExpirationDataInfo, String> token;

}

