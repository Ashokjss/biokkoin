package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BurnedToken.class)
public abstract class BurnedToken_ {

	public static volatile SingularAttribute<BurnedToken, Date> date;
	public static volatile SingularAttribute<BurnedToken, Double> burnedtokens;
	public static volatile SingularAttribute<BurnedToken, RegisterInfo> registerInfo;
	public static volatile SingularAttribute<BurnedToken, String> emailId;
	public static volatile SingularAttribute<BurnedToken, Integer> id;

}

