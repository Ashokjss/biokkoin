package com.biocoin.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Token_info.class)
public abstract class Token_info_ {

	public static volatile SingularAttribute<Token_info, Double> totalToken;
	public static volatile SingularAttribute<Token_info, Date> lockedTokensEndDate;
	public static volatile SingularAttribute<Token_info, Date> endDate;
	public static volatile SingularAttribute<Token_info, Double> burnedTokens;
	public static volatile SingularAttribute<Token_info, Double> icoSoldTokens;
	public static volatile SingularAttribute<Token_info, Double> mintTokens;
	public static volatile SingularAttribute<Token_info, Double> initialToken;
	public static volatile SingularAttribute<Token_info, String> tokenValuesInUSD;
	public static volatile SingularAttribute<Token_info, Double> manualAvailableTokens;
	public static volatile SingularAttribute<Token_info, Double> totalAvailableTokens;
	public static volatile SingularAttribute<Token_info, Double> icoAvailableTokens;
	public static volatile SingularAttribute<Token_info, Integer> id;
	public static volatile SingularAttribute<Token_info, Double> totalSoldTokens;
	public static volatile SingularAttribute<Token_info, Date> startDate;

}

