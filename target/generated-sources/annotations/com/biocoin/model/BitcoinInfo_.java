package com.biocoin.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BitcoinInfo.class)
public abstract class BitcoinInfo_ {

	public static volatile SingularAttribute<BitcoinInfo, String> walletId;
	public static volatile SingularAttribute<BitcoinInfo, String> btcWalletAddress;
	public static volatile SingularAttribute<BitcoinInfo, String> emailId;
	public static volatile SingularAttribute<BitcoinInfo, Integer> id;
	public static volatile SingularAttribute<BitcoinInfo, String> label;
	public static volatile SingularAttribute<BitcoinInfo, String> tokenType;
	public static volatile SingularAttribute<BitcoinInfo, String> accessTokens;
	public static volatile SingularAttribute<BitcoinInfo, String> btcWalletPassword;

}

